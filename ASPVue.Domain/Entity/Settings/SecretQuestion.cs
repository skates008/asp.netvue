﻿using ASPVueProject.Domain.BaseEntity;
using ASPVueProject.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.Domain.Entity
{
    public class SecretQuestion : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string SetupTable { get; set; }
        public bool IsEnable { get; set; }
        
    }
}
