﻿using ASPVueProject.Domain.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.Domain.Entity
{
    [Table("GeneralSetting")]
    public class GeneralSetting:BaseEntity<Guid>
    {
        public bool EnableSMS { get; set; }
        public bool EnablePremiumDelivery { get; set; }
        [Required]
        public string Remarks { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime CreatedDate { get; set; }
        public decimal  B2B { get; set; }
        public decimal  B2C { get; set; }
        public decimal OB2C { get; set; }
        public decimal BonuslinkRm { get; set; }
        public bool EnableVirtualInventory { get; set; }
        public TimeSpan VirtualInventoryStartTime { get; set; }
        public TimeSpan VirtualInventoryEndTime { get; set; }
        public bool EnabledWelcomePromo { get; set; }
        public bool EnableCurrentTxnPromo { get; set; }
        public bool EnableNextTxnPromo { get; set; }
        public bool EnableBonusLink { get; set; }
        public bool EnablePayPayment { get; set; }
        public bool EnableMerchantradeMoneyPayment { get; set; }
        public bool IsActive { get; set; }
    }
}
