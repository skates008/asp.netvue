﻿using ASPVueProject.Domain.BaseEntity;
using System;

namespace ASPVueProject.Domain.Entity
{
    public class NatureOfBusiness : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string SetupTable { get; set; }
        public bool IsEnable { get; set; }
    }
}
