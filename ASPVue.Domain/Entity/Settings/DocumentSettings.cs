﻿using ASPVueProject.Domain.BaseEntity;
using ASPVueProject.Domain.Enum;
using System;

namespace ASPVueProject.Domain.Entity
{
    public class DocumentSettings : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public DocumentType Type { get; set; }
        public bool IsRequired { get; set; }
        public bool IsEnabled { get; set; }
    }
}
