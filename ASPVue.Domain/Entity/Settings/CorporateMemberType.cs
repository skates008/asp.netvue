﻿using ASPVueProject.Domain.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.Domain.Entity
{
    public class CorporateMemberType:BaseEntity<Guid>
    {

        public string Name { get; set; }
        public string Description { get; set; }
        public string SetupTable { get; set; }
        public bool IsActive { get; set; }
    }
}
