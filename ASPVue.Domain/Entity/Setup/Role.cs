﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ASPVueProject.Domain.Entity
{
	public class Role : IdentityRole<Guid, UserRole>
	{
		public string Description { get; set; }
		public bool IsActive { get; set; }
		public bool SystemDefined { get; set; }
		public int? Hierarchy { get; set; }
		public virtual ICollection<UserPage> UserPages { get; set; }
	}
}
