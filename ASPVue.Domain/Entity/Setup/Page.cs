﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPVueProject.Domain.Entity
{
	public class Page
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int Id { get; set; }
		[Required]
		public string Title { get; set; }
		public string Path { get; set; }
		[Required]
		public string Icon { get; set; }
		[Required]
		public int DisplayOrder { get; set; }
		public int? ParentPageId { get; set; }
		public bool ShowInView { get; set; }
	}

	public class UserPage
	{
		public int Id { get; set; }
		[Required]
		public int PageId { get; set; }
		[Required]
		public Guid RoleId { get; set; }
		public virtual Page Page { get; set; }
		public virtual Role Role { get; set; }
	}
}
