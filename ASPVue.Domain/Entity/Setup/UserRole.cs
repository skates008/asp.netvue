﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ASPVueProject.Domain.Entity
{
	public class UserRole : IdentityUserRole<Guid>
	{
		[ForeignKey("UserId")]
		public virtual User User { get; set; }
		[ForeignKey("RoleId")]
		public virtual Role Role { get; set; }
	}
}
