﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ASPVueProject.Domain.Enum;

namespace ASPVueProject.Domain.Entity
{
	public class User : IdentityUser<Guid, UserLogin, UserRole, UserClaim>
	{
		[Required]
		public string FullName { get; set; }
		public string DisplayPicture { get; set; }
		public bool SystemDefined { get; set; }
		public UserType Type { get; set; }
		public UserStatus Status { get; set; }
		public ApprovalStatus ApprovalStatus { get; set; }
		public bool ForceChangePassword { get; set; }
		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User, Guid> manager)
		{
			// Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
			var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
			// Add custom user claims here
			return userIdentity;
		}
		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User, Guid> manager, string authenticationType, List<System.Security.Claims.Claim> claims)
		{
			var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
			userIdentity.AddClaims(claims);
			// Add custom user claims here
			return userIdentity;
		}
	}

	public class UserClaim : IdentityUserClaim<Guid>
	{
	}
	public class UserLogin : IdentityUserLogin<Guid>
	{
	}
}
