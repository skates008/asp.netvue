﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPVueProject.Domain.Entity
{
	public class PreviousPassword
	{
		[Key, Column(Order = 0)]
		public string PasswordHash { get; set; }
		public DateTime CreatedDate { get; set; }
		[Key, Column(Order = 1)]
		public Guid UserId { get; set; }
		[ForeignKey("UserId")]
		public virtual User User { get; set; }
	}
}
