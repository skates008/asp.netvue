﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ASPVueProject.Domain.BaseEntity;

namespace ASPVueProject.Domain.Entity
{
	public class UserDeviceKey : BaseEntity<Guid>
	{
		public Guid UserId { get; set; }
		[ForeignKey("UserId")]
		public virtual User User { get; set; }
		[Required]
		public string DeviceAuthToken { get; set; }
		public DateTime DeviceAuthTokenExpiryDate { get; set; }
		public string RefreshToken { get; set; }
		public string DeviceIdentifier { get; set; }
		public bool IsActive { get; set; }
		public DateTime CreatedDate { get; set; }
		public string OtpToken { get; set; }
	}
}
