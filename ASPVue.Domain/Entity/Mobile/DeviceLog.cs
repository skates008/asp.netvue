﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ASPVueProject.Domain.BaseEntity;
using ASPVueProject.Domain.Enum;

namespace ASPVueProject.Domain.Entity
{
	public class DeviceLog : BaseEntity<Guid>
	{
		[Required]
		public string DeviceToken { get; set; }
		[Required]
		public DeviceType DeviceType { get; set; }
		public Guid? UserId { get; set; }
		[ForeignKey("UserId")]
		public virtual User User { get; set; }
		[Required]
		public DateTime ModifiedDate { get; set; }
		[Required]
		public DateTime CreatedDate { get; set; }
		public bool IsExpired { get; set; }
	}
}
