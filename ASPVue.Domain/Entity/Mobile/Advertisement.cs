﻿using System;
using System.ComponentModel.DataAnnotations;
using ASPVueProject.Domain.BaseEntity;

namespace ASPVueProject.Domain.Entity
{
	public class Advertisement : BaseEntity<Guid>
	{
		[Required]
		public string Title { get; set; }
		[Required]
		public string Description { get; set; }
		[Required]
		public string ImagePath { get; set; }
		[Required]
		public int SortIndex { get; set; }
	}
}
