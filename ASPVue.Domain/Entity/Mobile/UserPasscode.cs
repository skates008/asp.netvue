﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPVueProject.Domain.Entity
{
	public class UserPasscode
	{
		[Key]
		public Guid UserId { get; set; }
		[ForeignKey("UserId")]
		public virtual User User { get; set; }
		public string Code { get; set; }
		public int AccessFailedCount { get; set; }
		public DateTime Date { get; set; }
	}
}
