﻿using ASPVueProject.Domain.BaseEntity;
using ASPVueProject.Domain.Enum;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPVueProject.Domain.Entity
{
	public class Notification : BaseEntity<Guid>
	{
		[Required]
		public string Title { get; set; }
		[Required]
		public string Body { get; set; }
		public DateTime CreatedDate { get; set; }
		public Guid? UserId { get; set; }
		[ForeignKey("UserId")]
		public virtual User User { get; set; }
		public NotificationType NotificationType { get; set; }
		public bool IsDisabled { get; set; }
	}
}
