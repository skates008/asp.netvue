﻿using ASPVueProject.Domain.BaseEntity;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPVueProject.Domain.Entity
{
	public class NotificationUserLog : BaseEntity<Guid>
	{
		public Guid NotificationId { get; set; }
		[ForeignKey("NotificationId")]
		public virtual Notification Notification { get; set; }
		public Guid UserId { get; set; }
		[ForeignKey("UserId")]
		public virtual User User { get; set; }
		public bool IsClear { get; set; }
	}
}
