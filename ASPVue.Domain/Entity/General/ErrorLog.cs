﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ASPVueProject.Domain.Enum;

namespace ASPVueProject.Domain.Entity
{
	public class ErrorLog
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }
		[Required]
		public string Data { get; set; }
		[Required]
		public string Action { get; set; }
		[Required]
		public string IpAddress { get; set; }
		public string Message { get; set; }
		public string InnerException { get; set; }
		public Guid? UserId { get; set; }
		[Required]
		public DateTime Date { get; set; }
		public virtual User User { get; set; }
	}
	public class ApplicationLog
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }
		[Required]
		public string Entity { get; set; }
		[Required]
		public string Data { get; set; }
		[Required]
		public string Action { get; set; }
		[Required]
		public string IpAddress { get; set; }
		[Required]
		public LogType LogType { get; set; }
		public string Description { get; set; }
		public Guid? UserId { get; set; }
		[Required]
		public DateTime Date { get; set; }
		public virtual User User { get; set; }
	}
}
