﻿using System;
using ASPVueProject.Domain.BaseEntity;

namespace ASPVueProject.Domain.Entity
{
	public class SmsLog : BaseEntity<Guid>
	{
		public int TimeStamp { get; set; }
		public string ServiceId { get; set; }
		public string ChargeCode { get; set; }
		public string Mcn { get; set; }
		public string MessageType { get; set; }
		public string Message { get; set; }
		public string MobileNumber { get; set; }
		public string MessageStatus { get; set; }
		public DateTime CreatedDate { get; set; }
	}
}
