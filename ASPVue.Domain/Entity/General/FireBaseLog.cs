﻿using System;
using ASPVueProject.Domain.BaseEntity;

namespace ASPVueProject.Domain.Entity
{
	public class FireBaseLog : BaseEntity<Guid>
	{
		public string SubmitedData { get; set; }
		public string MultiCastId { get; set; }
		public int Success { get; set; }
		public int Failure { get; set; }
		public int CanonicalIds { get; set; }
		public string Results { get; set; }
		public DateTime CreatedDate { get; set; }
	}
}
