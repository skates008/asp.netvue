﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPVueProject.Domain.Entity
{
	public class EnumCollection
	{
		[Key]
		[Column(Order = 1)]
		public string Type { get; set; }
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		[Key]
		[Column(Order = 2)]
		public int Id { get; set; }
		public string Name { get; set; }
	}

	public class ConstantCollection
	{
		[Key]
		public string Key { get; set; }
		public string Value { get; set; }
	}
}
