﻿using ASPVueProject.Domain.BaseEntity;
using System;

namespace ASPVueProject.Domain.Entity
{
	public class CommonAttachment : BaseEntity<Guid>
	{
		public string AttachmentName { get; set; }
		public string AttachmentUrl { get; set; }
	}
}
