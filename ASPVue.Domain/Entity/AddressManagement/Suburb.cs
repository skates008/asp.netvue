﻿using ASPVueProject.Domain.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.Domain.Entity
{
    [Table("Suburb")]
    public class Suburb : BaseEntity<Guid>
    {
        public Guid PostCodeId { get; set; }
        [ForeignKey("PostCodeId")]
        public virtual PostCode PostCode { get; set; }
        public string SuburbName { get; set; }
        public bool IsEnabled { get; set; }

    }
}
