﻿using ASPVueProject.Domain.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.Domain.Entity
{
    [Table("StateProvince")]
    public class StateProvince : BaseEntity<Guid>
    {
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public string DisplayName { get; set; }
        [Required]
        public Guid CountryId { get; set; }
        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }
        public virtual ICollection<City> Cities { get; set; }


    }
}
