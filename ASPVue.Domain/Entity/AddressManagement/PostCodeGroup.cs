﻿using ASPVueProject.Domain.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.Domain.Entity
{
    [Table("PostCodeGroup")]
    public class PostCodeGroup : BaseEntity<int>
    {
        [Required, Column(TypeName = "varchar"), StringLength(250, ErrorMessage = "PostCode Group Name cannot be more than 250 characters")]
        [Display(Name = "Post Code Group")]
        public String PostCodeGroupName { get; set; }
        public bool IsEnabled { get; set; }
        //public virtual ICollection<PostCodeGroupPostCode> PostCodeGroupPostCodes { get; set; }
    }
}
