﻿using System;
using ASPVueProject.Domain.BaseEntity;

namespace ASPVueProject.Domain.Entity
{
	public class Country : BaseEntity<Guid>
	{
		public string Name { get; set; }
		public string Code { get; set; }
		public string Nationality { get; set; }
		public bool IsEnable { get; set; }
		public bool IsHighRisk { get; set; }
	}
}
