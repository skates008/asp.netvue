﻿using ASPVueProject.Domain.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.Domain.Entity
{
    [Table("PostCode")]
    public class PostCode : BaseEntity<Guid>
    {
        public Guid CityId { get; set; }
        [ForeignKey("CityId")]
        public virtual City City { get; set; }

        public string PostCodeName { get; set; }
        public bool IsEnabled { get; set; }

        //public virtual ICollection<Branch> Branches { get; set; }
        //public virtual ICollection<PostCodeGroupPostCode> PostCodeGroupPostCodes { get; set; }
    }
}
