﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.Domain.Entity
{
    [Table("PostCodeGroupPostCode")]
    public class PostCodeGroupPostCode
    {
        [Key, Column(Order = 1)]
        public int PostCodeGroupId { get; set; }
        [Key, Column(Order = 2)]
        public int PostCodeId { get; set; }
        [ForeignKey("PostCodeGroupId")]
        public virtual PostCodeGroup PostCodeGroup { get; set; }
        [ForeignKey("PostCodeId")]
        public virtual PostCode PostCode { get; set; }
    }
}
