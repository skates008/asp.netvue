﻿using ASPVueProject.Domain.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.Domain.Entity
{
    [Table("Street")]
    public class Street: BaseEntity<Guid>
    {
        public Guid SuburbId { get; set; }
        public virtual Suburb Suburb { get; set; }
        public string StreetName { get; set; }
        public bool IsEnabled { get; set; }
    }
}
