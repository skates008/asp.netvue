﻿using ASPVueProject.Domain.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.Domain.Entity
{
    [Table("City")]
    public class City: BaseEntity<Guid>
    {
        public string CityName { get; set; }
        public Guid StateProvinceId { get; set; }
        [ForeignKey("StateProvinceId")]
        public virtual StateProvince StateProvince { get; set; }
        public virtual ICollection<PostCode> PostCodes { get; set; }
    }
}
