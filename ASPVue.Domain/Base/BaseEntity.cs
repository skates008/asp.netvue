﻿namespace ASPVueProject.Domain.BaseEntity
{
    public class BaseEntity<TKey>
    {
        public virtual TKey Id { get; set; }
    }
}
