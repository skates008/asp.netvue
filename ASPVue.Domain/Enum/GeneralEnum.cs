﻿using System.ComponentModel;

namespace ASPVueProject.Domain.Enum
{
    public enum LogType
    {
        System,
        Activity,
        Error
    }
    public enum LogDescription
    {
        [Description("Updated successfully")]
        Update,
        [Description("Added successfully")]
        Add,
        [Description("Deleted successfully")]
        Delete,
        [Description("Password Reset successfully")]
        PasswordReset
    }
    public enum UserType
    {
        [Description("System User")]
        SystemUser = 1,
        [Description("Normal User")]
        NormalUser = 2,
        [Description("Corporate User")]
        CorporateUser = 2
    }
    public enum UserStatus
    {
        [Description("Active")]
        Active = 2,
        [Description("Suspended")]
        Suspened = 3,
        [Description("Locked")]
        Locked = 4,
    }
    public enum NotificationType
    {
        [Description("Registered Users")]
        RegisterUser = 1,
        [Description("All Users")]
        AllUser = 2,
        [Description("Selected User")]
        SingleUser = 3
    }
    public enum QueryType
    {
        None,
        NeedToWrapBySelect,
        AlreadyHasWhereClause
    }
    
    public enum RunningNumberType
    {

    }
    
    public enum CollectionMode
    {
        [Description("Branch Collection")]
        BranchCollection = 1,
        [Description("Premium Delivery")]
        PremiumDelivery = 2
    }
    public enum ApprovalStatus
    {
        Pending = 1,
        Verified = 2,
        Approved = 3
    }
    public enum OrderStatus
    {
        [Description("Pending")]
        Pending = 1,
        [Description("Pending Authorization")]
        PendingAuthorization = 2,
        [Description("New")]
        New = 3,
        [Description("Failed")]
        Failed = 4,
        [Description("Verified")]
        Verified = 5,
        [Description("Approved")]
        Approved = 6,
        [Description("Completed")]
        Completed = 7,
        [Description("Amended")]
        Amended = 8,
        [Description("Amendment Requested")]
        AmendmentRequested = 9,
        [Description("Void Requested")]
        VoidRequested = 10,
        [Description("Void Approval")]
        VoidApproval = 11,
        [Description("Void Rejected")]
        VoidRejected = 12,
        [Description("Refund")]
        Refund = 13,
        [Description("Void")]
        Void = 14,
        [Description("CIT")]
        Cit = 15
    }
    public enum OrderPackedStatus
    {
        Pending = 1,
        Packed = 2
    }
    public enum DeviceType
    {
        Android = 1,
        IOS = 2,
        Web = 3,
        Windows = 4,
        Terminal = 5
    }
    public enum MatchType
    {
        [Description("Normal Match")]
        NormalMatch = 1,
        [Description("Confirm Match")]
        ConfirmMatch = 2
    }
    public enum MatchCategory
    {
        [Description("Name Only")]
        NameOnly = 1,
        [Description("Name Nationality and Year of Birth")]
        NameNationalityAndYearOfBirth = 2,
        [Description("Name and Year of Birth")]
        NameAndYearOfBirth = 3,
        [Description("Name and Nationality")]
        NameAndNationality = 4
    }

    public enum PurposeOfOrderType
    {
        [Description("Individual")]
        Individual = 1,
        [Description("Corporate")]
        Corporate = 2,
    }

    public enum DocumentType
    {
        [Description("Foreign User Documents")]
        ForeignUserDocuments = 1,
        [Description("Sole Proprietorship Or Parthership Documents")]
        SoleProprietorshipOrParthershipDocuments = 2,
        [Description("Malaysian User Documents")]
        MalaysianUserDocuments = 3,
        [Description("Order Supporting Documents")]
        OrderSupportingDocuments = 4,
        [Description("Corporate Foreign Company Documents")]
        CorporateForeignCompanyDocuments = 5,
        [Description("Private Limited Company Documents")]
        PrivateLimitedCompanyDocuments = 6,
        [Description("Public Limited Company Documents")]
        PublicLimitedCompanyDocuments = 7,
        [Description("Corporate Clubs Socities And Charities Documents")]
        CorporateClubsSocitiesAndCharitiesDocuments = 8


    }
    public enum ConfigUserType
    {
        [Description("Individual Customer")]
        IndividualCustomer = 1,
        [Description("Corporate Customer")]
        CorporateCustomer = 2
    }

    public enum ConfigCategory
    {
        [Description("User Status")]
        UserStatus = 1,
        [Description("Gender")]
        Gender = 2,
        [Description("User Type")]
        UserType = 3,
        [Description("Currency Order Type")]
        CurrencyOrderType = 4,
        [Description("Occupation Type")]
        OccupationType = 5,
        [Description("Residential Status")]
        ResidentialStatus = 6,
        [Description("InformationS ource")]
        InformationSource = 7,
        [Description("Fund Source")]
        FundSource = 8,
       
    }
}
