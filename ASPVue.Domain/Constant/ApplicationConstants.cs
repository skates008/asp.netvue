﻿namespace ASPVueProject.Domain.Constant
{
    public class ApplicationConstants
    {
        public const string SuperAdminRoleGuid = "1c3dede6-1080-4698-ab7a-69ecddc58a16";
        public const string AdminRoleGuid = "01c64062-2251-4ba1-8340-c463ec9657f1";
        public const string MemberRoleGuid = "1d3dede6-1720-4698-ab7a-70ecddc58a18";
        public const string SuperAdminUserGuid = "1c3dede6-1080-4698-ab7a-69ecddc58a14";
	    public const string AdminUserGuid = "1c3dede6-1080-4698-ab7a-69ecddc58a15";
        public const string SystemAgentGuid = "1c3dede6-1080-4698-ab7a-69ecddc58a17";
    }
}
