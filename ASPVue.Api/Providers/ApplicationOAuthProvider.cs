﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using ASPVueProject.Api.Identity;
using ASPVueProject.Data;
using ASPVueProject.DTO.Configuration;
using ASPVueProject.Domain.Enum;
using WebGrease.Css.Extensions;
using System.IO;
using System.Web;
using ASPVueProject.Domain.Entity;
using ASPVueProject.DTO.ViewModel;
using ASPVueProject.Common.Configuration;
using ASPVueProject.Api.Helpers;

namespace ASPVueProject.Api.Providers
{
	public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
	{
		private readonly string _publicClientId;

		public ApplicationOAuthProvider(string publicClientId)
		{
			_publicClientId = publicClientId ?? throw new ArgumentNullException($"publicClientId");
		}
		public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
		{
			var appContext = ApplicationContext.Create();
			try
			{
				context.Request.Body.Position = 0;
				var reader = new StreamReader(context.Request.Body);
				var body = reader.ReadToEnd();
				var parsed = HttpUtility.ParseQueryString(body);
				var isBoUser = bool.Parse(parsed["isBOUser"]);
				var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
				var user = await userManager.FindAsync(context.UserName, context.Password);
				var mapper = new DiObjectMapper();

				var logViewModel = new ApplicationLogViewModel()
				{
					Action = "Login",
					Data = user,
					Entity = "User"
				};

				var appLog = mapper.Map<ApplicationLogViewModel, ApplicationLog>(logViewModel);
				appLog.IpAddress = ServiceRequest.ClientIp;
				appLog.LogType = LogType.System;
				appLog.Date = GeneralService.CurrentDate();
				logViewModel.Data = new { context.UserName };


				if (user == null)
				{
					appLog.Description = "Invalid username or password";
					appContext.ApplicationLogs.Add(appLog);
					appContext.SaveChanges();
					context.SetError("invalid_grant", "The username or password is incorrect.");
					return;
				}

				//if (user.IsBackOfficeUser != isBoUser)
				//{
				//	appLog.Description = "Invalid username or password";
				//	appContext.ApplicationLogs.Add(appLog);
				//	appContext.SaveChanges();
				//	context.SetError("invalid_grant", "The username or password is incorrect.");
				//	return;
				//}

				//if (!user.IsBackOfficeUser && !user.EmailConfirmed)
				//{
				//	appLog.Description = "Email not confirmed.";
				//	appContext.ApplicationLogs.Add(appLog);
				//	appContext.SaveChanges();
				//	context.SetError("invalid_grant", "Your account is not active. please review you inbox.");
				//	return;
				//}
				if (user.LockoutEnabled && user.AccessFailedCount >= 3 || user.Status == UserStatus.Locked)
				{
					appLog.Description = "Account is locked.";
					appContext.ApplicationLogs.Add(appLog);
					appContext.SaveChanges();
					context.SetError("invalid_grant", "Your account has been locked. Please contact the administrator!");
					return;
				}
				if (user.Status == UserStatus.Suspened)
				{
					appLog.Description = "Account is suspened.";
					appContext.ApplicationLogs.Add(appLog);
					appContext.SaveChanges();
					context.SetError("invalid_grant", "Your account has been suspened. Please contact the administrator!");
					return;
				}

				userManager.UpdateSecurityStamp(user.Id);

				appLog.UserId = user.Id;
				appLog.Description = "Login successful.";
				appContext.ApplicationLogs.Add(appLog);
				appContext.SaveChanges();

				var loginInfo = new LoginInfo()
				{
					UserId = user.Id,
					FullName = user.FullName,
					DisplayPicture = string.IsNullOrEmpty(user.DisplayPicture) ? "../../static/img/noimage.jpg" : user.DisplayPicture,
					RoleId = user.Roles.Select(c => c.RoleId).FirstOrDefault(),
					Role = user.Roles.Select(c => c.Role.Name).FirstOrDefault(),
					RoleHierarchy = user.Roles.Select(c => c.Role.Hierarchy).FirstOrDefault()
				};

                var loginView = new LoginInfoWeb()
                {
                    UserId = user.Id,
					DisplayPicture = loginInfo.DisplayPicture,
					FullName = loginInfo.FullName,
					Role = loginInfo.Role,
					RoleId = loginInfo.RoleId,
					ForceChangePassword = user.ForceChangePassword
				};
				var loginInfoValue = Newtonsoft.Json.JsonConvert.SerializeObject(loginView);

				#region Menus
				var pageTreeValue = string.Empty;
				if (isBoUser)
				{
					var menus = new List<UserPageViewModel>();
					var userwisePage = appContext.UserPages.Distinct().Where(c => c.RoleId == loginInfo.RoleId && c.Page.ShowInView).Select(p => new UserPageViewModel()
					{
						PageDisplayOrder = p.Page.DisplayOrder,
						PageIcon = p.Page.Icon,
						PageId = p.Page.Id,
						PageParentPageId = p.Page.ParentPageId,
						PagePath = p.Page.Path,
						PageTitle = p.Page.Title,
						url = p.Page.Path,
						name = p.Page.Title,
						icon = "fa " + p.Page.Icon
					}).ToList();
					userwisePage.Where(c => c.PageParentPageId == null).OrderBy(c => c.PageDisplayOrder).ForEach(p =>
						  {
							  p.ChildPages = GetChildPages(userwisePage, p.PageId);
							  p.children = GetChildPages(userwisePage, p.PageId);
							  menus.Add(p);
						  });
					pageTreeValue = Newtonsoft.Json.JsonConvert.SerializeObject(menus);
				}

				#endregion
				IDictionary<string, string> authProp = new Dictionary<string, string>()
				{
					{ "UserName", user.FullName },
					{ "LoginInfo", loginInfoValue},
					{ "UserPages", pageTreeValue}
				};
				var claims = new List<Claim>()
				{
					new Claim("LoginInfo",loginInfoValue),
					new Claim("securitystamp",user.SecurityStamp)
				};
				var oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, OAuthDefaults.AuthenticationType, claims);
				var cookiesIdentity = await user.GenerateUserIdentityAsync(userManager, CookieAuthenticationDefaults.AuthenticationType, claims);
				var properties = new AuthenticationProperties(authProp);
				var ticket = new AuthenticationTicket(oAuthIdentity, properties);
				context.Validated(ticket);
				context.Request.Context.Authentication.SignIn(cookiesIdentity);
			}
			catch (Exception ex)
			{
				context.SetError("invalid_grant", "The username or password is incorrect.");
			}
		}

		public override Task TokenEndpoint(OAuthTokenEndpointContext context)
		{
			foreach (var property in context.Properties.Dictionary)
			{
				context.AdditionalResponseParameters.Add(property.Key, property.Value);
			}
			return Task.FromResult<object>(null);
			//return base.TokenEndpoint(context);
		}
		public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
		{
			// Resource owner password credentials not provide a client ID.
			if (context.ClientId == null)
				context.Validated();
			return Task.FromResult<object>(null);
		}
		public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
		{
			if (context.ClientId == _publicClientId)
			{
				Uri expectedRootUri = new Uri(context.Request.Uri, "/");
				if (expectedRootUri.AbsoluteUri == context.RedirectUri)
				{
					context.Validated();
				}
			}
			return Task.FromResult<object>(null);
		}
		public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
		{
			var newId = new ClaimsIdentity(context.Ticket.Identity);
			var newTicket = new AuthenticationTicket(newId, context.Ticket.Properties);
			context.Validated(newTicket);
			return Task.FromResult<object>(null);
		}
		private List<UserPageViewModel> GetChildPages(List<UserPageViewModel> pages, int? parentPageId)
		{
			var childpages = pages.Where(c => c.PageParentPageId == parentPageId).OrderBy(c => c.PageDisplayOrder);
			return childpages
				.Select(c => new UserPageViewModel()
				{
					children = GetChildPages(pages, c.PageId),
					PageDisplayOrder = c.PageDisplayOrder,
					icon = "fa " + c.PageIcon,
					PageId = c.PageId,
					PageParentPageId = c.PageParentPageId,
					url = c.PagePath,
					name = c.PageTitle
				}).ToList();
		}
	}

}

