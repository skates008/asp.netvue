﻿using System.Collections.Specialized;
using System.Linq;
using System.Web;
using ASPVueProject.Api.Identity;
using ASPVueProject.DTO.ViewModel;

namespace ASPVueProject.Api.Helpers
{
	public static class ServiceRequest
	{
		public static string BuildUrl(string urlSegment, NameValueCollection paramsCollection)
		{
			var array = (from key in paramsCollection.AllKeys
						 from value in paramsCollection.GetValues(key)
						 select string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value)))
				.ToArray();
			var url = ApplicationUrl + urlSegment + "?" + string.Join("&", array);
			return url;
		}
		public static string ClientIp => HttpContext.Current.Request.Headers["ClientIp"];
		public static string DeviceType => HttpContext.Current.Request.Headers["DeviceType"];
		public static string DeviceKey => HttpContext.Current.Request.Headers["DeviceKey"];
		public static string ApplicationUrl => HttpContext.Current.Request.Headers["ApplicationUrl"];
		public static ServiceRequestModel<T> ToServiceRequestModel<T>(this T model)
        {
            var serviceRequestModel = new ServiceRequestModel<T>()
            {
                Model = model,
                IpAddress = ClientIp,
                LoginInfo = HttpContext.Current.User.Identity.GetUserLoginInfo()
            };
            return serviceRequestModel;
        }
        public static CurrentUserInfo CurrentUserInfo()
        {
            var currentUserInfoModel = new CurrentUserInfo()
            {
                IpAddress = ClientIp,
                LoginInfo = HttpContext.Current.User.Identity.GetUserLoginInfo()
            };
            return currentUserInfoModel;
        }
    }
}

