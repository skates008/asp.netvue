﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Filters;
using ASPVueProject.Api.Helpers;
using ASPVueProject.DTO.ViewModel;
using ASPVueProject.Service.Interface;

namespace ASPVueProject.Api.Configuration
{
	public class CustomExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {

            try
            {
                if (actionExecutedContext.Exception.GetType().Name != "ArgumentNullException")
                {

                    var errorLogServiceLog =
                        actionExecutedContext.Request.GetDependencyScope().GetService(typeof (IErrorLogService)) as
                            IErrorLogService;

                    var model = new ErrorLogViewModel()
                    {
                        Action = actionExecutedContext.ActionContext.ActionDescriptor.ActionName,
                        Data = actionExecutedContext.ActionContext.ActionArguments.First().Value,
                        Entity = actionExecutedContext.ActionContext.ActionArguments.First().Value.GetType().FullName,
                        Message = actionExecutedContext.Exception.Message,
                        InnerException = actionExecutedContext.Exception.InnerException?.Message,
                        ErrorCode = actionExecutedContext.Exception.GetType().Name
                    };
                    errorLogServiceLog?.Add(model.ToServiceRequestModel());
                }

            }
            catch (Exception)
            {
                // ignored
            }

            base.OnException(actionExecutedContext);
        }
    }
}