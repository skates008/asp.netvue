﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Filters;
using ASPVueProject.Api.Helpers;
using ASPVueProject.Common;
using ASPVueProject.Domain.Enum;
using ASPVueProject.DTO.ViewModel;
using ASPVueProject.Service.Interface;

namespace ASPVueProject.Api.Configuration
{
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public class ActivityLogAttribute : ActionFilterAttribute
	{
		private IApplicationLogService ServiceActivityLog { get; set; }

		private string LogDescription { get; set; }
		private bool ForceLog { get; set; }
		public ActivityLogAttribute(LogDescription logDescription, bool forceLog = false)
		{
			ForceLog = forceLog;
			LogDescription = logDescription.ToDescription();
		}
		public ActivityLogAttribute(string logDescription, bool forceLog = false)
		{
			ForceLog = forceLog;
			LogDescription = logDescription;
		}
		public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
		{
			try
			{
				if (actionExecutedContext.Exception == null)
				{
					HandleLog(actionExecutedContext);
				}

			}
			catch
			{
				// ignored
			}
			base.OnActionExecuted(actionExecutedContext);
		}
		private void HandleLog(HttpActionExecutedContext actionExecutedContext)
		{
			try
			{
				ServiceActivityLog = actionExecutedContext.Request.GetDependencyScope().GetService(typeof(IApplicationLogService)) as IApplicationLogService;

				if (ServiceActivityLog == null)
					throw new ArgumentException("ActivityLog Service not working.");

				var option = LogDescription;
				if (option == null)
					return;

				var model = new ApplicationLogViewModel()
				{
					Action = actionExecutedContext.ActionContext.ActionDescriptor.ActionName,
					Data = actionExecutedContext.ActionContext.ActionArguments.First().Value,
					Entity = actionExecutedContext.ActionContext.ActionArguments.First().Value.GetType().FullName,
					Description = LogDescription,
					LogType = LogType.Activity
				};



				ServiceActivityLog.AddActivityApplicationLog(model.ToServiceRequestModel());
			}
			catch
			{
				// ignored
			}
		}
	}
}