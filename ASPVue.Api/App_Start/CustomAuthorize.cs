﻿using ASPVueProject.Api.Helpers;
using ASPVueProject.Api.Identity;
using ASPVueProject.Common.Configuration;
using ASPVueProject.Common.Helper;
using ASPVueProject.Data;
using ASPVueProject.Domain.Enum;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace ASPVueProject.Api.App_Start
{
    public class CustomAuthorize : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any() ||
                actionContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any())
                return;

            var userId = HttpContext.Current.User.Identity.GetUserGuid();
            if (userId == default(Guid))
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            var context = ApplicationContext.Create();
            var userDetail = context.Users.FirstOrDefault(x => x.Id == userId);
            if (!userDetail.EmailConfirmed)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
            if (userDetail.AccessFailedCount >= 3)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            base.OnAuthorization(actionContext);
        }
    }
}