using System;
using System.Linq;
using System.Linq.Dynamic;
using System.Reflection;
using System.Web.Compilation;
using Microsoft.Practices.Unity;
using System.Web.Http;
using Unity.WebApi;

namespace ASPVueProject.Api
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterTypes(AllClasses.FromAssemblies(
                    BuildManager.GetReferencedAssemblies().Cast<Assembly>()
                ).Where(a => a.Namespace !=null && (a.Namespace.StartsWith("ASPVueProject.Data") || a.Namespace.StartsWith("ASPVueProject.Service") || a.Namespace.StartsWith("ASPVueProject.DTO") || a.Namespace.StartsWith("ASPVueProject.Mailing")))
                , WithMappings.FromMatchingInterface, WithName.Default
                , WithLifetime.Hierarchical, overwriteExistingMappings: true
                );

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}