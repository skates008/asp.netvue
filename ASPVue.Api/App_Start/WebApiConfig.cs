﻿using System.Web.Http;
using ASPVueProject.Api.Configuration;
using System.Web.Http.ExceptionHandling;
using Elmah.Contrib.WebApi;

namespace ASPVueProject.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // config.EnableCors();

            config.Formatters.Remove(config.Formatters.XmlFormatter);

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Serialize;
            config.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None;
            //config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            // Web API routes
            config.MapHttpAttributeRoutes();
            // config.Routes.MapHttpRoute("AXD", "{resource}.axd/{*pathInfo}", null, null, new StopRoutingHandler());
            //  config.Routes.MapHttpRoute("axd", "{resource}.axd/{*pathInfo}", null, null, new StopRoutingHandler());

            config.Filters.Add(new ElmahHandleErrorApiAttribute());
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Filters.Add(new CustomExceptionFilter());
            config.Services.Add(typeof(IExceptionLogger), new ElmahExceptionLogger());

        }
    }
}
