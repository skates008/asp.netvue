﻿using System.Web.Http;
using ASPVueProject.Service.Interface;
using ASPVueProject.Api.App_Start;
using System;


namespace ASPVueProject.Api.Controllers
{
    [RoutePrefix("api/list")]
    [CustomAuthorize]
    public class ListController : ApiController
    {
        private IListService ListService { get; set; }
        public ListController(IListService listService)
        {
            ListService = listService;
        }
        [HttpGet]
        [Route("role")]
        public IHttpActionResult ListRole()
        {
            return Ok(ListService.ListRole());
        }

        [HttpGet]
        [Route("userstatus")]
        public IHttpActionResult ListUserStatus()
        {
            return Ok(ListService.ListUserStatus());
        }
        [HttpGet]
        [Route("countrylist")]
        public IHttpActionResult ListCountry()
        {
            return Ok(ListService.ListAllCountry());
        }

        [HttpGet]
        [Route("statelist")]
        public IHttpActionResult ListState()
        {
            return Ok(ListService.ListAllState());
        }

        [HttpGet]
        [Route("state/get/{id}")]
        public IHttpActionResult ListStateByCountryId(Guid Id)
        {
            return Ok(ListService.ListStateByCountryId(Id));
        }


        [HttpGet]
        [Route("citylist/get/{id}")]
        public IHttpActionResult ListCity(Guid Id)
        {
            return Ok(ListService.ListCityByStateId(Id));
        }
        [HttpGet]
        [Route("postcode/get/{id}")]
        public IHttpActionResult ListPostCode(Guid Id)
        {
            return Ok(ListService.ListPostCodeByCityId(Id));
        }
        [HttpGet]
        [Route("purposeType")]
        public IHttpActionResult ListPurposeOfOrderType()
        {
            return Ok(ListService.ListPurposeOfOrderType());
        }

        [HttpGet]
        [Route("documentType")]
        public IHttpActionResult ListDocumentType()
        {
            return Ok(ListService.ListDocumentType());
        }

        

       
        [HttpGet]
        [Route("collectionmode")]
        public IHttpActionResult ListCollectionMode()
        {
            return Ok(ListService.ListCollectionMode());
        }

        [HttpGet]
        [Route("postcodegroup")]
        public IHttpActionResult ListPostCodeGroup()
        {
            return Ok(ListService.ListPostCodeGroup());
        }
    }
}
