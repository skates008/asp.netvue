﻿using System;
using System.Web.Http;
using ASPVueProject.Service.Interface;
using ASPVueProject.Domain.Enum;
using ASPVueProject.Api.Configuration;
using ASPVueProject.Api.Helpers;
using ASPVueProject.DTO.ViewModel;
using System.Collections.Generic;

namespace ASPVueProject.Api.Controllers
{
    [RoutePrefix("api/settings")]

    public class SettingsController : ApiController
    {
        private ISettingsService SettingsService { get; set; }

        public SettingsController(ISettingsService settingsService)
        {
            SettingsService = settingsService;

        }

        #region Purpose Of Order
        [HttpPost]
        [Route("purposeoforder/list")]
        public IHttpActionResult ListPurposeOfOrder(KendoDataRequest kendoDataRequest)
        {
            return Ok(SettingsService.ListPurposeOfOrder(kendoDataRequest.ToServiceRequestModel()));
        }
        [HttpPost]
        [Route("purposeoforder/add")]
        [ActivityLog(LogDescription.Add)]
        public IHttpActionResult AddPurposeOfOrder(PurposeOfOrderViewModel model)
        {
            model.Id = Guid.NewGuid();
            SettingsService.AddPurposeOfOrder(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpPost]
        [Route("purposeoforder/update")]
        [ActivityLog(LogDescription.Update)]
        public IHttpActionResult UpdatePurposeOfOrder(PurposeOfOrderViewModel model)
        {
            SettingsService.UpdatePurposeOfOrder(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpGet]
        [Route("purposeoforder/get/{id}")]
        public IHttpActionResult GetPurposeOfOrder(Guid id)
        {
            return Ok(SettingsService.GetPurposeOfOrder(id.ToServiceRequestModel()));
        }

        [HttpGet]
        [Route("purposeoforder/delete/{id}")]
        [ActivityLog(LogDescription.Delete)]
        public IHttpActionResult DeletePurposeOfOrder(Guid id)
        {
            SettingsService.DeletePurposeOfOrder(id.ToServiceRequestModel());
            return Ok();
        }
        #endregion

        #region Nature Of Business
        [HttpPost]
        [Route("natureofbusiness/list")]
        public IHttpActionResult ListNatureOfBusiness(KendoDataRequest kendoDataRequest)
        {
            return Ok(SettingsService.ListNatureOfBusiness(kendoDataRequest.ToServiceRequestModel()));
        }
        [HttpPost]
        [Route("natureofbusiness/add")]
        [ActivityLog(LogDescription.Add)]
        public IHttpActionResult AddNatureOfBusiness(NatureOfBusinessViewModel model)
        {
            model.Id = Guid.NewGuid();
            SettingsService.AddNatureOfBusiness(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpPost]
        [Route("natureofbusiness/update")]
        [ActivityLog(LogDescription.Update)]
        public IHttpActionResult UpdateNatureOfBusiness(NatureOfBusinessViewModel model)
        {
            SettingsService.UpdateNatureOfBusiness(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpGet]
        [Route("natureofbusiness/get/{id}")]
        public IHttpActionResult GetNatureOfBusiness(Guid id)
        {
            return Ok(SettingsService.GetNatureOfBusiness(id.ToServiceRequestModel()));
        }

        [HttpGet]
        [Route("natureofbusiness/delete/{id}")]
        [ActivityLog(LogDescription.Delete)]
        public IHttpActionResult DeleteNatureOfBusiness(Guid id)
        {
            SettingsService.DeleteNatureOfBusiness(id.ToServiceRequestModel());
            return Ok();
        }
        #endregion
        
        #region Secret Question
        [HttpPost]
        [Route("secretquestion/list")]
        public IHttpActionResult ListSecretQuestion(KendoDataRequest kendoDataRequest)
        {
            return Ok(SettingsService.ListSecretQuestion(kendoDataRequest.ToServiceRequestModel()));
        }
        [HttpPost]
        [Route("secretquestion/add")]
        [ActivityLog(LogDescription.Add)]
        public IHttpActionResult AddSecretQuestion(SecretQuestionViewModel model)
        {
            model.Id = Guid.NewGuid();
            SettingsService.AddSecretQuestion(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpPost]
        [Route("secretquestion/update")]
        [ActivityLog(LogDescription.Update)]
        public IHttpActionResult UpdateSecretQuestion(SecretQuestionViewModel model)
        {
            SettingsService.UpdateSecretQuestion(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpGet]
        [Route("secretquestion/get/{id}")]
        public IHttpActionResult GetSecretQuestion(Guid id)
        {
            return Ok(SettingsService.GetSecretQuestion(id.ToServiceRequestModel()));
        }

        [HttpGet]
        [Route("secretquestion/delete/{id}")]
        [ActivityLog(LogDescription.Delete)]
        public IHttpActionResult DeleteSecretQuestion(Guid id)
        {
            SettingsService.DeleteSecretQuestion(id.ToServiceRequestModel());
            return Ok();
        }
        #endregion
    }
}
