﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using ASPVueProject.Api.Helpers;
using ASPVueProject.Api.Identity;
using ASPVueProject.DTO.Configuration;
using ASPVueProject.Mailing.Interfaces;
using ASPVueProject.Mailing.Models;
using ASPVueProject.Service.Interface;
using ASPVueProject.DTO.ViewModel;
using ASPVueProject.Domain.Entity;

namespace ASPVueProject.Api.Controllers
{
	[RoutePrefix("api/account")]
    public class AccountController : ApiController
    {
        private ApplicationUserManager _userManager;
        private readonly IEmailComposer _emailComposer;
        private readonly IEmailer _emailer;
        private readonly IDiObjectMapper _mapper;
        private readonly IApplicationLogService _applicationLogService;

        public AccountController(IEmailComposer emailComposer, IEmailer emailer,
            IDiObjectMapper mapper,
            IApplicationLogService applicationLogService)
        {
            _emailComposer = emailComposer;
            _emailer = emailer;
            _mapper = mapper;
            _applicationLogService = applicationLogService;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                if (_userManager == null)
                {
                    _userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    _userManager.EmailComposer = _emailComposer;
                    _userManager.EmailService = new IdentityEmailComposer(_emailer);
                }
                return _userManager;
            }
            private set
            {
                _userManager = value;
            }
        }
        
        [Route("signout")]
        [AllowAnonymous]
        public IHttpActionResult Logout()
        {
            var userId = User.Identity.GetUserGuid();
            if (userId != Guid.Empty)
            {
                var model = new ApplicationLogViewModel()
                {
                    Action = System.Reflection.MethodBase.GetCurrentMethod().Name,
                    Data = new { UserId = User.Identity.GetUserGuid() },
                    Entity = "User",
                    Description = userId == Guid.Empty ? "Session out" : "User log out"
                };

                _applicationLogService.AddSystemApplicationLog(model.ToServiceRequestModel());
            }
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }
        [HttpGet]
        [Route("forgotpassword")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ForgetPassword(string email)
        {
            var user = await UserManager.FindByEmailAsync(email);
            if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                return Ok();
            var uservm = _mapper.Map<User, UserViewModel>(user);
            await UserManager.SendPasswordResetEmail(uservm);
            return Ok();
        }

        [HttpGet]
        [Route("user/role")]
        // header token
        [AllowAnonymous]
        public async Task<IHttpActionResult> UserRole(string email)
        {
            var user = await UserManager.FindByEmailAsync(email);
            //var role = user?.UserRoles.FirstOrDerfault().Id ?? null;
            return Ok();
        }

        [HttpGet]
        [Route("user/authenticate")]
        // header token
        [AllowAnonymous]
        public async Task<IHttpActionResult> UserRole(SignInViewModel model)
        {
            var user = await UserManager.FindAsync(model.Email, model.Password);
            //var role = user?.UserRoles.FirstOrDerfault().Id ?? null;
            return Ok();
        }

        [Route("resetpassword")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ResetPassword(ResetPasswordModel model)
        {
            var logModel = new ApplicationLogViewModel()
            {
                Action = System.Reflection.MethodBase.GetCurrentMethod().Name,
                Entity = "User",
                Data = model
            };

            var code = System.Web.HttpUtility.UrlDecode(model.Code)?.Replace(" ", "+");
            model.UserId = System.Web.HttpUtility.UrlDecode(model.UserId)?.Replace(" ", "+");
            if (string.IsNullOrEmpty(code) || string.IsNullOrEmpty(model.UserId))
            {
                logModel.Description = "Invalid user or code";
                _applicationLogService.AddSystemApplicationLog(logModel.ToServiceRequestModel());
                return BadRequest("Invalid user or code");
            }
            var userId = new Guid(model.UserId);
            var user = await UserManager.FindByIdAsync(userId);
            if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                return Ok();
            var messsage = await UserManager.ResetPasswordAsync(user.Id, code, model.NewPassword);
            if (messsage.Succeeded)
            {
                var uservm = _mapper.Map<User, UserViewModel>(user);
                var er = new EmailRequest<UserViewModel>(uservm);
                er.To.Add(new System.Net.Mail.MailAddress(uservm.Username));
                er.Subject = "ASPVueProject : Password Reset Confirmation";
                _emailComposer.SendEmail(er, EmailTemplate.PasswordChangeConfirmation);

                logModel.Description = "User password reset successfully";
                _applicationLogService.AddSystemApplicationLog(logModel.ToServiceRequestModel());
                return Ok();
            }
            else
            {
                logModel.Description = messsage.Errors.SingleOrDefault();
                _applicationLogService.AddSystemApplicationLog(logModel.ToServiceRequestModel());
                return BadRequest(messsage.Errors.SingleOrDefault());
            }
        }
        [Route("changepassword")]
        public IHttpActionResult ChangePassword(ChangePasswordViewModel model)
        {
            var response = UserManager.ChangePassword(model.UserId, model.OldPassword, model.NewPassword);
            if (response.Succeeded)
                return Ok();
            return BadRequest(response.Errors.SingleOrDefault());
        }
        [HttpGet]
        [Route("confirmemail")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ConfirmEmail(string userId, string code)
        {
            var logModel = new ApplicationLogViewModel()
            {
                Action = System.Reflection.MethodBase.GetCurrentMethod().Name,
                Entity = "User",
                Data = new { userId, code }
            };
            code = System.Web.HttpUtility.UrlDecode(code)?.Replace(" ", "+");
            userId = System.Web.HttpUtility.UrlDecode(userId)?.Replace(" ", "+");
            if (string.IsNullOrEmpty(code) || string.IsNullOrEmpty(userId))
            {
                logModel.Description = "Invalid user or code";
                _applicationLogService.AddSystemApplicationLog(logModel.ToServiceRequestModel());
                return BadRequest("Invalid user or code");
            }
            var userGuid = new Guid(userId);
            var user = await UserManager.FindByIdAsync(userGuid);
            if (user == null || (await UserManager.IsEmailConfirmedAsync(user.Id)))
                return Ok();

            var messsage = await UserManager.ConfirmEmailAsync(userGuid, code);
            if (messsage.Succeeded)
            {
                logModel.Description = "User email verified successfully";
                _applicationLogService.AddSystemApplicationLog(logModel.ToServiceRequestModel());
                return Ok();
            }
            logModel.Description = messsage.Errors.SingleOrDefault();
            _applicationLogService.AddSystemApplicationLog(logModel.ToServiceRequestModel());
            return BadRequest(messsage.Errors.SingleOrDefault());
        }
        

        #region Helpers
        private IAuthenticationManager Authentication => Request.GetOwinContext().Authentication;

        #endregion
    }
}
