﻿using System;
using System.Web.Http;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.AspNet.Identity.Owin;
using ASPVueProject.Api.Identity;
using ASPVueProject.Mailing.Interfaces;
using ASPVueProject.Service.Interface;
using ASPVueProject.Domain.Enum;
using ASPVueProject.Api.Configuration;
using ASPVueProject.Api.Helpers;
using ASPVueProject.DTO.ViewModel;


namespace ASPVueProject.Api.Controllers
{
    [RoutePrefix("api/address")]
    public class AddressSetupController : ApiController
    {

        private IAddressSetupService _addresssetUpService { get; set; }

        public AddressSetupController(IAddressSetupService addresssetUpService)
        {
            _addresssetUpService = addresssetUpService;

        }

        #region Country Setup
        [HttpPost]
        [Route("country/list")]
        public IHttpActionResult ListCountry(KendoDataRequest kendoDataRequest)
        {
            return Ok(_addresssetUpService.ListCountry(kendoDataRequest.ToServiceRequestModel()));
        }
        [HttpPost]
        [Route("country/add")]
        [ActivityLog(LogDescription.Add)]
        public IHttpActionResult AddCountry(CountryViewModel model)
        {
            _addresssetUpService.AddCountry(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpPost]
        [Route("country/update")]
        [ActivityLog(LogDescription.Update)]
        public IHttpActionResult UpdateCountry(CountryViewModel model)
        {
            _addresssetUpService.UpdateCountry(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpGet]
        [Route("country/get/{id}")]
        public IHttpActionResult GetCountry(Guid id)
        {
            return Ok(_addresssetUpService.GetCountry(id.ToServiceRequestModel()));
        }

        [HttpGet]
        [Route("country/delete/{id}")]
        [ActivityLog(LogDescription.Delete)]
        public IHttpActionResult DeleteRole(Guid id)
        {
            _addresssetUpService.DeleteCountry(id.ToServiceRequestModel());
            return Ok();
        }
        #endregion

        #region SateProvince Setup
        [Route("state/list")]
        public IHttpActionResult ListState(KendoDataRequest kendoDataRequest)
        {
            return Ok(_addresssetUpService.ListState(kendoDataRequest.ToServiceRequestModel()));
        }
        [HttpPost]
        [Route("state/add")]
        [ActivityLog(LogDescription.Add)]
        public IHttpActionResult AddState(StateProvinceViewModel model)
        {
            _addresssetUpService.AddState(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpPost]
        [Route("state/update")]
        [ActivityLog(LogDescription.Update)]
        public IHttpActionResult UpdateState(StateProvinceViewModel model)
        {
            _addresssetUpService.UpdateState(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpGet]
        [Route("state/get/{id}")]
        public IHttpActionResult GetState(Guid id)
        {
            return Ok(_addresssetUpService.GetState(id.ToServiceRequestModel()));
        }

        [HttpGet]
        [Route("state/delete/{id}")]
        [ActivityLog(LogDescription.Delete)]
        public IHttpActionResult DeleteState(Guid id)
        {
            _addresssetUpService.DeleteState(id.ToServiceRequestModel());
            return Ok();
        }
        #endregion

        #region City Setup
        [Route("city/list")]
        public IHttpActionResult ListCity(KendoDataRequest kendoDataRequest)
        {
            return Ok(_addresssetUpService.ListCity(kendoDataRequest.ToServiceRequestModel()));
        }
        [HttpPost]
        [Route("city/add")]
        [ActivityLog(LogDescription.Add)]
        public IHttpActionResult AddCity(CityViewModel model)
        {
            _addresssetUpService.AddCity(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpPost]
        [Route("city/update")]
        [ActivityLog(LogDescription.Update)]
        public IHttpActionResult UpdateCity(CityViewModel model)
        {
            _addresssetUpService.UpdateCity(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpGet]
        [Route("city/get/{id}")]
        public IHttpActionResult GetCity(Guid id)
        {
            return Ok(_addresssetUpService.GetCity(id.ToServiceRequestModel()));
        }

        [HttpGet]
        [Route("city/delete/{id}")]
        [ActivityLog(LogDescription.Delete)]
        public IHttpActionResult DeleteCity(Guid id)
        {
            _addresssetUpService.DeleteCity(id.ToServiceRequestModel());
            return Ok();
        }
        #endregion

        #region Post Code Setup
        [Route("postcode/list")]
        public IHttpActionResult ListPostCode(KendoDataRequest kendoDataRequest)
        {
            return Ok(_addresssetUpService.ListPostCode(kendoDataRequest.ToServiceRequestModel()));
        }
        [HttpPost]
        [Route("postcode/add")]
        [ActivityLog(LogDescription.Add)]
        public IHttpActionResult AddPostCode(PostCodeViewModel model)
        {
            _addresssetUpService.AddPostCode(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpPost]
        [Route("postcode/update")]
        [ActivityLog(LogDescription.Update)]
        public IHttpActionResult UpdatePostCode(PostCodeViewModel model)
        {
            _addresssetUpService.UpdatePostCode(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpGet]
        [Route("postcode/get/{id}")]
        public IHttpActionResult GetPostCode(Guid id)
        {
            return Ok(_addresssetUpService.GetPostCode(id.ToServiceRequestModel()));
        }

        [HttpGet]
        [Route("postcode/delete/{id}")]
        [ActivityLog(LogDescription.Delete)]
        public IHttpActionResult DeletePostCode(Guid id)
        {
            _addresssetUpService.DeletePostCode(id.ToServiceRequestModel());
            return Ok();
        }
        #endregion

        #region Suburb Setup
        [Route("suburb/list")]
        public IHttpActionResult ListSuburb(KendoDataRequest kendoDataRequest)
        {
            return Ok(_addresssetUpService.ListSuburb(kendoDataRequest.ToServiceRequestModel()));
        }
        [HttpPost]
        [Route("suburb/add")]
        [ActivityLog(LogDescription.Add)]
        public IHttpActionResult AddSuburb(SuburbViewModel model)
        {
            _addresssetUpService.AddSuburb(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpPost]
        [Route("suburb/update")]
        [ActivityLog(LogDescription.Update)]
        public IHttpActionResult UpdateSuburb(SuburbViewModel model)
        {
            _addresssetUpService.UpdateSuburb(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpGet]
        [Route("suburb/get/{id}")]
        public IHttpActionResult GetSuburb(Guid id)
        {
            return Ok(_addresssetUpService.GetSuburb(id.ToServiceRequestModel()));
        }

        [HttpGet]
        [Route("suburb/delete/{id}")]
        [ActivityLog(LogDescription.Delete)]
        public IHttpActionResult DeleteSuburb(Guid id)
        {
            _addresssetUpService.DeleteSuburb(id.ToServiceRequestModel());
            return Ok();
        }
        #endregion
    }
}
