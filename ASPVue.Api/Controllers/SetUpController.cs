﻿using System;
using System.Web.Http;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.AspNet.Identity.Owin;
using ASPVueProject.Api.Identity;
using ASPVueProject.Mailing.Interfaces;
using ASPVueProject.Service.Interface;
using ASPVueProject.Domain.Enum;
using ASPVueProject.Api.Configuration;
using ASPVueProject.Api.Helpers;
using ASPVueProject.DTO.ViewModel;

namespace ASPVueProject.Api.Controllers
{
	[RoutePrefix("api/setup")]
    public class SetUpController : ApiController
    {
        private ApplicationUserManager _userManager;
        private readonly IEmailComposer _emailComposer;
        private readonly IEmailer _emailer;
        private ISetUpService SetUpService { get; set; }

        public SetUpController(IEmailComposer emailComposer, IEmailer emailer, ISetUpService setUpService)
        {
            _emailComposer = emailComposer;
            _emailer = emailer;
            SetUpService = setUpService;

        }

        #region Authentication

        [Route("user/checkenduser")]
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult SignIn(SignInViewModel model)
        {
            return Ok(SetUpService.SignIn(model.ToServiceRequestModel()));
        }

        #endregion

        #region Role
        [HttpPost]
        [Route("role/list")]
        public IHttpActionResult ListRole(KendoDataRequest kendoDataRequest)
        {
            return Ok(SetUpService.ListRole(kendoDataRequest.ToServiceRequestModel()));
        }
        [HttpPost]
        [Route("role/add")]
        [ActivityLog(LogDescription.Add)]
        public IHttpActionResult AddRole(RoleViewModel model)
        {
            SetUpService.AddRole(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpPost]
        [Route("role/update")]
        [ActivityLog(LogDescription.Update)]
        public IHttpActionResult UpdateRole(RoleViewModel model)
        {
            SetUpService.UpdateRole(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpGet]
        [Route("role/get/{id}")]
        public IHttpActionResult GetRole(Guid id)
        {
            return Ok(SetUpService.GetRole(id.ToServiceRequestModel()));
        }

        [HttpGet]
        [Route("role/delete/{id}")]
        [ActivityLog(LogDescription.Delete)]
        public IHttpActionResult DeleteRole(Guid id)
        {
            SetUpService.DeleteRole(id.ToServiceRequestModel());
            return Ok();
        }
        #endregion

        #region User
        public ApplicationUserManager UserManager
        {
            get
            {
                if (_userManager != null) return _userManager;
                _userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
                _userManager.EmailComposer = _emailComposer;
                _userManager.EmailService = new IdentityEmailComposer(_emailer);
                return _userManager;
            }
            private set
            {
                _userManager = value;
            }
        }
        [HttpPost]
        [Route("user/list")]
        public IHttpActionResult ListUser(KendoDataRequest kendoDataRequest)
        {
            return Ok(SetUpService.ListUser(kendoDataRequest.ToServiceRequestModel()));
        }


        [HttpPost]
        [Route("user/add")]
        [ActivityLog(LogDescription.Add)]
        public async Task<IHttpActionResult> AddUser(UserViewModel model)
        {
            model.Id = Guid.NewGuid();
            var user = SetUpService.AddUser(model.ToServiceRequestModel());
            await UserManager.SendPasswordResetEmail(user);
            return Ok();
        }


        [HttpPost]
        [Route("user/update")]
        [ActivityLog(LogDescription.Update)]
        public IHttpActionResult UpdateUser(UserViewModel model)
        {
            SetUpService.UpdateUser(model.ToServiceRequestModel());
            return Ok();
        }
        [HttpGet]
        [Route("user/get/{id}")]
        public IHttpActionResult GetUser(Guid id)
        {
            return Ok(SetUpService.GetUser(id));
        }
   
      
        [HttpGet]
        [Route("user/delete/{id}")]
        [ActivityLog(LogDescription.Delete)]
        public IHttpActionResult DeleteUser(Guid id)
        {
            SetUpService.DeleteUser(id.ToServiceRequestModel());
            return Ok();
        }

        #endregion User
    }
}
