﻿using System;
using System.Reflection;
using System.Web.Http;
using ASPVueProject.Common.Configuration;
using ASPVueProject.DTO.Configuration;
using ASPVueProject.DTO.ViewModel;

namespace ASPVueProject.Api
{
	public class WebApiApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			AutoMapperSetup.SetupMappers(Assembly.GetAssembly(typeof(UserViewModel)));
			UnityConfig.RegisterComponents();
			GlobalConfiguration.Configure(WebApiConfig.Register);
			// RouteTable.Routes.Ignore("{resource}.axd/{*pathInfo}");

		}
		protected void Application_BeginRequest(object sender, EventArgs e)
		{
			GeneralService.SetCulture();
		}
		protected void Application_Error(object sender, EventArgs e)
		{

		}
	}
}
