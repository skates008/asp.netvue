﻿using System.Web.Http;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Cors;

[assembly: OwinStartup(typeof(ASPVueProject.Api.Startup))]

namespace ASPVueProject.Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
           // app.UseCors(CorsOptions.AllowAll);
            ConfigureAuth(app);
            HttpConfiguration httpConfig = new HttpConfiguration();
            app.UseCors(CorsOptions.AllowAll);            
            app.UseWebApi(httpConfig);
        }
    }
}
