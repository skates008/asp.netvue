﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using Microsoft.AspNet.Identity;
using ASPVueProject.DTO.ViewModel;

namespace ASPVueProject.Api.Identity
{
	public static class IdentityExtensions
    {
        public static Guid GetUserGuid(this IIdentity identity)
        {
            var userId = identity.GetUserId();
            return userId == null ? Guid.Empty : new Guid(userId);
        }
        public static LoginInfo GetUserLoginInfo(this IIdentity identity)
        {
            var loginInfo = GetClaimValue("LoginInfo");
            if (loginInfo == string.Empty)
                return null;
            var loginInfoVal = Newtonsoft.Json.JsonConvert.DeserializeObject<LoginInfo>(loginInfo);
            return loginInfoVal;
        }
        public static LoginInfo GetCurrentUserInfo(this IPrincipal user)
        {
            return user.Identity.GetUserLoginInfo();
        }

        public static string GetSecurityStamp(this IIdentity identity)
        {
            return GetClaimValue("securitystamp");
        }
        private static string GetClaimValue(string claimType)
        {
            var user = HttpContext.Current.User;
            if (user.Identity.IsAuthenticated)
            {
                return (user.Identity as ClaimsIdentity).FindFirstValue(claimType);
            }
            return string.Empty;
        }
    }
}