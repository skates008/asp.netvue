﻿using System;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using ASPVueProject.Api.Helpers;
using ASPVueProject.Mailing.Interfaces;
using ASPVueProject.Mailing.Models;
using ASPVueProject.DTO.ViewModel;

namespace ASPVueProject.Api.Identity
{
	public class IdentityEmailComposer : IIdentityMessageService
    {
        private readonly IEmailer _emailer;
        public IdentityEmailComposer(IEmailer emailer)
        {
            _emailer = emailer;
        }
        public async Task SendAsync(IdentityMessage message)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.Body = message.Body;
                mailMessage.IsBodyHtml = true;
                mailMessage.Subject = message.Subject;
                mailMessage.To.Add(new MailAddress(message.Destination));
                await _emailer.SendEmailAsync(mailMessage);
            }
        }
    }
    public static class IdentityEmailComposerExtensions
    {
        public static async Task SendPasswordResetEmail(this ApplicationUserManager userManager, UserViewModel user, EmailTemplate emailTemplate = EmailTemplate.ResetPassword, string subject = "ASPVueProject : Password reset")
        {
            var code = await userManager.GeneratePasswordResetTokenAsync(user.Id);
            var nvc = new NameValueCollection() { { "code", code }, { "userid", user.Id.ToString() } };
            var model = new EmailRequestWithUrl<UserViewModel>()
            {
                Url = ServiceRequest.BuildUrl("/resetpassword", nvc),
                Model = user
            };
            var mailBody = userManager.EmailComposer.GetMailContent(model, emailTemplate);
            await userManager.SendEmailAsync(user.Id, subject, mailBody);
        }
        public static async Task SendEmailConfirmationEmail(this ApplicationUserManager userManager, Guid id, EmailTemplate emailTemplate = EmailTemplate.EmailConfirmation, string subject = "ASPVueProject : Email Confirmation")
        {
            var code = await userManager.GenerateEmailConfirmationTokenAsync(id);
            var nvc = new NameValueCollection() { { "code", code }, { "userid", id.ToString() } };
            var user = await userManager.FindByIdAsync(id);
            var model = new EmailRequestWithUrl<UserViewModel>()
            {
                Url = ServiceRequest.BuildUrl("/confirmemail", nvc),
                Model = new UserViewModel() { FullName = user.FullName, Id = user.Id, Username = user.UserName }
            };
            var mailBody = userManager.EmailComposer.GetMailContent(model, emailTemplate);
            await userManager.SendEmailAsync(user.Id, subject, mailBody);
        }
    }
}