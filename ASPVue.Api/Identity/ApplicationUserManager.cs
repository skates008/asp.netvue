﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using ASPVueProject.Data;
using ASPVueProject.Mailing.Interfaces;
using ASPVueProject.Domain.Entity;

namespace ASPVueProject.Api.Identity
{
	public class ApplicationUserManager : UserManager<User, Guid>
    {
        public IEmailComposer EmailComposer
        {
            get; set;
        }
        public ApplicationUserManager(IUserStore<User, Guid> store)
            : base(store)
        {
        }
        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new UserStore<User, Role, Guid, UserLogin, UserRole, UserClaim>(ApplicationContext.Create()));
            manager.UserValidator = new UserValidator<User, Guid>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            //Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true
            };
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<User, Guid>(dataProtectionProvider.Create("ASPVueProject"))
                {
                    TokenLifespan = TimeSpan.FromHours(48)
                };
            }
            return manager;
        }
    }
}