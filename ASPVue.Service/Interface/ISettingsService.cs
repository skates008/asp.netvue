﻿using System;
using Kendo.DynamicLinq;
using ASPVueProject.DTO.ViewModel;
using System.Collections.Generic;
using ASPVueProject.Domain.Enum;

namespace ASPVueProject.Service.Interface
{
    public interface ISettingsService
    {
        #region Purpose Of Order
        DataSourceResult ListPurposeOfOrder(ServiceRequestModel<KendoDataRequest> request);
        PurposeOfOrderViewModel AddPurposeOfOrder(ServiceRequestModel<PurposeOfOrderViewModel> serviceRequestModel);
        void UpdatePurposeOfOrder(ServiceRequestModel<PurposeOfOrderViewModel> serviceRequestModel);
        PurposeOfOrderViewModel GetPurposeOfOrder(ServiceRequestModel<Guid> serviceRequestModel);
        void DeletePurposeOfOrder(ServiceRequestModel<Guid> serviceRequestModel);
        #endregion

        #region Nature Of Business
        DataSourceResult ListNatureOfBusiness(ServiceRequestModel<KendoDataRequest> request);
        NatureOfBusinessViewModel AddNatureOfBusiness(ServiceRequestModel<NatureOfBusinessViewModel> serviceRequestModel);
        void UpdateNatureOfBusiness(ServiceRequestModel<NatureOfBusinessViewModel> serviceRequestModel);
        NatureOfBusinessViewModel GetNatureOfBusiness(ServiceRequestModel<Guid> serviceRequestModel);
        void DeleteNatureOfBusiness(ServiceRequestModel<Guid> serviceRequestModel);
        #endregion

        #region Secret Question
        DataSourceResult ListSecretQuestion(ServiceRequestModel<KendoDataRequest> request);
        SecretQuestionViewModel AddSecretQuestion(ServiceRequestModel<SecretQuestionViewModel> serviceRequestModel);
        void UpdateSecretQuestion(ServiceRequestModel<SecretQuestionViewModel> serviceRequestModel);
        SecretQuestionViewModel GetSecretQuestion(ServiceRequestModel<Guid> serviceRequestModel);
        void DeleteSecretQuestion(ServiceRequestModel<Guid> serviceRequestModel);
        #endregion

        

       

    }
}
