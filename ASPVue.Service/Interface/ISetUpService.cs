﻿using System;
using System.Collections.Generic;
using Kendo.DynamicLinq;
using ASPVueProject.DTO.ViewModel;

namespace ASPVueProject.Service.Interface
{
    public interface ISetUpService
    {
        #region Authentication

        SignInResponseViewModel SignIn(ServiceRequestModel<SignInViewModel> request);
        #endregion
        #region Role
        DataSourceResult ListRole(ServiceRequestModel<KendoDataRequest> request);
        void AddRole(ServiceRequestModel<RoleViewModel> serviceRequestModel);
        void UpdateRole(ServiceRequestModel<RoleViewModel> serviceRequestModel);
        RoleViewModel GetRole(ServiceRequestModel<Guid> serviceRequestModel);
        void DeleteRole(ServiceRequestModel<Guid> serviceRequestModel);
        List<TreeViewModel> ListRoleUserPage(ServiceRequestModel<Guid> serviceRequestModel);
        #endregion Role

        #region User
        DataSourceResult ListUser(ServiceRequestModel<KendoDataRequest> request);
        UserViewModel AddUser(ServiceRequestModel<UserViewModel> serviceRequestModel);
        void UpdateUser(ServiceRequestModel<UserViewModel> serviceRequestModel);
        UserViewModel GetUser(Guid id);
        void DeleteUser(ServiceRequestModel<Guid> serviceRequestModel);
        #endregion
    }
}
