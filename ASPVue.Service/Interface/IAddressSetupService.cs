﻿using System;
using System.Collections.Generic;
using Kendo.DynamicLinq;
using ASPVueProject.DTO.ViewModel;

namespace ASPVueProject.Service.Interface
{
    public interface IAddressSetupService
    {
        #region Country Setup
        DataSourceResult ListCountry(ServiceRequestModel<KendoDataRequest> request);
        void AddCountry(ServiceRequestModel<CountryViewModel> serviceRequestModel);
        void UpdateCountry(ServiceRequestModel<CountryViewModel> serviceRequestModel);
        CountryViewModel GetCountry(ServiceRequestModel<Guid> serviceRequestModel);
        void DeleteCountry(ServiceRequestModel<Guid> serviceRequestModel);
        #endregion

        #region State Province Setup
        DataSourceResult ListState(ServiceRequestModel<KendoDataRequest> request);
        void AddState(ServiceRequestModel<StateProvinceViewModel> serviceRequestModel);
        void UpdateState(ServiceRequestModel<StateProvinceViewModel> serviceRequestModel);
        StateProvinceViewModel GetState(ServiceRequestModel<Guid> serviceRequestModel);
        void DeleteState(ServiceRequestModel<Guid> serviceRequestModel);
        #endregion

        #region City Setup
        DataSourceResult ListCity(ServiceRequestModel<KendoDataRequest> request);
        void AddCity(ServiceRequestModel<CityViewModel> serviceRequestModel);
        void UpdateCity(ServiceRequestModel<CityViewModel> serviceRequestModel);
        CityViewModel GetCity(ServiceRequestModel<Guid> serviceRequestModel);
        void DeleteCity(ServiceRequestModel<Guid> serviceRequestModel);

        #endregion

        #region PostCode Setup

        DataSourceResult ListPostCode(ServiceRequestModel<KendoDataRequest> serviceRequestModel);
        void DeletePostCode(ServiceRequestModel<Guid> serviceRequestModel);
        PostCodeViewModel GetPostCode(ServiceRequestModel<Guid> serviceRequestModel);
        void UpdatePostCode(ServiceRequestModel<PostCodeViewModel> serviceRequestModel);
        void AddPostCode(ServiceRequestModel<PostCodeViewModel> serviceRequestModel);

        #endregion

        #region Suburb Setup
        void AddSuburb(ServiceRequestModel<SuburbViewModel> serviceRequestModel);
        DataSourceResult ListSuburb(ServiceRequestModel<KendoDataRequest> serviceRequestModel);
        void UpdateSuburb(ServiceRequestModel<SuburbViewModel> serviceRequestModel);
        SuburbViewModel GetSuburb(ServiceRequestModel<Guid> serviceRequestModel);
        void DeleteSuburb(ServiceRequestModel<Guid> serviceRequestModel);
        #endregion

        #region Street Setup
        void AddStreet(ServiceRequestModel<StreetViewModel> serviceRequestModel);
        DataSourceResult ListStreet(ServiceRequestModel<KendoDataRequest> serviceRequestModel);
        void UpdateStreet(ServiceRequestModel<StreetViewModel> serviceRequestModel);
        StreetViewModel GetStreet(ServiceRequestModel<Guid> serviceRequestModel);
        void DeleteStreet(ServiceRequestModel<Guid> serviceRequestModel);
        #endregion
    }
}
