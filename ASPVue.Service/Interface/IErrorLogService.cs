﻿using ASPVueProject.DTO.ViewModel;

namespace ASPVueProject.Service.Interface
{
	public interface IErrorLogService
    {
        bool Add(ServiceRequestModel<ErrorLogViewModel> serviceModel);
    }
    public interface IApplicationLogService
    {
        bool AddActivityApplicationLog(ServiceRequestModel<ApplicationLogViewModel> serviceModel);
        bool AddSystemApplicationLog(ServiceRequestModel<ApplicationLogViewModel> serviceModel);
    }
}
