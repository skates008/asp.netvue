﻿using System.Collections.Generic;
using ASPVueProject.DTO.ViewModel;
using System;

namespace ASPVueProject.Service.Interface
{
    public interface IListService
    {
        IList<ListViewModel> ListRole();
        IList<ListViewModel> ListUserStatus();
        IList<ListViewModel> ListPurposeOfOrderType();
        IList<ListViewModel> ListAllCountry();
        IList<ListViewModel> ListAllState();
        IList<ListViewModel> ListCityByStateId(Guid Id);
        IList<ListViewModel> ListStateByCountryId(Guid Id);
        IList<ListViewModel> ListPostCodeByCityId(Guid Id);
        IList<ListViewModel> ListSuburbByPostCodeId(Guid id);
        IList<ListViewModel> ListDocumentType();
        IList<ListViewModel> ListCollectionMode();
        IList<ListViewModel> ListPostCodeGroup();
    }
}