﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Reflection;
using Kendo.DynamicLinq;
using System.Data.SqlClient;
using System.Globalization;
using ASPVueProject.DTO.ViewModel;
using ASPVueProject.DTO.Configuration;
using ASPVueProject.Data.Infrastructure;

namespace ASPVue.Service.Configuration
{
    public static class Extensions
    {
        public static string ToDescription(this Enum value)
        {
            try
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());

                DescriptionAttribute[] attributes =
                    (DescriptionAttribute[])fi.GetCustomAttributes(
                        typeof(DescriptionAttribute),
                        false);

                if (attributes.Length > 0)
                    return attributes[0].Description;
                return value.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static bool IsValidDateTime(this string value, string format, bool validateEmpty = false)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value) && validateEmpty)
                    return false;
                var result = DateTime.ParseExact(value, "ddMMyyyy", CultureInfo.InvariantCulture);
                return result != default(DateTime);
            }
            catch (Exception)
            {
                return false;
            }


        }
        public static List<Filter> ToCustomDateFilter(this Filter filter)
        {
            var value = Convert.ToDateTime(filter.Value);
            IDictionary<string, List<Filter>> filters = new Dictionary<string, List<Filter>>() {
               { "eq", new List<Filter>()
                                {
                                    new Filter(){
                                        Field = filter.Field,
                                        Operator="lte",
                                        Value=  new DateTime(value.Year, value.Month, value.Day, 0, 0, 0).AddDays(1) ,                        },
                                     new Filter()
                                     {
                                         Field = filter.Field,
                                         Operator = "gte",
                                         Value = new DateTime(value.Year, value.Month, value.Day, 0, 0, 0)
                                     }
                            }
                },

           };
            List<Filter> filterList = null;
            if (filters.TryGetValue(filter.Operator, out filterList))
            {
                return filterList;
            }
            return filterList;

        }
    }
    #region Kendo grid
    public static class QueryableExtensions
    {





        /// <summary>
        /// Applies data processing (paging, sorting and filtering) over IQueryable using Dynamic Linq.
        /// </summary>
        /// <typeparam name="T">The type of the IQueryable</typeparam>
        /// <param name="queryable">The IQueryable which should be processed.</param>
        /// <param name="kendoDataRequest"></param>
        /// <param name="initialsort"></param>
        /// <returns>A DataSourceResult object populated from the processed IQueryable.</returns>
        public static DataSourceResult ToDataSourceResult<T>(this IQueryable<T> queryable, KendoDataRequest kendoDataRequest, Sort initialsort)
        {
            // Filter the data first
            queryable = Filter(queryable, kendoDataRequest.Filter, kendoDataRequest.DataExtensions);

            // Calculate the total number of records (needed for paging)
            var total = queryable.Count();

            // Sort the data
            queryable = Sort(queryable, kendoDataRequest.Sort, initialsort, kendoDataRequest.DataExtensions);

            // Finally page the data
            queryable = Page(queryable, kendoDataRequest.Take, kendoDataRequest.Skip);

            return new DataSourceResult
            {
                Data = queryable.ToList(),
                Total = total
            };
        }

        /// <summary>
        /// Applies data processing (paging, sorting and filtering) over IQueryable using Dynamic Linq.
        /// </summary>
        /// <typeparam name="T">The source type of the IQueryable</typeparam>
        /// <typeparam name="TD">The destination type of the IQueryable</typeparam>
        /// <param name="queryable">The IQueryable which should be processed.</param>
        /// <param name="kendoDataRequest"></param>
        /// <param name="initialsort"></param>
        /// <returns>A DataSourceResult object populated from the processed IQueryable.</returns>
        public static DataSourceResult ToDataSourceResult<T, TD>(this IQueryable<T> queryable, KendoDataRequest kendoDataRequest, Sort initialsort)
        {
            var mapper = new DiObjectMapper();


            // Filter the data first
            queryable = Filter(queryable, kendoDataRequest.Filter, kendoDataRequest.DataExtensions);

            // Calculate the total number of records (needed for paging)
            var total = queryable.Count();

            // Sort the data
            queryable = Sort(queryable, kendoDataRequest.Sort, initialsort, kendoDataRequest.DataExtensions);

            // Finally page the data
            queryable = Page(queryable, kendoDataRequest.Take, kendoDataRequest.Skip);
            var mapped = mapper.Map<IList<T>, IList<TD>>(queryable.ToList());
            return new DataSourceResult
            {
                Data = mapped,
                Total = total
            };
        }

        public static object GetFilterValue(this Filter filter, string key)
        {
            if (filter != null && filter.Filters.Any())
            {
                return filter.Filters.SingleOrDefault(c => c.Field == key)?.Value;
            }
            return null;
        }
        private static IQueryable<T> Filter<T>(IQueryable<T> queryable, Filter filter, IList<DataExtension> dataExtensions)
        {
            if (filter == null || filter.Logic == null)
                return queryable;


            if (dataExtensions != null && filter.Filters != null)
            {
                if (dataExtensions.Any(p => p.CustomType == "MCS"))
                {
                    dataExtensions.Where(p => p.CustomType == "MCS").ToList().ForEach(oim =>
                    {
                        var str = filter.Filters.SingleOrDefault(c => c.Field == oim.Field);
                        if (str == null) return;
                        queryable = queryable.MultiValueContainsAnyAll(new Collection<string>() { str.Value.ToString() },
                            false, (Expression<Func<T, string[]>>)oim.Columns, str.Operator);

                        filter.Filters = filter.Filters.Except(new List<Filter>() { str });
                        if (filter.Filters.Any() == false)
                            return;
                    });

                    if (filter.Filters.Any() == false)
                        return queryable;

                }
            }


            // Collect a flat list of all filters
            var filters = filter.Filters.ToList();
            var ignorestring = "Ignore-Filter";
            var filtersToRemove = new List<Filter>();
            var filtersToAdd = new List<Filter>();
            if (dataExtensions != null)
            {
                filters.ToList().ForEach(c =>
                {
                    var dataExtension = dataExtensions.SingleOrDefault(p => p.Field == c.Field);
                    if (dataExtension == null) return;
                    if (dataExtension.Ignore)
                        c.Field = ignorestring;
                    if (!string.IsNullOrEmpty(dataExtension.ActualField))
                        c.Field = dataExtension.ActualField;

                    if (dataExtension.FieldType != null)
                        c.Value = dataExtension.FieldType.IsEnum ? Enum.Parse(dataExtension.FieldType, c.Value.ToString()) : Convert.ChangeType(c.Value, dataExtension.FieldType);
                    if (dataExtension.FieldType == typeof(DateTime))
                    {
                        var value = Convert.ToDateTime(c.Value);
                        if (c.Operator == "eq")
                        {
                            filtersToRemove.Add(c);
                            filtersToAdd.AddRange(
                                 c.ToCustomDateFilter()
                                 );
                        }
                    }



                });
            }
            filters = filters.Where(c => c.Field != ignorestring).ToList();
            foreach (var item in filtersToRemove)
            {
                filters.Remove(item);
            }
            foreach (var item in filtersToAdd)
            {
                filters.Add(item);
            }
            filter.Filters = filters;//filter.Filters.Where(c => c.Field != ignorestring).ToList();

            if (!filter.Filters.Any())
                return queryable;
            // Get all filter values as array (needed by the Where method of Dynamic Linq)
            var values = filters.Select(f => f.Value).ToArray();

            // Create a predicate expression e.g. Field1 = @0 And Field2 > @1
            string predicate = filter.ToExpression(filters);

            // Use the Where method of Dynamic Linq to filter the data
            queryable = queryable.Where(predicate, values);

            return queryable;
        }

        private static IQueryable<T> Sort<T>(IQueryable<T> queryable, IEnumerable<Sort> sort, Sort initialsort, IList<DataExtension> dataExtensions)
        {
            if (sort == null || !sort.Any())
            {
                sort = new List<Sort>() { initialsort };
            }


            if (dataExtensions != null && dataExtensions.Any(p => p.CustomType == "MCS"))
            {
                dataExtensions.Where(p => p.CustomType == "MCS").ToList().ForEach(oim =>
                {
                    var s = sort.SingleOrDefault(c => c.Field == oim.Field);

                    if (s == null)
                        return;

                    var direction = s.Dir;


                    var e =
                        ((Expression<Func<T, string[]>>)oim.Columns).Body.ToString()
                            .Replace("x.", "")
                            .Replace("new [] {", "")
                            .Replace("}", "")
                            .Split(',');

                    sort = sort.Except(new List<Sort>() { s });
                    var newList = e.Select(param => new Sort()
                    {
                        Field = param,
                        Dir = direction
                    }).ToList();

                    sort = sort.Concat(newList);

                });
            }

            if (dataExtensions != null)
            {
                sort.ToList().ForEach(c =>
                {
                    var dataExtension = dataExtensions.SingleOrDefault(p => p.Field == c.Field);
                    if (dataExtension == null) return;
                    if (!string.IsNullOrEmpty(dataExtension.ActualField))
                        c.Field = dataExtension.ActualField;
                });
            }
            //if (sort != null && sort.Any())
            //{
            // Create ordering expression e.g. Field1 asc, Field2 desc
            var ordering = String.Join(",", sort.Select(s => s.ToExpression()));

            // Use the OrderBy method of Dynamic Linq to sort the data
            return queryable.OrderBy(ordering);
            //}

            //return queryable;
        }

        private static IQueryable<T> Page<T>(IQueryable<T> queryable, int take, int skip)
        {
            return take == 0 ? queryable.Skip(skip) : queryable.Skip(skip).Take(take);
        }
    }

    public static class NonMappedQueryableExtensions
    {
        public static DataSourceResult ToDataSourceResult<T>(this IGenericUnitOfWork generalRepository, string queryable, KendoDataRequest kendoDataRequest,
            Sort initialsort, QueryType queryType = QueryType.None)
        {
            int total;
            if (queryType == QueryType.NeedToWrapBySelect)
                queryable = $"select * from ({queryable}) wrapped";
            string filteredQuery = GetQueryable(queryable, generalRepository, kendoDataRequest, out total, queryType == QueryType.AlreadyHasWhereClause);

            queryable = Page(filteredQuery, kendoDataRequest.Take, kendoDataRequest.Skip, kendoDataRequest.Sort, initialsort, kendoDataRequest.DataExtensions);
            var data = generalRepository.SqlQuery<T>(queryable).ToList();
            return new DataSourceResult()
            {
                Data = data,
                Total = total
            };
        }
        public static DataSourceResult ToDataSourceResult<T, Td>(this IGenericUnitOfWork generalRepository, string queryable, KendoDataRequest kendoDataRequest,
            Sort initialsort, QueryType queryType = QueryType.None)
        {
            int total;
            if (queryType == QueryType.NeedToWrapBySelect)
                queryable = $"select * from ({queryable}) wrapped";
            string filteredQuery = GetQueryable(queryable, generalRepository, kendoDataRequest, out total, queryType == QueryType.AlreadyHasWhereClause);

            queryable = Page(filteredQuery, kendoDataRequest.Take, kendoDataRequest.Skip, kendoDataRequest.Sort, initialsort, kendoDataRequest.DataExtensions);
            var data = generalRepository.SqlQuery<T>(queryable).ToList();
            var mapper = new DiObjectMapper();
            var mapped = mapper.Map<IList<T>, IList<Td>>(data.ToList());
            return new DataSourceResult()
            {
                Data = mapped,
                Total = total
            };
        }
        private static string GetQueryable(string queryable, IGenericUnitOfWork generalRepository, KendoDataRequest kendoDataRequest, out int total,
            bool hasWhereClause)
        {
            queryable = Filter(queryable, kendoDataRequest.Filter, kendoDataRequest.DataExtensions, hasWhereClause);
            total = generalRepository.SqlQuery<int>("select count(*) from(" + queryable + ") a").FirstOrDefault();
            return queryable;
        }
        private static string Filter(string queryable, Filter filter, IList<DataExtension> dataExtensions, bool hasWhereClause)
        {
            if (filter != null && filter.Logic != null)
            {
                // Collect a flat list of all filters
                var filters = filter.All();

                if (dataExtensions != null)
                {
                    filters.ToList().ForEach(c =>
                    {
                        var dataExtension = dataExtensions.SingleOrDefault(p => p.Field == c.Field);
                        if (dataExtension == null) return;
                        if (!string.IsNullOrEmpty(dataExtension.ActualField))
                            c.Field = dataExtension.ActualField;

                        if (dataExtension.FieldType != null)
                            c.Value = dataExtension.FieldType.IsEnum ? Enum.Parse(dataExtension.FieldType, c.Value.ToString()) : Convert.ChangeType(c.Value, dataExtension.FieldType);

                    });
                }
                var values = filters.Select(f => f.Value).ToArray();
                var predicate = filter.ToSql(filters);
                if (!hasWhereClause)
                    queryable += " where ";
                else queryable += " and ";
                queryable += string.Format(predicate, values);
            }

            return queryable;
        }

        private static string Page(string queryable, int take, int skip, IEnumerable<Sort> sort, Sort initialsort, IList<DataExtension> dataExtensions)
        {
            if (take == 0) // for take 0 get all
                return queryable;

            if (sort == null || !sort.Any())
            {
                sort = new List<Sort>() { initialsort };
            }
            if (dataExtensions != null)
            {
                sort.ToList().ForEach(c =>
                {
                    var dataExtension = dataExtensions.SingleOrDefault(p => p.Field == c.Field);
                    if (dataExtension == null) return;
                    if (!string.IsNullOrEmpty(dataExtension.ActualField))
                        c.Field = dataExtension.ActualField;
                });
            }

            var ordering = String.Join(",", sort.Select(s => s.ToExpression()));

            queryable = "select Top " + (take + skip) + " ROW_NUMBER() OVER (ORDER BY " + ordering + ") AS SNo,* from(" + queryable + ") listing ORDER BY " + ordering;
            return @"WITH Results_CTE AS (" + queryable + ") SELECT * FROM Results_CTE WHERE SNo > " + skip + " AND SNo <=" + (take + skip);


        }

        /// <summary>
        /// Mapping of Kendo DataSource filtering operators
        /// </summary>
        private static readonly IDictionary<string, string> Operators = new Dictionary<string, string>
        {
            {"eq", "="},
            {"neq", "!="},
            {"lt", "<"},
            {"lte", "<="},
            {"gt", ">"},
            {"gte", ">="},
            {"startswith", "StartsWith"},
            {"endswith", "EndsWith"},
            {"contains", "Contains"},
            {"in", "in"},
            {"doesnotcontain", "Contains"},
            {"isempty","isempty" },
            {"isnotempty","isnotempty" }
        };

        /// <summary>
        /// Converts the filter expression to a predicate suitable for Dynamic Linq e.g. "Field1 = @1 and Field2.Contains(@2)"
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="filters">A list of flattened filters.</param>
        private static string ToSql(this Filter filter, IList<Filter> filters)
        {
            if (filter.Filters != null && filter.Filters.Any())
                return String.Join(" " + filter.Logic + " ", filter.Filters.Select(f => f.ToSql(filters)).ToArray());

            var index = filters.IndexOf(filter);
            var comparison = Operators[filter.Operator];
            if (filter.Operator == "doesnotcontain")
                return filter.Field + " NOT LIKE '%{" + index + "}%'";
            if (comparison == "StartsWith")
                return filter.Field + " LIKE '{" + index + "}%'";
            if (comparison == "EndsWith")
                return filter.Field + " LIKE '%{" + index + "}'";
            if (comparison == "Contains")
                return filter.Field + " LIKE '%{" + index + "}%'";
            if (comparison == "in")
                return filter.Field + " in ({" + index + "})";
            if (comparison == "isempty")
                return filter.Field + " = ''";
            if (comparison == "isnotempty")
                return filter.Field + " != ''";


            return filter.Field + comparison + "'{" + index + "}'";
        }

        public static IQueryable<T> MultiValueContainsAnyAll<T>(this IQueryable<T> source, ICollection<string> searchKeys, bool all, Expression<Func<T, string[]>> fieldSelectors, string opt)
        {

            var containsMethod = typeof(string).GetMethod("Contains");
            var startsWithMethod = typeof(string).GetMethod("StartsWith", new Type[] { typeof(string) });
            var endsWithMethod = typeof(string).GetMethod("EndsWith", new Type[] { typeof(string) });


            if (source == null)
                throw new ArgumentNullException("source");
            if (fieldSelectors == null)
                throw new ArgumentNullException("fieldSelectors");
            var newArray = fieldSelectors.Body as NewArrayExpression;
            if (newArray == null)
                throw new ArgumentOutOfRangeException("fieldSelectors", fieldSelectors, "You need to use fieldSelectors similar to 'x => new string [] { x.LastName, x.FirstName, x.NickName }'; other forms not handled.");
            if (newArray.Expressions.Count == 0)
                throw new ArgumentException("No field selected.");
            if (searchKeys == null || searchKeys.Count == 0)
                return source;

            //MethodInfo containsMethod = typeof(string).GetMethod("Contains", new Type[] { typeof(string) });
            Expression expression = null;

            foreach (var searchKeyPart in searchKeys)
            {
                var tmp = new Tuple<string>(searchKeyPart);
                Expression searchKeyExpression = Expression.Property(Expression.Constant(tmp), tmp.GetType().GetProperty("Item1"));

                Expression oneValueExpression = null;
                foreach (var fieldSelector in newArray.Expressions)
                {
                    Expression act = null;// = Expression.Call(, containsMethod, );
                    switch (opt)
                    {
                        case "eq":
                            act = Expression.Equal(fieldSelector, searchKeyExpression);
                            break;
                        case "neq":
                            act = Expression.NotEqual(fieldSelector, searchKeyExpression);
                            break;
                        case "contains":
                            act = Expression.Call(fieldSelector, containsMethod, searchKeyExpression);
                            break;
                        case "startswith":
                            act = Expression.Call(fieldSelector, startsWithMethod, searchKeyExpression);
                            break;
                        case "endswith":
                            act = Expression.Call(fieldSelector, endsWithMethod, searchKeyExpression);
                            break;
                    }

                    oneValueExpression = oneValueExpression == null ? act : Expression.OrElse(oneValueExpression, act);
                }

                if (expression == null)
                    expression = oneValueExpression;
                else if (all)
                    expression = Expression.AndAlso(expression, oneValueExpression);
                else
                    expression = Expression.OrElse(expression, oneValueExpression);
            }
            return source.Where(Expression.Lambda<Func<T, bool>>(expression, fieldSelectors.Parameters));
        }
    }

    public enum QueryType
    {
        None, NeedToWrapBySelect, AlreadyHasWhereClause
    }
    #endregion
 

    public static class SqlExtensions
    {
        public static List<SqlParameter> ToSqlParameterList(this Dictionary<string, object> parameters)
        {
            var sqlParameterCollection = new List<SqlParameter>();
            foreach (var parameter in parameters)
            {
                sqlParameterCollection.Add(new SqlParameter($"@{parameter.Key}", parameter.Value == null ? DBNull.Value : parameter.Value));
            }
            return sqlParameterCollection;
        }

        public static string NormalizeProcedureName(this string name, Dictionary<string, object> param)
        {
            if (!name.Contains("@"))
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                string delimiter = "";
                sb.Append(name);

                foreach (var item in param)
                {
                    sb.Append(delimiter);
                    sb.Append(" @");
                    sb.Append(item.Key);
                    delimiter = ",";
                }

                name = sb.ToString();
            }
            return name;
        }
    }

 
}
