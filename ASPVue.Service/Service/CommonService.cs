﻿using ASPVueProject.Data.Infrastructure;
using ASPVueProject.Service.Interface;

namespace ASPVueProject.Service.Service
{
	public class CommonService : ICommonService
    {
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        public CommonService(IGenericUnitOfWork genericUnitOfWork)
        {
            _genericUnitOfWork = genericUnitOfWork;
        }
    }
}
