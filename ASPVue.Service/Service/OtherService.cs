﻿using ASPVueProject.Data.Infrastructure;
using ASPVueProject.DTO.Configuration;
using ASPVueProject.Service.Interface;

namespace ASPVueProject.Service.Service
{
	public class OtherService : IOtherService
    {
        private readonly IDiObjectMapper _mapper;
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        public OtherService(IGenericUnitOfWork genericUnitOfWork, IDiObjectMapper mapper)
        {
            _genericUnitOfWork = genericUnitOfWork;
            _mapper = mapper;
        }

        #region Dashboard
        #endregion
    }
}
