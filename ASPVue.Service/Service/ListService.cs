﻿using ASPVueProject.Data.Infrastructure;
using ASPVueProject.Domain.Constant;
using ASPVueProject.Domain.Entity;
using ASPVueProject.Domain.Enum;
using ASPVueProject.DTO.Configuration;
using ASPVueProject.DTO.ViewModel;
using ASPVueProject.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using static ASPVueProject.Common.Configuration.GeneralService;

namespace ASPVueProject.Service.Service
{
    public class ListService : IListService
    {
        private readonly IDiObjectMapper _mapper;
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        public ListService(
            IDiObjectMapper mapper, IGenericUnitOfWork genericUnitOfWork)
        {
            _mapper = mapper;
            _genericUnitOfWork = genericUnitOfWork;
        }
        public IList<ListViewModel> ListRole()
        {
            var roles =
                _genericUnitOfWork.GetRepository<Role, Guid>().GetAll()
                    .Where(x => x.IsActive && x.Id != new Guid(ApplicationConstants.SuperAdminRoleGuid))
                    .OrderBy(c => c.Name).ToList();
            return _mapper.Map<IList<Role>, IList<ListViewModel>>(roles);
        }

        public IList<ListViewModel> ListUserStatus()
        {
            return EnumList<UserStatus>.Get().ToList();
        }
        public IList<ListViewModel> ListAllCountry()
        {
            var countryList = _genericUnitOfWork.GetRepository<Country, Guid>().GetAll().Where(x => x.IsEnable).OrderBy(t => t.Name).AsEnumerable().ToList();
            return _mapper.Map<IList<Country>, IList<ListViewModel>>(countryList);
        }
        public IList<ListViewModel> ListAllState()
        {
            var stateList = _genericUnitOfWork.GetRepository<StateProvince, Guid>().GetAll().OrderBy(t => t.StateName).AsEnumerable().ToList();
            return _mapper.Map<IList<StateProvince>, IList<ListViewModel>>(stateList);
        }

        public IList<ListViewModel> ListCityByStateId(Guid Id)
        {
            var cityList = _genericUnitOfWork.GetRepository<City, Guid>().GetAll().Where(x => x.StateProvinceId == Id).AsEnumerable().ToList();
            return _mapper.Map<IList<City>, IList<ListViewModel>>(cityList);
        }

        public IList<ListViewModel> ListStateByCountryId(Guid Id)
        {
            var stateList = _genericUnitOfWork.GetRepository<StateProvince, Guid>().GetAll().Where(x => x.CountryId == Id).AsEnumerable().ToList();
            return _mapper.Map<IList<StateProvince>, IList<ListViewModel>>(stateList);
        }

        public IList<ListViewModel> ListPurposeOfOrderType()
        {
            return EnumList<PurposeOfOrderType>.Get().ToList();
        }

        public IList<ListViewModel> ListPostCodeByCityId(Guid CityId)
        {
            var postCodeList = _genericUnitOfWork.GetRepository<PostCode, Guid>().GetAll()
                .Where(x => x.CityId == CityId).AsEnumerable().ToList();
            return _mapper.Map<IList<PostCode>, IList<ListViewModel>>(postCodeList);
        }

        public IList<ListViewModel> ListSuburbByPostCodeId(Guid postCodeId)
        {
            var suburbList = _genericUnitOfWork.GetRepository<Suburb, Guid>().GetAll()
                .Where(x => x.PostCodeId == postCodeId).AsEnumerable().ToList();
            return _mapper.Map<IList<Suburb>, IList<ListViewModel>>(suburbList);
        }

        

        public IList<ListViewModel> ListDocumentType()
        {
            return EnumList<DocumentType>.Get().ToList();
        }
       

        
        public IList<ListViewModel> ListCollectionMode()
        {
            return EnumList<CollectionMode>.Get().ToList();
        }

        public IList<ListViewModel> ListPostCodeGroup()
        {
            var postCodeGroupList = _genericUnitOfWork.GetRepository<PostCodeGroup, Guid>().GetAll().Where(x => x.IsEnabled).OrderBy(t => t.PostCodeGroupName).AsEnumerable().ToList();
            return _mapper.Map<IList<PostCodeGroup>, IList<ListViewModel>>(postCodeGroupList);
        }
    }
}
