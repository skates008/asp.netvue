﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Kendo.DynamicLinq;
using Microsoft.AspNet.Identity;
using ASPVueProject.Data.Infrastructure;
using ASPVueProject.Domain.Constant;
using ASPVueProject.DTO.Configuration;
using ASPVueProject.Service.Interface;
using ASPVueProject.Domain.Enum;
using ASPVueProject.DTO.ViewModel;
using ASPVueProject.Domain.Entity;
using ASPVueProject.Common.Configuration;
using ASPVueProject.Common;

namespace ASPVueProject.Service.Service
{
    public class SetUpService : ISetUpService
    {
        private readonly IDiObjectMapper _mapper;
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        private readonly JobSchedular _jobSchedular;
        public SetUpService(
            IDiObjectMapper mapper,
            IGenericUnitOfWork genericUnitOfWork, JobSchedular jobSchedular)
        {
            _mapper = mapper;
            _genericUnitOfWork = genericUnitOfWork;
            _jobSchedular = jobSchedular;
        }

        #region Authentication


        public SignInResponseViewModel SignIn(ServiceRequestModel<SignInViewModel> request)
        {
            var userQueryable = _genericUnitOfWork.GetRepository<User, Guid>().GetAll();
            var userDeviceKeyRepo = _genericUnitOfWork.GetRepository<UserDeviceKey, Guid>();
            var user = userQueryable.FirstOrDefault(c =>
                c.UserName.Trim() == request.Model.Email.Trim() || c.PhoneNumber.Trim() == request.Model.Email.Trim());
            if (user == null)
                throw new ArgumentException("Invalid Email/PhoneNumber or Password!");
            //if (user.Type == UserType.SystemUser)
            //    throw new ArgumentException("Invalid Email/PhoneNumber or Password!");
            if (user.Status == UserStatus.Suspened || user.Status == UserStatus.Locked)
                throw new ArgumentException("Your account is inactive. Please contact our customer care!");

            var userDevices = userDeviceKeyRepo.GetAll().FirstOrDefault(c => c.UserId == user.Id && c.DeviceIdentifier != request.Model.DeviceIdentifier && c.IsActive)?.DeviceIdentifier ?? string.Empty;
            var mappedDevice = _mapper.Map<SignInViewModel, UserDeviceKey>(request.Model);
            mappedDevice.Id = Guid.NewGuid();
            mappedDevice.CreatedDate = GeneralService.CurrentDate();
            mappedDevice.DeviceAuthTokenExpiryDate = GeneralService.CurrentDate().AddDays(1);
            mappedDevice.RefreshToken = Guid.NewGuid().ToString();
            mappedDevice.UserId = user.Id;
            mappedDevice.DeviceAuthToken = Guid.NewGuid().ToString();
            userDeviceKeyRepo.Add(mappedDevice);
            _genericUnitOfWork.SaveChanges();
            return new SignInResponseViewModel()
            {
                OtpKey = mappedDevice.OtpToken,
                DeviceKey = SecurityService.Encrypt(mappedDevice.DeviceAuthToken),
                OldDeviceIdentifier = userDevices,
                RefreshToken = mappedDevice.RefreshToken,
                DeviceKeyExpiryDate = mappedDevice.DeviceAuthTokenExpiryDate,
                IsNotificationActive = false,
                Name = user.FullName
            };
        }

        #endregion

        #region Role
        public DataSourceResult ListRole(ServiceRequestModel<KendoDataRequest> request)
        {
            request.Model.DataExtensions = new List<DataExtension>()
                {
                    new DataExtension() { Field = "IsActive", FieldType = typeof(bool) }
                };
            var roles = _genericUnitOfWork.GetRepository<Role, Guid>().GetAll();

            if (request.Model.DataExtensions != null)
            {
                if (request.Model.ExternalFilter.ContainsKey("Name"))
                {
                    var roleName = request.Model.ExternalFilter["Name"].ToString();
                    roles = roles.Where(p => p.Name.Contains(roleName));
                }
            }



            return roles.Where(c => c.Id != new Guid(ApplicationConstants.SuperAdminRoleGuid))
                .ToDataSourceResult<Role, RoleListViewModel>(request.Model, new Sort() { Field = "Id", Dir = "asc" });
        }
        public void AddRole(ServiceRequestModel<RoleViewModel> serviceRequestModel)
        {
            var repo = _genericUnitOfWork.GetRepository<Role, Guid>();
            var mapped = _mapper.Map<RoleViewModel, Role>(serviceRequestModel.Model);
            mapped.Id = Guid.NewGuid();
            mapped.SystemDefined = false;
            repo.Add(mapped);
            _genericUnitOfWork.SaveChanges();
        }
        public void UpdateRole(ServiceRequestModel<RoleViewModel> serviceRequestModel)
        {
            var repo = _genericUnitOfWork.GetRepository<Role, Guid>();
            _genericUnitOfWork.NonQuery("delete from UserPage where roleid = @roleId", new SqlParameter("RoleId", serviceRequestModel.Model.Id));
            var data = repo.GetById(serviceRequestModel.Model.Id);
            var mapped = _mapper.Map(serviceRequestModel.Model, data);
            repo.Update(mapped);
            _genericUnitOfWork.SaveChanges();
        }
        public RoleViewModel GetRole(ServiceRequestModel<Guid> request)
        {
            var roleViewModel = _mapper.Map<Role, RoleViewModel>(_genericUnitOfWork.GetRepository<Role, Guid>().GetById(request.Model));
            if (request.Model == Guid.Empty)
                roleViewModel = new RoleViewModel();

            roleViewModel.Pages = ListRolePage(request);
            return roleViewModel;
        }
        public List<RolePageTreeViewModel> ListRolePage(ServiceRequestModel<Guid> request)
        {
            var rolewisePage =
                _genericUnitOfWork.GetRepository<UserPage, int>().GetAll();

            if (request.Model != new Guid(ApplicationConstants.SuperAdminRoleGuid))
                rolewisePage = rolewisePage.Where(c => c.RoleId == request.Model && c.Page.ShowInView).OrderBy(c => c.Page.DisplayOrder);

            var pages = _genericUnitOfWork.GetRepository<UserPage, int>().GetAll().Where(c => c.Page.ShowInView && c.RoleId == request.LoginInfo.RoleId).Select(c => c.Page).Distinct().ToList();
            var query = pages.GroupJoin(rolewisePage, page => page.Id, userPage => userPage.PageId,
                (page, userpage) => new RolePageTreeViewModel
                {
                    Id = page.Id,
                    Text = page.Title,
                    ParentId = page.ParentPageId,
                    Checked = page.Id == 1 || userpage != null && userpage.Any(),
                    DisplayOrder = page.DisplayOrder,
                    Expanded = true
                }).ToList();

            var pageTree = GetRoleChildPages(query, null);
            return pageTree;
            //return query;
        }
        public List<RolePageTreeViewModel> GetRoleChildPages(List<RolePageTreeViewModel> pages, int? parentPageId)
        {
            var childpages = pages.Where(c => c.ParentId == parentPageId).OrderBy(c => c.DisplayOrder);
            return childpages
                .Select(c => new RolePageTreeViewModel()
                {
                    Pages = GetRoleChildPages(pages, c.Id),
                    DisplayOrder = c.DisplayOrder,
                    Id = c.Id,
                    Text = c.Text,
                    ParentId = c.ParentId,
                    Checked = c.Checked,
                    Expanded = pages.Any(x => x.ParentId == c.Id && x.Checked == true)
                }).ToList();
        }
        public List<TreeViewModel> ListRoleUserPage(ServiceRequestModel<Guid> request)
        {
            var rolewisePage =
                _genericUnitOfWork.GetRepository<UserPage, int>().GetAll();
            if (request.Model != new Guid(ApplicationConstants.SuperAdminRoleGuid))
                rolewisePage = rolewisePage.Where(c => c.RoleId == request.Model && c.Page.ShowInView).OrderBy(c => c.Page.DisplayOrder);
            var pages = _genericUnitOfWork.GetRepository<UserPage, int>().GetAll().Where(c => c.Page.ShowInView && c.RoleId == request.LoginInfo.RoleId).Select(c => c.Page).Distinct().ToList();
            var query = pages.GroupJoin(rolewisePage, page => page.Id, userPage => userPage.PageId, (page, userpage) => new TreeViewModel
            {
                id = page.Id,
                text = page.Title,
                parentid = page.ParentPageId,
                @checked = (userpage != null && userpage.Any()),
                displayorder = page.DisplayOrder,
                expanded = true
            }).ToList();
            var pageTree = GetChildPages(query, null);
            return pageTree;
        }
        public List<TreeViewModel> GetChildPages(List<TreeViewModel> pages, int? parentPageId)
        {
            var childpages = pages.Where(c => c.parentid == parentPageId).OrderBy(c => c.displayorder);
            return childpages
                .Select(c => new TreeViewModel()
                {
                    items = GetChildPages(pages, c.id),
                    displayorder = c.displayorder,
                    id = c.id,
                    text = c.text,
                    parentid = c.parentid,
                    @checked = c.@checked,
                    expanded = true

                }).ToList();
        }
        public void DeleteRole(ServiceRequestModel<Guid> serviceRequestModel)
        {
            var repo = _genericUnitOfWork.GetRepository<Role, Guid>();
            if (repo.GetTagList(serviceRequestModel.Model, new List<string>() { "UserPage" }).Any())
                throw new ArgumentNullException($"Cannot delete. Already tagged.", innerException: null);
            var data = repo.GetById(serviceRequestModel.Model);
            if (data.SystemDefined)
                throw new ArgumentNullException($"Cannot delete. Access denied.", innerException: null);
            _genericUnitOfWork.NonQuery("delete from UserPage where roleid = @roleId", new SqlParameter("RoleId", serviceRequestModel.Model));
            repo.Delete(data);
            _genericUnitOfWork.SaveChanges();
        }
        #endregion

        #region User
        public DataSourceResult ListUser(ServiceRequestModel<KendoDataRequest> request)
        {
            var queryable = _genericUnitOfWork.GetRepository<User, Guid>().GetAll().Where(c => c.Id != new Guid(ApplicationConstants.SuperAdminUserGuid));
            if (request.Model.ExternalFilter.Count > 0 && request.Model.ExternalFilter["UserType"] != null)
            {
                var userType = (UserType)Enum.Parse(typeof(UserType), request.Model.ExternalFilter["UserType"].ToString());
                queryable = queryable.Where(c => c.Type == userType);
                if (userType != UserType.SystemUser)
                    return queryable.ToDataSourceResult<User, UserListViewModel>(request.Model, new Sort() { Field = "FullName", Dir = "asc" });
            }

            if (request.LoginInfo.RoleHierarchy == null)
                queryable = queryable.Where(c => c.Id != request.LoginInfo.UserId);
            else
            {
                queryable =
                    queryable.Where(
                        c =>
                            c.Roles.Any(
                                r => r.Role.Hierarchy >= request.LoginInfo.RoleHierarchy || r.Role.Hierarchy == null));
            }
            return queryable.ToDataSourceResult<User, UserListViewModel>(request.Model, new Sort() { Field = "FullName", Dir = "asc" });
        }
        public UserViewModel AddUser(ServiceRequestModel<UserViewModel> serviceRequestModel)
        {
            try
            {
                var repo = _genericUnitOfWork.GetRepository<User, Guid>();
                var users = repo.GetAll();

                var emailExists = users.Any(c => c.UserName.Trim() == serviceRequestModel.Model.Username.Trim());
                if (emailExists)
                    throw new ArgumentNullException($"A user with the email already exists in the system", innerException: null);

                var mapped = _mapper.Map<UserViewModel, User>(serviceRequestModel.Model);
                mapped.AccessFailedCount = 0;
                mapped.LockoutEnabled = false;
                mapped.TwoFactorEnabled = false;
                mapped.EmailConfirmed = true;
                mapped.ApprovalStatus = ApprovalStatus.Pending;
                mapped.Status = UserStatus.Active;
                mapped.Type = UserType.SystemUser;
                mapped.ForceChangePassword = true;
                mapped.PasswordHash = new PasswordHasher().HashPassword("test@@#$%");//garbagepassword
                mapped.SecurityStamp = Guid.NewGuid().ToString();
                repo.Add(mapped);

                #region Insert Role
                if (serviceRequestModel.Model.RoleId != Guid.Empty)
                {
                    _genericUnitOfWork.GetRepository<UserRole, Guid>().Add(new UserRole
                    {
                        UserId = mapped.Id,
                        RoleId = serviceRequestModel.Model.RoleId
                    });
                }
                #endregion
                _genericUnitOfWork.SaveChanges();


                var result = _mapper.Map<User, UserViewModel>(mapped);
                return result;
            }
            catch (Exception ex) {
                throw new ArgumentNullException(ex.Message);
            }
        }
        public void UpdateUser(ServiceRequestModel<UserViewModel> serviceRequestModel)
        {
            var repo = _genericUnitOfWork.GetRepository<User, Guid>();
            var users = repo.GetAll();

            var emailExists = users.Any(c => c.UserName.Trim() == serviceRequestModel.Model.Username.Trim() && c.Id != serviceRequestModel.Model.Id);
            if (emailExists)
                throw new ArgumentNullException($"A user with the email already exists in the system", innerException: null);

            var data = repo.GetById(serviceRequestModel.Model.Id);
            var mapped = _mapper.Map(serviceRequestModel.Model, data);


            repo.Update(mapped);

            var userRoleRepo = _genericUnitOfWork.GetRepository<UserRole, Guid>();

            var userRole = userRoleRepo.GetAll().FirstOrDefault(c => c.UserId == serviceRequestModel.Model.Id);

            userRoleRepo.Delete(userRole);

            userRoleRepo.Add(new UserRole()
            {
                UserId = mapped.Id,
                RoleId = serviceRequestModel.Model.RoleId
            });

            _genericUnitOfWork.SaveChanges();
        }
        public UserViewModel GetUser(Guid id)
        {
            return _mapper.Map<User, UserViewModel>(_genericUnitOfWork.GetRepository<User, Guid>().GetById(id));
        }
        public void DeleteUser(ServiceRequestModel<Guid> serviceRequestModel)
        {
            var repo = _genericUnitOfWork.GetRepository<User, Guid>();
            if (repo.GetTagList(serviceRequestModel.Model, new List<string>() { "UserRole" }).Any())
                throw new ArgumentNullException($"Cannot delete. Already tagged.", innerException: null);
            var data = repo.GetById(serviceRequestModel.Model);
            if (data.SystemDefined)
                throw new ArgumentNullException($"Cannot delete. Access denied.", innerException: null);
            var userRoleRepo = _genericUnitOfWork.GetRepository<UserRole, Guid>();

            var userRole = userRoleRepo.GetAll().FirstOrDefault(c => c.UserId == serviceRequestModel.Model);

            userRoleRepo.Delete(userRole);
            repo.Delete(data);
            _genericUnitOfWork.SaveChanges();
        }
        public void ApproveUser(ServiceRequestModel<Guid> serviceRequestModel)
        {
            var repo = _genericUnitOfWork.GetRepository<User, Guid>();
            var data = repo.GetById(serviceRequestModel.Model);
            data.Status = UserStatus.Active;
            repo.Update(data);
            _genericUnitOfWork.SaveChanges();
        }

        #endregion User
    }
}
