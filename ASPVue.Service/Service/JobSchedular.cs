﻿using ASPVueProject.Data.Infrastructure;
using ASPVueProject.DTO.Configuration;
using ASPVueProject.Mailing.Interfaces;
using ASPVueProject.Service.Interface;

namespace ASPVueProject.Service.Service
{
    public class JobSchedular : IJobSchedular
    {
        private readonly ICommonService _commonService;
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        private readonly IEmailComposer _emailComposer;
        private readonly IDiObjectMapper _mapper;
        public JobSchedular(
            ICommonService commonService,
            IGenericUnitOfWork genericUnitOfWork, IEmailComposer emailComposer,IDiObjectMapper mapper)
        {
            _commonService = commonService;
            _genericUnitOfWork = genericUnitOfWork;
            _emailComposer = emailComposer;
            _mapper = mapper;
        }
        
    }
}
