﻿using ASPVueProject.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASPVueProject.DTO.ViewModel;
using Kendo.DynamicLinq;
using ASPVueProject.Data.Infrastructure;
using ASPVueProject.DTO.Configuration;
using ASPVueProject.Domain.Entity;

namespace ASPVueProject.Service.Service
{
    public class AddressSetupService : IAddressSetupService
    {
        private readonly IDiObjectMapper _mapper;
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        public AddressSetupService(
            IDiObjectMapper mapper, IGenericUnitOfWork genericUnitOfWork)
        {
            _mapper = mapper;
            _genericUnitOfWork = genericUnitOfWork;
        }
        #region Country Setup
        public void AddCountry(ServiceRequestModel<CountryViewModel> serviceRequestModel)
        {
            var mappedData = _mapper.Map<CountryViewModel, Country>(serviceRequestModel.Model);
            _genericUnitOfWork.GetRepository<Country, Guid>().Add(mappedData);

            mappedData.Id = Guid.NewGuid();

            _genericUnitOfWork.SaveChanges();
        }

        public void DeleteCountry(ServiceRequestModel<Guid> serviceRequestModel)
        {

            var repo = _genericUnitOfWork.GetRepository<Country, Guid>();
            var data = repo.GetById(serviceRequestModel.Model);

            repo.Delete(data);

            _genericUnitOfWork.SaveChanges();
        }
        public CountryViewModel GetCountry(ServiceRequestModel<Guid> request)
        {
            var countryViewModel = new CountryViewModel();

            if (request.Model == Guid.Empty)
                return countryViewModel;


            countryViewModel = _mapper.Map<Country, CountryViewModel>(_genericUnitOfWork.GetRepository<Country, Guid>().
               GetById(request.Model));

            return countryViewModel;

        }
        public DataSourceResult ListCountry(ServiceRequestModel<KendoDataRequest> request)
        {
            var queryable = _genericUnitOfWork.GetRepository<Country, Guid>().GetAll();
            return queryable.ToDataSourceResult<Country, CountryListViewModel>(request.Model, new Sort() { Field = "Name", Dir = "asc" });
        }


        public void UpdateCountry(ServiceRequestModel<CountryViewModel> serviceRequestModel)
        {
            var repoObj = _genericUnitOfWork.GetRepository<Country, Guid>();

            var country = repoObj.GetById(serviceRequestModel.Model.Id);

            var mapped = _mapper.Map(serviceRequestModel.Model, country);

            repoObj.Update(mapped);

            _genericUnitOfWork.SaveChanges();
        }

        #endregion

        #region State Setup
        public void DeleteState(ServiceRequestModel<Guid> serviceRequestModel)
        {
            var repo = _genericUnitOfWork.GetRepository<StateProvince, Guid>();
            var data = repo.GetById(serviceRequestModel.Model);

            repo.Delete(data);

            _genericUnitOfWork.SaveChanges();
        }
        public StateProvinceViewModel GetState(ServiceRequestModel<Guid> requestModel)
        {
            var stateProvinceModel = new StateProvinceViewModel();

            if (requestModel.Model == Guid.Empty)
                return stateProvinceModel;


            stateProvinceModel = _mapper.Map<StateProvince, StateProvinceViewModel>(_genericUnitOfWork.GetRepository<StateProvince, Guid>().
               GetById(requestModel.Model));

            return stateProvinceModel;
        }
        public DataSourceResult ListState(ServiceRequestModel<KendoDataRequest> request)
        {
            var queryable = _genericUnitOfWork.GetRepository<StateProvince, Guid>().GetAll();
            return queryable.ToDataSourceResult<StateProvince, StateProvinceListViewModel>(request.Model, new Sort() { Field = "StateName", Dir = "asc" });
        }

        public void UpdateState(ServiceRequestModel<StateProvinceViewModel> serviceRequestModel)
        {
            var repoObj = _genericUnitOfWork.GetRepository<StateProvince, Guid>();

            var stateProvince = repoObj.GetById(serviceRequestModel.Model.Id);

            var mapped = _mapper.Map(serviceRequestModel.Model, stateProvince);

            repoObj.Update(mapped);

            _genericUnitOfWork.SaveChanges();
        }
        public void AddState(ServiceRequestModel<StateProvinceViewModel> serviceRequestModel)
        {
            var mappedData = _mapper.Map<StateProvinceViewModel, StateProvince>(serviceRequestModel.Model);
            _genericUnitOfWork.GetRepository<StateProvince, Guid>().Add(mappedData);

            mappedData.Id = Guid.NewGuid();
            mappedData.DisplayName = serviceRequestModel.Model.StateName;

            _genericUnitOfWork.SaveChanges();
        }
        #endregion

        #region City Setup
        public void DeleteCity(ServiceRequestModel<Guid> serviceRequestModel)
        {
            var repo = _genericUnitOfWork.GetRepository<City, Guid>();
            var data = repo.GetById(serviceRequestModel.Model);

            repo.Delete(data);

            _genericUnitOfWork.SaveChanges();
        }
        public CityViewModel GetCity(ServiceRequestModel<Guid> requestModel)
        {
            var stateProvinceModel = new CityViewModel();

            if (requestModel.Model == Guid.Empty)
                return stateProvinceModel;


            stateProvinceModel = _mapper.Map<City, CityViewModel>(_genericUnitOfWork.GetRepository<City, Guid>().
               GetById(requestModel.Model));

            return stateProvinceModel;
        }
        public DataSourceResult ListCity(ServiceRequestModel<KendoDataRequest> request)
        {
            var queryable = _genericUnitOfWork.GetRepository<City, Guid>().GetAll();
            return queryable.ToDataSourceResult<City, CityListViewModel>(request.Model, new Sort() { Field = "CityName", Dir = "asc" });
        }

        public void UpdateCity(ServiceRequestModel<CityViewModel> serviceRequestModel)
        {
            var repoObj = _genericUnitOfWork.GetRepository<City, Guid>();

            var stateProvince = repoObj.GetById(serviceRequestModel.Model.Id);

            var mapped = _mapper.Map(serviceRequestModel.Model, stateProvince);

            repoObj.Update(mapped);

            _genericUnitOfWork.SaveChanges();
        }
        public void AddCity(ServiceRequestModel<CityViewModel> serviceRequestModel)
        {
            var mappedData = _mapper.Map<CityViewModel, City>(serviceRequestModel.Model);
            _genericUnitOfWork.GetRepository<City, Guid>().Add(mappedData);

            mappedData.Id = Guid.NewGuid();

            _genericUnitOfWork.SaveChanges();
        }
        #endregion

        #region PostCode Setup
        public void AddPostCode(ServiceRequestModel<PostCodeViewModel> serviceRequestModel)
        {
            var mappedData = _mapper.Map<PostCodeViewModel, PostCode>(serviceRequestModel.Model);
            _genericUnitOfWork.GetRepository<PostCode, Guid>().Add(mappedData);

            mappedData.Id = Guid.NewGuid();

            _genericUnitOfWork.SaveChanges();
        }

        public void DeletePostCode(ServiceRequestModel<Guid> serviceRequestModel)
        {

            var repo = _genericUnitOfWork.GetRepository<PostCode, Guid>();
            var data = repo.GetById(serviceRequestModel.Model);

            repo.Delete(data);

            _genericUnitOfWork.SaveChanges();
        }
        public PostCodeViewModel GetPostCode(ServiceRequestModel<Guid> request)
        {
            var countryViewModel = new PostCodeViewModel();

            if (request.Model == Guid.Empty)
                return countryViewModel;

            var repoObj = _genericUnitOfWork.GetRepository<PostCode, Guid>().GetById(request.Model);

            countryViewModel = _mapper.Map<PostCode, PostCodeViewModel>(repoObj);
            countryViewModel.StateProvinceId = repoObj.City.StateProvinceId;

            return countryViewModel;

        }
        public DataSourceResult ListPostCode(ServiceRequestModel<KendoDataRequest> request)
        {
            var queryable = _genericUnitOfWork.GetRepository<PostCode, Guid>().GetAll();
            return queryable.ToDataSourceResult<PostCode, PostCodeListViewModel>(request.Model, new Sort() { Field = "PostCodeName", Dir = "asc" });
        }


        public void UpdatePostCode(ServiceRequestModel<PostCodeViewModel> serviceRequestModel)
        {
            var repoObj = _genericUnitOfWork.GetRepository<PostCode, Guid>();

            var country = repoObj.GetById(serviceRequestModel.Model.Id);

            var mapped = _mapper.Map(serviceRequestModel.Model, country);

            repoObj.Update(mapped);

            _genericUnitOfWork.SaveChanges();
        }
        #endregion

        #region Add Suburb
        public void AddSuburb(ServiceRequestModel<SuburbViewModel> serviceRequestModel)
        {
            var mappedData = _mapper.Map<SuburbViewModel, Suburb>(serviceRequestModel.Model);
            _genericUnitOfWork.GetRepository<Suburb, Guid>().Add(mappedData);

            mappedData.Id = Guid.NewGuid();

            _genericUnitOfWork.SaveChanges();
        }

        public DataSourceResult ListSuburb(ServiceRequestModel<KendoDataRequest> serviceRequestModel)
        {
            var queryable = _genericUnitOfWork.GetRepository<Suburb, Guid>().GetAll();
            return queryable.ToDataSourceResult<Suburb, SuburbListViewModel>(serviceRequestModel.Model, new Sort() { Field = "SuburbName", Dir = "asc" });
        }

        public void UpdateSuburb(ServiceRequestModel<SuburbViewModel> serviceRequestModel)
        {
            var repoObj = _genericUnitOfWork.GetRepository<Suburb, Guid>();

            var country = repoObj.GetById(serviceRequestModel.Model.Id);

            var mapped = _mapper.Map(serviceRequestModel.Model, country);

            repoObj.Update(mapped);

            _genericUnitOfWork.SaveChanges();
        }

        public SuburbViewModel GetSuburb(ServiceRequestModel<Guid> serviceRequestModel)
        {
            var suburbViewModel = new SuburbViewModel();

            if (serviceRequestModel.Model == Guid.Empty)
                return suburbViewModel;

            var repoObj = _genericUnitOfWork.GetRepository<Suburb, Guid>().GetById(serviceRequestModel.Model);

            suburbViewModel = _mapper.Map<Suburb, SuburbViewModel>(repoObj);

            suburbViewModel.StateProvinceId = repoObj.PostCode.City.StateProvinceId;
            suburbViewModel.CityId = repoObj.PostCode.CityId;

            return suburbViewModel;
        }

        public void DeleteSuburb(ServiceRequestModel<Guid> serviceRequestModel)
        {
            try
            {

                var repo = _genericUnitOfWork.GetRepository<Suburb, Guid>();
                var data = repo.GetById(serviceRequestModel.Model);

                repo.Delete(data);

                _genericUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot delete Suburb. Value already in use!");
            }
        }
        #endregion

        #region Street Setup
        public void AddStreet(ServiceRequestModel<StreetViewModel> serviceRequestModel)
        {
            var mappedData = _mapper.Map<StreetViewModel, Street>(serviceRequestModel.Model);
            _genericUnitOfWork.GetRepository<Street, Guid>().Add(mappedData);

            mappedData.Id = Guid.NewGuid();

            _genericUnitOfWork.SaveChanges();
        }

        public DataSourceResult ListStreet(ServiceRequestModel<KendoDataRequest> serviceRequestModel)
        {
            var queryable = _genericUnitOfWork.GetRepository<Street, Guid>().GetAll();
            return queryable.ToDataSourceResult<Street, StreetListViewModel>(serviceRequestModel.Model, new Sort() { Field = "StreetName", Dir = "asc" });
        }

        public void UpdateStreet(ServiceRequestModel<StreetViewModel> serviceRequestModel)
        {
            var repoObj = _genericUnitOfWork.GetRepository<Street, Guid>();

            var street = repoObj.GetById(serviceRequestModel.Model.Id);

            var mapped = _mapper.Map(serviceRequestModel.Model, street);

            repoObj.Update(mapped);

            _genericUnitOfWork.SaveChanges();
        }

        public StreetViewModel GetStreet(ServiceRequestModel<Guid> serviceRequestModel)
        {
            var streetViewModel = new StreetViewModel();

            if (serviceRequestModel.Model == Guid.Empty)
                return streetViewModel;

            var repoObj = _genericUnitOfWork.GetRepository<Street, Guid>().GetById(serviceRequestModel.Model);

            streetViewModel = _mapper.Map<Street, StreetViewModel>(repoObj);

            streetViewModel.StateProvinceId = repoObj.Suburb.PostCode.City.StateProvinceId;
            streetViewModel.CityId = repoObj.Suburb.PostCode.CityId;
            streetViewModel.PostCodeId = repoObj.Suburb.PostCodeId;
            streetViewModel.SuburbId = repoObj.Suburb.Id;

            return streetViewModel;
        }

        public void DeleteStreet(ServiceRequestModel<Guid> serviceRequestModel)
        {
            try
            {
                var repo = _genericUnitOfWork.GetRepository<Street, Guid>();
                var data = repo.GetById(serviceRequestModel.Model);

                repo.Delete(data);

                _genericUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot delete Street. Value already in use!");
             }
        }
        #endregion
    }
}
