﻿using ASPVueProject.Common.Configuration;
using ASPVueProject.Data.Infrastructure;
using ASPVueProject.Domain.Entity;
using ASPVueProject.Domain.Enum;
using ASPVueProject.DTO.Configuration;
using ASPVueProject.DTO.ViewModel;
using ASPVueProject.Service.Interface;
using Kendo.DynamicLinq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ASPVueProject.Service.Service
{
    public class SettingsService : ISettingsService
    {
        private readonly IDiObjectMapper _mapper;
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        private readonly JobSchedular _jobSchedular;
        public SettingsService(
            IDiObjectMapper mapper,
            IGenericUnitOfWork genericUnitOfWork, JobSchedular jobSchedular)
        {
            _mapper = mapper;
            _genericUnitOfWork = genericUnitOfWork;
            _jobSchedular = jobSchedular;
        }

        #region Purpose Of Order
        public DataSourceResult ListPurposeOfOrder(ServiceRequestModel<KendoDataRequest> request)
        {
            var queryable = _genericUnitOfWork.GetRepository<PurposeOfOrder, Guid>().GetAll();

            return queryable.ToDataSourceResult<PurposeOfOrder, PurposeOfOrderListViewModel>(request.Model, new Sort() { Field = "Name", Dir = "asc" });
        }
        public PurposeOfOrderViewModel AddPurposeOfOrder(ServiceRequestModel<PurposeOfOrderViewModel> serviceRequestModel)
        {
            var repo = _genericUnitOfWork.GetRepository<PurposeOfOrder, Guid>();
            var purposeOfOrder = repo.GetAll();

            var PurposeOfOrderExists = purposeOfOrder.Any(c => c.Name.Trim() == serviceRequestModel.Model.Name.Trim());
            if (PurposeOfOrderExists)
                throw new ArgumentNullException($"PurposeOfOrder Already exists", innerException: null);

            var mapped = _mapper.Map<PurposeOfOrderViewModel, PurposeOfOrder>(serviceRequestModel.Model);
            repo.Add(mapped);
            _genericUnitOfWork.SaveChanges();
            return _mapper.Map<PurposeOfOrder, PurposeOfOrderViewModel>(mapped);

        }
        public void UpdatePurposeOfOrder(ServiceRequestModel<PurposeOfOrderViewModel> serviceRequestModel)
        {
            var repo = _genericUnitOfWork.GetRepository<PurposeOfOrder, Guid>();
            var purposeOfOrder = repo.GetAll();

            var PurposeOfOrderExists = purposeOfOrder.Any(c => c.Name.Trim() == serviceRequestModel.Model.Name.Trim() && c.Id != serviceRequestModel.Model.Id);
            if (PurposeOfOrderExists)
                throw new ArgumentNullException($"PurposeOfOrder already exists in the system", innerException: null);

            var data = repo.GetById(serviceRequestModel.Model.Id);
            var mapped = _mapper.Map(serviceRequestModel.Model, data);

            repo.Update(mapped);
            _genericUnitOfWork.SaveChanges();
        }
        public PurposeOfOrderViewModel GetPurposeOfOrder(ServiceRequestModel<Guid> request)
        {
            var purposeOfOrderViewModel = _mapper.Map<PurposeOfOrder, PurposeOfOrderViewModel>(_genericUnitOfWork.GetRepository<PurposeOfOrder, Guid>().GetById(request.Model));
            if (request.Model == Guid.Empty)
                purposeOfOrderViewModel = new PurposeOfOrderViewModel();

            return purposeOfOrderViewModel;
        }

        public void DeletePurposeOfOrder(ServiceRequestModel<Guid> serviceRequestModel)
        {
            var PurposeOfOrderRepo = _genericUnitOfWork.GetRepository<PurposeOfOrder, Guid>();
            var data = PurposeOfOrderRepo.GetById(serviceRequestModel.Model);
            PurposeOfOrderRepo.Delete(data);
            _genericUnitOfWork.SaveChanges();
        }
        #endregion

        #region  Nature Of Business
        public DataSourceResult ListNatureOfBusiness(ServiceRequestModel<KendoDataRequest> request)
        {
            var queryable = _genericUnitOfWork.GetRepository<NatureOfBusiness, Guid>().GetAll();

            return queryable.ToDataSourceResult<NatureOfBusiness, NatureOfBusinessListViewModel>(request.Model, new Sort() { Field = "Name", Dir = "asc" });
        }
        public NatureOfBusinessViewModel AddNatureOfBusiness(ServiceRequestModel<NatureOfBusinessViewModel> serviceRequestModel)
        {
            var repo = _genericUnitOfWork.GetRepository<NatureOfBusiness, Guid>();
            var natureOfBusiness = repo.GetAll();

            var NatureOfBusinessExists = natureOfBusiness.Any(c => c.Name.Trim() == serviceRequestModel.Model.Name.Trim());
            if (NatureOfBusinessExists)
                throw new ArgumentNullException($"NatureOfBusiness Already exists", innerException: null);

            var mapped = _mapper.Map<NatureOfBusinessViewModel, NatureOfBusiness>(serviceRequestModel.Model);
            repo.Add(mapped);
            _genericUnitOfWork.SaveChanges();
            return _mapper.Map<NatureOfBusiness, NatureOfBusinessViewModel>(mapped);

        }
        public void UpdateNatureOfBusiness(ServiceRequestModel<NatureOfBusinessViewModel> serviceRequestModel)
        {
            var repo = _genericUnitOfWork.GetRepository<NatureOfBusiness, Guid>();
            var natureOfBusiness = repo.GetAll();

            var natureOfBusinessExists = natureOfBusiness.Any(c => c.Name.Trim() == serviceRequestModel.Model.Name.Trim() && c.Id != serviceRequestModel.Model.Id);
            if (natureOfBusinessExists)
                throw new ArgumentNullException($"NatureOfBusiness already exists in the system", innerException: null);

            var data = repo.GetById(serviceRequestModel.Model.Id);
            var mapped = _mapper.Map(serviceRequestModel.Model, data);

            repo.Update(mapped);
            _genericUnitOfWork.SaveChanges();
        }
        public NatureOfBusinessViewModel GetNatureOfBusiness(ServiceRequestModel<Guid> request)
        {
            var natureOfBusinessViewModel = _mapper.Map<NatureOfBusiness, NatureOfBusinessViewModel>(_genericUnitOfWork.GetRepository<NatureOfBusiness, Guid>().GetById(request.Model));
            if (request.Model == Guid.Empty)
                natureOfBusinessViewModel = new NatureOfBusinessViewModel();

            return natureOfBusinessViewModel;
        }

        public void DeleteNatureOfBusiness(ServiceRequestModel<Guid> serviceRequestModel)
        {
            var natureOfBusinessRepo = _genericUnitOfWork.GetRepository<NatureOfBusiness, Guid>();
            var data = natureOfBusinessRepo.GetById(serviceRequestModel.Model);
            natureOfBusinessRepo.Delete(data);
            _genericUnitOfWork.SaveChanges();
        }
        #endregion

        #region Secret Question
        public DataSourceResult ListSecretQuestion(ServiceRequestModel<KendoDataRequest> request)
        {
            var queryable = _genericUnitOfWork.GetRepository<SecretQuestion, Guid>().GetAll();

            return queryable.ToDataSourceResult<SecretQuestion, SecretQuestionListViewModel>(request.Model, new Sort() { Field = "Name", Dir = "asc" });
        }
        public SecretQuestionViewModel AddSecretQuestion(ServiceRequestModel<SecretQuestionViewModel> serviceRequestModel)
        {
            var repo = _genericUnitOfWork.GetRepository<SecretQuestion, Guid>();
            var secretQuestion = repo.GetAll();

            var secretQuestionExists = secretQuestion.Any(c => c.Name.Trim() == serviceRequestModel.Model.Name.Trim());
            if (secretQuestionExists)
                throw new ArgumentNullException($"SecretQuestion Already exists", innerException: null);

            var mapped = _mapper.Map<SecretQuestionViewModel, SecretQuestion>(serviceRequestModel.Model);
            repo.Add(mapped);
            _genericUnitOfWork.SaveChanges();
            return _mapper.Map<SecretQuestion, SecretQuestionViewModel>(mapped);

        }
        public void UpdateSecretQuestion(ServiceRequestModel<SecretQuestionViewModel> serviceRequestModel)
        {
            var repo = _genericUnitOfWork.GetRepository<SecretQuestion, Guid>();
            var secretQuestion = repo.GetAll();

            var secretQuestionExists = secretQuestion.Any(c => c.Name.Trim() == serviceRequestModel.Model.Name.Trim() && c.Id != serviceRequestModel.Model.Id);
            if (secretQuestionExists)
                throw new ArgumentNullException($"SecretQuestion already exists in the system", innerException: null);

            var data = repo.GetById(serviceRequestModel.Model.Id);
            var mapped = _mapper.Map(serviceRequestModel.Model, data);

            repo.Update(mapped);
            _genericUnitOfWork.SaveChanges();
        }
        public SecretQuestionViewModel GetSecretQuestion(ServiceRequestModel<Guid> request)
        {
            var SecretQuestionViewModel = _mapper.Map<SecretQuestion, SecretQuestionViewModel>(_genericUnitOfWork.GetRepository<SecretQuestion, Guid>().GetById(request.Model));
            if (request.Model == Guid.Empty)
                SecretQuestionViewModel = new SecretQuestionViewModel();

            return SecretQuestionViewModel;
        }

        public void DeleteSecretQuestion(ServiceRequestModel<Guid> serviceRequestModel)
        {
            var SecretQuestionRepo = _genericUnitOfWork.GetRepository<SecretQuestion, Guid>();
            var data = SecretQuestionRepo.GetById(serviceRequestModel.Model);
            SecretQuestionRepo.Delete(data);
            _genericUnitOfWork.SaveChanges();
        }
        #endregion

      


    }
}
