﻿using System;
using ASPVueProject.Data.Infrastructure;
using ASPVueProject.Domain.Entity;
using ASPVueProject.Domain.Enum;
using ASPVueProject.DTO.Configuration;
using ASPVueProject.DTO.ViewModel;
using ASPVueProject.Service.Interface;

namespace ASPVueProject.Service.Service
{
	public class ErrorLogService : IErrorLogService
    {
        private readonly IDiObjectMapper _objectMapper;
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        public ErrorLogService(IDiObjectMapper objectMapper, IGenericUnitOfWork genericUnitOfWork)
        {
            _objectMapper = objectMapper;
            _genericUnitOfWork = genericUnitOfWork;
        }
        public bool Add(ServiceRequestModel<ErrorLogViewModel> serviceModel)
        {
            try
            {
                var mappedData = _objectMapper.Map<ErrorLogViewModel, ErrorLog>(serviceModel.Model);
                mappedData.Date = DateTime.Now;
                mappedData.IpAddress = serviceModel.IpAddress;
                mappedData.UserId = serviceModel.LoginInfo.UserId;
                _genericUnitOfWork.GetRepository<ErrorLog, int>().Add(mappedData);
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
    public class ApplicationLogService : IApplicationLogService
    {
        private readonly IDiObjectMapper _objectMapper;
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        public ApplicationLogService(IDiObjectMapper objectMapper, IGenericUnitOfWork genericUnitOfWork)
        {
            _objectMapper = objectMapper;
            _genericUnitOfWork = genericUnitOfWork;
        }
        public bool AddActivityApplicationLog(ServiceRequestModel<ApplicationLogViewModel> serviceModel)
        {
            try
            {
                var mappedData = _objectMapper.Map<ApplicationLogViewModel, ApplicationLog>(serviceModel.Model);
                mappedData.Date = DateTime.Now;
                mappedData.LogType = LogType.Activity;
                mappedData.IpAddress = serviceModel.IpAddress;
                mappedData.UserId = serviceModel.LoginInfo.UserId;
                _genericUnitOfWork.GetRepository<ApplicationLog, int>().Add(mappedData);
                _genericUnitOfWork.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool AddSystemApplicationLog(ServiceRequestModel<ApplicationLogViewModel> serviceModel)
        {
            try
            {
                var mappedData = _objectMapper.Map<ApplicationLogViewModel, ApplicationLog>(serviceModel.Model);
                mappedData.Date = DateTime.Now;
                mappedData.LogType = LogType.System;
                mappedData.IpAddress = serviceModel.IpAddress;
                mappedData.UserId = serviceModel.LoginInfo.UserId;
                _genericUnitOfWork.GetRepository<ApplicationLog, int>().Add(mappedData);
                _genericUnitOfWork.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
