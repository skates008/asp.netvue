﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace ASPVueProject.DTO.Configuration
{
    public static class BaseExtension
	{
		public static string ToDescription(this Enum value)
		{
			try
			{
				FieldInfo fi = value.GetType().GetField(value.ToString());

				DescriptionAttribute[] attributes =
					(DescriptionAttribute[])fi.GetCustomAttributes(
						typeof(DescriptionAttribute),
						false);

				if (attributes.Length > 0)
					return attributes[0].Description;
				return value.ToString();
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
		}
	}
}
