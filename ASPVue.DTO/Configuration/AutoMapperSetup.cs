﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;

namespace ASPVueProject.DTO.Configuration
{
	public class AutoMapperSetup
	{
		public static IMapper Mapper { get; set; }
		public static void SetupMappers(Assembly assembly)
		{
			var types = assembly.GetExportedTypes();
			LoadCustomMappings(types);
		}
		private static void LoadCustomMappings(IEnumerable<Type> types)
		{
			var profiles =
			from t in types
			where typeof(Profile).IsAssignableFrom(t)
			select (Profile)Activator.CreateInstance(t);

			var config = new MapperConfiguration(cfg =>
			{
				foreach (var profile in profiles)
				{
					cfg.AddProfile(profile);
				}
			});
			Mapper = config.CreateMapper();
		}
	}
}
