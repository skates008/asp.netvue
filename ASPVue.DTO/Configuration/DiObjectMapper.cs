﻿namespace ASPVueProject.DTO.Configuration
{
	public interface IDiObjectMapper
	{
		TDestination Map<TSource, TDestination>(TSource source)
			where TSource : class
			where TDestination : class;

		TDestination Map<TSource, TDestination>(TSource source, TDestination destination)
			where TSource : class
			where TDestination : class;
	}
	public interface IMapInto<TDestination>
		where TDestination : class
	{
		TDestination MapInto(TDestination destinationObject);
	}
	public class DiObjectMapper : IDiObjectMapper
	{
		public TDestination Map<TSource, TDestination>(TSource source)
			where TSource : class
			where TDestination : class
		{
			return AutoMapperSetup.Mapper.Map<TSource, TDestination>(source);
		}
		public TDestination Map<TSource, TDestination>(TSource source, TDestination destination)
			where TSource : class
			where TDestination : class
		{
			if (typeof(IMapInto<TDestination>).IsAssignableFrom(typeof(TSource)))
			{
				return ((IMapInto<TDestination>)source).MapInto(destination);
			}
			return AutoMapperSetup.Mapper.Map(source, destination);
		}
	}
}
