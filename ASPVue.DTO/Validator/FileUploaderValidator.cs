﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ASPVueProject.DTO.Validator
{
	#region File Size
	public class FileSizeAttribute : ValidationAttribute, IClientValidatable
	{
		private string _errorMessage = "{0} can not be larger than {1}MB";
		public double MaxFileSize { get; private set; }

		public FileSizeAttribute(double maxFileSize)
		{
			MaxFileSize = maxFileSize;
		}

		public override bool IsValid(object value)
		{
			if (value == null)
				return true;
			if (IsValidMaxFileSize((value as HttpPostedFileBase).ContentLength)) return true;
			ErrorMessage = string.Format(_errorMessage, "{0}", MaxFileSize);
			return false;
		}

		public override string FormatErrorMessage(string name)
		{
			return string.Format(_errorMessage, name, MaxFileSize);
		}

		public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
		{
			var clientValidationRule = new ModelClientValidationRule()
			{
				ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
				ValidationType = "filesize"
			};
			clientValidationRule.ValidationParameters.Add("size", MaxFileSize);
			return new[] { clientValidationRule };
		}

		private bool IsValidMaxFileSize(int fileSize)
		{
			return (ConvertBytesToMegaBytes(fileSize) <= MaxFileSize);
		}

		private static double ConvertBytesToMegaBytes(int bytes)
		{
			return (bytes / 1024f) / 1024f;
		}
	}
	#endregion

	#region File Type
	public class FileTypeAttribute : ValidationAttribute, IClientValidatable
	{
		private string _errorMessage = "{0} must be one of the following file types: {1}";

		public string[] ValidFileTypes { get; set; }

		public FileTypeAttribute(params string[] validFileTypes)
		{
			ValidFileTypes = validFileTypes;
		}

		public override bool IsValid(object value)
		{
			var file = value as HttpPostedFileBase;
			if (value == null || string.IsNullOrEmpty(file?.FileName))
				return true;
			if (ValidFileTypes == null) return true;
			var validTypeFound = false;
			foreach (var validFileType in ValidFileTypes)
			{
				var fileNameParts = file.FileName.Split('.');
				if (fileNameParts[fileNameParts.Length - 1] != validFileType) continue;
				validTypeFound = true;
				break;
			}
			if (validTypeFound) return true;
			ErrorMessage = string.Format(_errorMessage, "{0}", String.Join("/", ValidFileTypes));
			return false;
		}

		public override string FormatErrorMessage(string name)
		{
			return string.Format(_errorMessage, name, string.Join("/", ValidFileTypes));
		}

		public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
		{
			var clientValidationRule = new ModelClientValidationRule()
			{
				ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
				ValidationType = "filetype"
			};

			clientValidationRule.ValidationParameters.Add("filetypes", string.Join(",", ValidFileTypes));
			return new[] { clientValidationRule };
		}
	}
	#endregion

	#region File Type and File Size
	public class FileUploadAttribute : ValidationAttribute, IClientValidatable
	{
		private readonly FileSizeAttribute _fileSizeAttribute;
		private readonly FileTypeAttribute _fileTypeAttribute;

		public FileUploadAttribute(params string[] validFileTypes)
			: base()
		{
			_fileTypeAttribute = new FileTypeAttribute(validFileTypes);
		}

		public FileUploadAttribute(
			double maxFileSize, params string[] validFileTypes)
			: base()
		{
			_fileSizeAttribute = new FileSizeAttribute(maxFileSize);
			_fileTypeAttribute = new FileTypeAttribute(validFileTypes);
		}

		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			if (value == null)
				return ValidationResult.Success;
			try
			{
				if (value.GetType() != typeof(HttpPostedFileWrapper))
					throw new InvalidOperationException("");
				var errorMessage = new StringBuilder();
				var file = value as HttpPostedFileBase;
				if (_fileSizeAttribute != null)
				{
					if (!_fileSizeAttribute.IsValid(file))
						errorMessage.Append($"{_fileSizeAttribute.FormatErrorMessage(validationContext.DisplayName)}. ");
				}

				if (_fileTypeAttribute == null)
					return string.IsNullOrEmpty(errorMessage.ToString())
						? ValidationResult.Success
						: new ValidationResult(errorMessage.ToString());
				if (!_fileTypeAttribute.IsValid(file))
					errorMessage.Append($"{_fileTypeAttribute.FormatErrorMessage(validationContext.DisplayName)}. ");

				return string.IsNullOrEmpty(errorMessage.ToString()) ? ValidationResult.Success : new ValidationResult(errorMessage.ToString());

			}
			catch (Exception ex)
			{
				return new ValidationResult(ex.Message);
			}
		}

		public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
		{
			var clientValidationRule = new ModelClientValidationRule()
			{
				ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
				ValidationType = "fileupload"
			};
			var clientvalidationmethods = new List<string>();
			var parameters = new List<string>();
			var errormessages = new List<string>();
			if (_fileSizeAttribute != null)
			{
				clientvalidationmethods.Add(_fileSizeAttribute.GetClientValidationRules(metadata, context).First().ValidationType);
				parameters.Add(_fileSizeAttribute.MaxFileSize.ToString());
				errormessages.Add(_fileSizeAttribute.FormatErrorMessage(metadata.GetDisplayName()));
			}

			if (_fileTypeAttribute != null)
			{
				clientvalidationmethods.Add(_fileTypeAttribute.GetClientValidationRules(metadata, context).First().ValidationType);
				parameters.Add(string.Join(",", _fileTypeAttribute.ValidFileTypes));
				errormessages.Add(_fileTypeAttribute.FormatErrorMessage(metadata.GetDisplayName()));
			}

			clientValidationRule.ValidationParameters.Add("clientvalidationmethods", string.Join(",", clientvalidationmethods));
			clientValidationRule.ValidationParameters.Add("parameters", string.Join("|", parameters));
			clientValidationRule.ValidationParameters.Add("errormessages", string.Join(",", errormessages));
			yield return clientValidationRule;
		}

		private double ConvertBytesToMegabytes(long bytes)
		{
			return (bytes / 1024f) / 1024f;
		}
	}
	#endregion
}
