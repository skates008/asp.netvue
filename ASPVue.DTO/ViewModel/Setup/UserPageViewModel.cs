﻿using System.Collections.Generic;
using AutoMapper;
using ASPVueProject.Domain.Entity;

namespace ASPVueProject.DTO.ViewModel
{
	public class UserPageViewModel
	{
		public int PageId { get; set; }
		public string PagePath { get; set; }
		public string PageTitle { get; set; }
		public string PageIcon { get; set; }
		public int PageDisplayOrder { get; set; }
		public int? PageParentPageId { get; set; }
		public List<UserPageViewModel> ChildPages { get; set; }
		public string name { get; set; }
		public string url { get; set; }
		public string icon { get; set; }
		public List<UserPageViewModel> children { get; set; }
	}
	public class TreeViewModel
	{
		public int id { get; set; }
		public string text { get; set; }
		public int? parentid { get; set; }
		public bool @checked { get; set; }
		public bool expanded { get; set; }
		public int displayorder { get; set; }
		public IList<TreeViewModel> items { get; set; }
	}
	public class RolePageViewModel
	{
		public int PageId { get; set; }
		public bool Display { get; set; }
	}
	public class UserPageMapper : Profile
	{
		public UserPageMapper()
		{
			CreateMap<UserPage, UserPageViewModel>()
				.ForMember(x => x.PagePath, opts => opts.ResolveUsing(s => s.Page.Path))
				.ForMember(x => x.PageTitle, opts => opts.ResolveUsing(s => s.Page.Title))
				.ForMember(x => x.PageIcon, opts => opts.ResolveUsing(s => s.Page.Icon))
				.ForMember(x => x.PageDisplayOrder, opts => opts.ResolveUsing(s => s.Page.DisplayOrder))
				.ForMember(x => x.PageParentPageId, opts => opts.ResolveUsing(s => s.Page.ParentPageId));
			CreateMap<RolePageViewModel, UserPage>();
			CreateMap<UserPage, RolePageViewModel>();
		}
	}
}
