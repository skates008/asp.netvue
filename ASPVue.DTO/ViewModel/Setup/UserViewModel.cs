﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using ASPVueProject.Domain.Entity;
using ASPVueProject.Domain.Enum;
using ASPVueProject.DTO.Configuration;

namespace ASPVueProject.DTO.ViewModel
{
	public class UserViewModel
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        [EmailAddress]
        public string Username { get; set; }
        [Required]
        public string FullName { get; set; }
        public UserStatus UserStatus { get; set; }
        [Required]
        public Guid RoleId { get; set; }
    }
    public class UserListViewModel
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
        public string FullName { get; set; }
        public bool SystemDefined { get; set; }
        public UserType UserType { get; set; }
        public string UserStatus { get; set; }
    }
    public class UserMapper : Profile
    {
        public UserMapper()
        {
            CreateMap<UserViewModel, User>()
                .ForMember(x => x.Type, opts => opts.ResolveUsing(s => UserType.SystemUser))
                .ForMember(x => x.Email, opts => opts.ResolveUsing(s => s.Username));
            CreateMap<User, UserViewModel>()
                .ForMember(x => x.RoleId, opts => opts.ResolveUsing(s => s.Roles.FirstOrDefault()?.RoleId));
            CreateMap<User, UserListViewModel>()
                .ForMember(x => x.Username, opts => opts.ResolveUsing(s => s.Email))
                .ForMember(x => x.Role, opts => opts.ResolveUsing(s => s.Roles.FirstOrDefault()?.Role.Name))
                .ForMember(a => a.UserStatus, opts => opts.ResolveUsing(d => d.Status.ToDescription()));
        }
    }
}
