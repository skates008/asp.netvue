﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ASPVueProject.Domain.Entity;

namespace ASPVueProject.DTO.ViewModel
{
    public class RoleViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public ICollection<RolePageViewModel> UserPages { get; set; }
        public ICollection<RolePageTreeViewModel> Pages { get; set; }
        public bool IsSystemDefined { get; set; }
    }

    public class RolePageTreeViewModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int? ParentId { get; set; }
        public bool Checked { get; set; }
        public bool Expanded { get; set; }
        public int DisplayOrder { get; set; }
        public IList<RolePageTreeViewModel> Pages { get; set; }
    }

    public class RoleListViewModel
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string IsActive { get; set; }
		public bool SystemDefined { get; set; }
	}

	public class RoleMapper : Profile
	{
		public RoleMapper()
		{
			CreateMap<RoleViewModel, Role>()
				.ForMember(a => a.UserPages, opts => opts.ResolveUsing(d => d.UserPages.Where(c => c.Display)));
			CreateMap<Role, RoleViewModel>();
			CreateMap<Role, RoleListViewModel>()
				.ForMember(a => a.IsActive, opts => opts.ResolveUsing(d => d.IsActive ? "Yes" : "No"));
		}
	}
}
