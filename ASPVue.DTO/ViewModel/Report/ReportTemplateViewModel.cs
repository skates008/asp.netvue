﻿using System.Collections.Generic;

namespace ASPVueProject.DTO.ViewModel
{
	public class TestViewModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
	public class ReportSampleViewModel
	{
		public string ReportName { get; set; }
		public string Description { get; set; }
	}

	public class ReportHeaderViewModel
	{
		public string ReportTitle { get; set; }
		public Dictionary<string, object> FilterText { get; set; }
	}

	public class ReportTemplateViewModel<T>
	{
		public T Model { get; set; }
		public int Skip { get; set; }
	}

	public class ReportRequestViewModel
	{
		public int Take { get; set; }
		public int Skip { get; set; }
		public Dictionary<string, object> Filter { get; set; }
		public Dictionary<string, object> FilterText { get; set; }
		public bool IsExport { get; set; }
		public string ExportType { get; set; }
	}

	public class ReportThirdPartyLogRequestViewModel
	{
		public string FromDate { get; set; }
		public string ToDate { get; set; }
		public string FullName { get; set; }
		public string PhoneNumber { get; set; }
		public string ApplicationType { get; set; }
	}

	public class ReportResponseViewModel<T>
	{
		public T Data { get; set; }
		public string Content { get; set; }
		public int Total { get; set; }
		public int CurrentTotal { get; set; }
	}

	public class ReceiptResponseViewModel<T>
	{
		public T Data { get; set; }
		public string Content { get; set; }
	}


}
