﻿using AutoMapper;
using ASPVueProject.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.DTO.ViewModel
{
    public class PostCodeViewModel
    {
        public Guid Id { get; set; }
        public Guid CityId { get; set; }
        public string PostCodeName { get; set; }
        public Guid StateProvinceId { get; set; }
        public bool IsEnabled { get; set; }
    }

    public class PostCodeListViewModel
    {
        public Guid Id { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string PostCodeName { get; set; }
        public string IsEnabled { get; set; }
    }

    public class PostCodeMapper : Profile
    {
        public PostCodeMapper()
        {
            CreateMap<PostCodeViewModel, PostCode>();
            CreateMap<PostCode, PostCodeViewModel>();
            CreateMap<PostCode, PostCodeListViewModel>()
                .ForMember(a => a.StateName, opts => opts.ResolveUsing(d => d.City.StateProvince.StateName))
                  .ForMember(a => a.CityName, opts => opts.ResolveUsing(d => d.City.CityName))
                  .ForMember(a => a.IsEnabled, opts => opts.ResolveUsing(d => d.IsEnabled ? "Yes" : "No"));
        }
    }
}
