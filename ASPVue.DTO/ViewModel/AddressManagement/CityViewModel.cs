﻿using AutoMapper;
using ASPVueProject.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.DTO.ViewModel
{
    public class CityViewModel
    {
        public Guid Id { get; set; }
        public string CityName { get; set; }
        public Guid StateProvinceId { get; set; }
    }

    public class CityListViewModel
    {
        public Guid Id { get; set; }
        public string CityName { get; set; }
        public Guid StateProvinceId { get; set; }
        public string StateName { get; set; }
    }

    public class CityMapper : Profile
    {
        public CityMapper()
        {
            CreateMap<CityViewModel, City>();//.ForMember(a => a.Country, opts => opts.ResolveUsing(d => d.Country));
            CreateMap<City, CityViewModel>();//.ForMember(a => a.Country, opts => opts.ResolveUsing(d => d.Country));
            CreateMap<City, CityListViewModel>()
                .ForMember(a => a.StateName, opts => opts.ResolveUsing(d => d.StateProvince.StateName));
        }

    }
}
