﻿using AutoMapper;
using ASPVueProject.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.DTO.ViewModel
{
    public class CountryViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string Nationality { get; set; }
        public string Code { get; set; }
        public bool IsEnable { get; set; }
        public bool IsHighRisk { get; set; }

    }
    public class CountryListViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Nationality { get; set; }
        public string Code { get; set; }
        public string IsEnable { get; set; }
        public string IsHighRisk { get; set; }

    }
    public class CountryMapper : Profile
    {
        public CountryMapper()
        {
            CreateMap<CountryViewModel, Country>();
            CreateMap<Country, CountryViewModel>();
            CreateMap<Country, CountryListViewModel>()
                .ForMember(a => a.IsEnable, opts => opts.ResolveUsing(d => d.IsEnable ? "Yes" : "No"))
                .ForMember(a => a.IsHighRisk, opts => opts.ResolveUsing(d => d.IsHighRisk ? "Yes" : "No"));
        }

    }
}
