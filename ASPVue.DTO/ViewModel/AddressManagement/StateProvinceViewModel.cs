﻿using AutoMapper;
using ASPVueProject.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.DTO.ViewModel
{
    public class StateProvinceViewModel
    {
        public Guid Id { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public string DisplayName { get; set; }
        public Guid CountryId { get; set; }
    }
    public class StateProvinceListViewModel
    {
        public Guid Id { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public string DisplayName { get; set; }
        public Guid CountryId { get; set; }
        public string CountryName { get; set; }
    }

    public class StateProvinceMapper : Profile
    {
        public StateProvinceMapper()
        {
            CreateMap<StateProvinceViewModel, StateProvince>();//.ForMember(a => a.Country, opts => opts.ResolveUsing(d => d.Country));
            CreateMap<StateProvince, StateProvinceViewModel>();//.ForMember(a => a.Country, opts => opts.ResolveUsing(d => d.Country));
            CreateMap<StateProvince, StateProvinceListViewModel>()
                .ForMember(a => a.CountryName, opts => opts.ResolveUsing(d => d.Country.Name));
        }

    }
}
