﻿using AutoMapper;
using ASPVueProject.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.DTO.ViewModel
{
    public class StreetViewModel
    {
        public Guid Id { get; set; }
        public Guid SuburbId { get; set; }
        public Guid PostCodeId { get; set; }
        public Guid StateProvinceId { get; set; }
        public Guid CityId { get; set; }
        public string StreetName { get; set; }
        public bool IsEnabled { get; set; }
    }
    public class StreetListViewModel
    {
        public Guid Id { get; set; }
        public string StreetName { get; set; }
        public string SuburbName { get; set; }
        public string IsEnabled { get; set; }
        public string PostCodeName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
    }

    public class StreetMapper : Profile
    {
        public StreetMapper()
        {
            CreateMap<StreetViewModel, Street>();
            CreateMap<Street, StreetViewModel>();
            CreateMap<Street, StreetListViewModel>()
                  .ForMember(a => a.PostCodeName, opts => opts.ResolveUsing(d => d.Suburb.PostCode.PostCodeName))
                  .ForMember(a => a.IsEnabled, opts => opts.ResolveUsing(d => d.IsEnabled ? "Yes" : "No"))
                  .ForMember(a => a.CityName, opts => opts.ResolveUsing(d => d.Suburb.PostCode.City.CityName))
                  .ForMember(a => a.StateName, opts => opts.ResolveUsing(d => d.Suburb.PostCode.City.StateProvince.StateName))
                  .ForMember(a => a.SuburbName, opts => opts.ResolveUsing(d => d.Suburb.SuburbName));
        }
    }
}
