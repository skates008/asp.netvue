﻿using AutoMapper;
using ASPVueProject.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.DTO.ViewModel
{
    public class SuburbViewModel
    {
        public Guid Id { get; set; }
        public string SuburbName { get; set; }
        public bool IsEnabled { get; set; }
        public Guid PostCodeId { get; set; }
        public Guid StateProvinceId { get; set; }
        public Guid CityId { get; set; }
    }
    public class SuburbListViewModel
    {
        public Guid Id { get; set; }
        public string SuburbName { get; set; }
        public string IsEnabled { get; set; }
        public string PostCodeName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
    }

    public class SuburbMapper : Profile
    {
        public SuburbMapper()
        {
            CreateMap<SuburbViewModel, Suburb>();
            CreateMap<Suburb, SuburbViewModel>();
            CreateMap<Suburb, SuburbListViewModel>()
                  .ForMember(a => a.PostCodeName, opts => opts.ResolveUsing(d => d.PostCode.PostCodeName))
                  .ForMember(a => a.IsEnabled, opts => opts.ResolveUsing(d => d.IsEnabled ? "Yes" : "No"))
                  .ForMember(a => a.CityName, opts => opts.ResolveUsing(d => d.PostCode.City.CityName))
                  .ForMember(a => a.StateName, opts => opts.ResolveUsing(d => d.PostCode.City.StateProvince.StateName));
        }
    }
}
