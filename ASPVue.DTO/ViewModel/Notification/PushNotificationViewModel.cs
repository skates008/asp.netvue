﻿using AutoMapper;
using ASPVueProject.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ASPVueProject.Domain.Entity;
using ASPVueProject.DTO.Configuration;

namespace ASPVueProject.DTO.ViewModel
{
	public class NotificationViewModel
	{
		[Required]
		public string Body { get; set; }
		[Required]
		public string Title { get; set; }
		public Guid Id { get; set; }
		public Guid? EndUserId { get; set; }
		[Required]
		public NotificationType NotificationType { get; set; }
	}

	public class NotificationDisplayViewModel
	{
		public string Body { get; set; }
		public string Title { get; set; }
		public Guid Id { get; set; }
		public DateTime CreatedDate { get; set; }
	}


	public class NotificationListViewModel
	{
		public string Title { get; set; }
		public Guid Id { get; set; }
		public string NotificationType { get; set; }
		public string CreatedDate { get; set; }
	}

	public class NotificationUpdateViewModel
	{
		public Guid Id { get; set; }
	}

	public class PushNotificationViewModel
	{
		public List<string> DeviceToken { get; set; }
		public NotificationViewModel Notification { get; set; }
	}


	public class NotificationMapper : Profile
	{
		public NotificationMapper()
		{
			CreateMap<NotificationViewModel, Notification>();
			CreateMap<Notification, NotificationViewModel>();

			CreateMap<NotificationDisplayViewModel, Notification>();
			CreateMap<Notification, NotificationDisplayViewModel>();

			CreateMap<Notification, NotificationListViewModel>()
				.ForMember(a => a.NotificationType, opts => opts.ResolveUsing(d => d.NotificationType.ToDescription()));
		}
	}
}
