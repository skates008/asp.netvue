﻿namespace ASPVueProject.DTO.ViewModel
{
	public class ErrorInformation
	{
		public string Code { get; set; }
		public string Message { get; set; }
	}
}
