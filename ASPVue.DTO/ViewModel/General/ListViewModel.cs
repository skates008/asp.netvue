﻿using AutoMapper;
using ASPVueProject.Domain.Entity;
using System;

namespace ASPVueProject.DTO.ViewModel
{
    public class ListViewModel
    {
        public object Id { get; set; }
        public string Text { get; set; }
    }
    public class MultiSelectListViewModel
    {
        public object Id { get; set; }
        public string Text { get; set; }
        public bool isSelected { get; set; }
        public int DisplayOrder { get; set; }
        public string ClassName { get; set; }
    }
    public class ListMapper : Profile
    {
        public ListMapper()
        {
            CreateMap<Role, ListViewModel>()
                .ForMember(a => a.Text, opts => opts.ResolveUsing(d => d.Name));
            CreateMap<User, ListViewModel>()
                .ForMember(a => a.Text, opts => opts.ResolveUsing(d => d.FullName));
            CreateMap<Country, ListViewModel>()
           .ForMember(a => a.Text, opts => opts.ResolveUsing(d => d.Name));
            CreateMap<StateProvince, ListViewModel>()
          .ForMember(a => a.Text, opts => opts.ResolveUsing(d => d.StateName));

            CreateMap<City, ListViewModel>()
       .ForMember(a => a.Text, opts => opts.ResolveUsing(d => d.CityName));
            CreateMap<PurposeOfOrder, ListViewModel>()
            .ForMember(a => a.Text, opts => opts.ResolveUsing(d => d.Type));

            CreateMap<PostCodeGroup, ListViewModel>()
      .ForMember(a => a.Text, opts => opts.ResolveUsing(d => d.PostCodeGroupName));

            CreateMap<PostCode, ListViewModel>()
           .ForMember(a => a.Text, opts => opts.ResolveUsing(d => d.PostCodeName));


            //CreateMap<AvailableBranch, ListViewModel>().ForMember(a => a.Text, opts => opts.ResolveUsing(d => d.BranchDetail.BranchName));

        }
    }

}
