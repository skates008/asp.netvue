﻿using System.Collections.Generic;

namespace ASPVueProject.DTO.ViewModel
{
	public class FilterViewModel
	{
		public int PageNumber { get; set; }
		public int PageSize { get; set; }
		public string Filter { get; set; }
	}

	public class FilterResponseViewModel<T>
	{
		public List<T> Data { get; set; }
		public int TotalRecords { get; set; }
		public int CurrentPage { get; set; }
	}
}
