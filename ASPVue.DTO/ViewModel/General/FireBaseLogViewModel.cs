﻿namespace ASPVueProject.DTO.ViewModel
{
	public class FireBaseLogResponseViewModel
	{
		public string Response { get; set; }
		public string Data { get; set; }
	}
	public class FireBaseLogViewModel
	{
		public string multicast_id { get; set; }
		public int success { get; set; }
		public int failure { get; set; }
		public int canonical_ids { get; set; }
		public object results { get; set; }
	}
}
