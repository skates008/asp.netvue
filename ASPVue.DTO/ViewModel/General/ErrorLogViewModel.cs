﻿using System;
using System.Reflection;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ASPVueProject.Domain.Enum;
using ASPVueProject.Domain.Entity;

namespace ASPVueProject.DTO.ViewModel
{
	public class ErrorLogViewModel
	{
		public int Id { get; set; }
		public string Entity { get; set; }
		public object Data { get; set; }
		public string Action { get; set; }
		public string Message { get; set; }
		public string ErrorCode { get; set; }
		public string InnerException { get; set; }

	}
	public class ErrorLogMapper : Profile
	{
		public ErrorLogMapper()
		{
			CreateMap<ErrorLogViewModel, ErrorLog>()
				.ForMember(d => d.Data, opts => opts.ResolveUsing(s => JsonConvert.SerializeObject(s.Data, Formatting.None, new JsonSerializerSettings() { ContractResolver = new CustomResolver(), NullValueHandling = NullValueHandling.Ignore, ReferenceLoopHandling = ReferenceLoopHandling.Ignore, PreserveReferencesHandling = PreserveReferencesHandling.None })));
		}
	}

	public class ApplicationLogViewModel
	{
		public int Id { get; set; }
		public string Entity { get; set; }
		public object Data { get; set; }
		public string Action { get; set; }
		public string Description { get; set; }
		public LogType LogType { get; set; }

	}
	public class ApplicationLogMapper : Profile
	{
		public ApplicationLogMapper()
		{
			CreateMap<ApplicationLogViewModel, ApplicationLog>()
			   .ForMember(d => d.Data, opts => opts.ResolveUsing(s => JsonConvert.SerializeObject(s.Data, Formatting.None, new JsonSerializerSettings() { ContractResolver = new CustomResolver(), NullValueHandling = NullValueHandling.Ignore, ReferenceLoopHandling = ReferenceLoopHandling.Ignore, PreserveReferencesHandling = PreserveReferencesHandling.None })));
		}
	}
	public class CustomResolver : DefaultContractResolver
	{
		protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
		{
			JsonProperty prop = base.CreateProperty(member, memberSerialization);

			if (/*prop.DeclaringType != typeof(PseudoContext) && || prop.PropertyType.IsGenericType*/
				(prop.PropertyType.IsClass) &&
				prop.PropertyType != typeof(string))
			{
				prop.ShouldSerialize = obj => false;
			}

			return prop;
		}
	}
}
