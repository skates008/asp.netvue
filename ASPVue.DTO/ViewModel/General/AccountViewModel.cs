﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using ASPVueProject.Domain.Entity;
using Newtonsoft.Json;

namespace ASPVueProject.DTO.ViewModel
{
    public class SignInViewModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }

        public string DeviceIdentifier { get; set; }
    }
    public class ResetPasswordModel
    {
        public string UserId { get; set; }
        public string Code { get; set; }
        [Required(ErrorMessage = "Password is required.")]
        public string NewPassword { get; set; }
        [Compare("NewPassword", ErrorMessage = "Password doesnot match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangeProfileViewModel
    {
        [Required(ErrorMessage = "Password is required.")]
        public string NewPassword { get; set; }
        [Compare("NewPassword", ErrorMessage = "Password doesnot match.")]
        public string ConfirmPassword { get; set; }
        public string OldPassword { get; set; }
        public Guid UserId { get; set; }

    }

    public class LoginInfo
    {
        public string FullName { get; set; }
        public string DisplayPicture { get; set; }
        public Guid UserId { get; set; }
        public string Role { get; set; }
        public Guid RoleId { get; set; }
        public int? RoleHierarchy { get; set; }
    }
    public class LoginInfoWeb
    {
        public Guid UserId { get; set; }
        public string FullName { get; set; }
        public string DisplayPicture { get; set; }
        public bool ForceChangePassword { get; set; }
        public string Role { get; set; }
        public Guid RoleId { get; set; }
    }

    public class TokenViewModel
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
        [JsonProperty("expires_in")]
        public uint ExpiresIn { get; set; }
        [JsonProperty("userName")]
        public string UserName { get; set; }
        [JsonProperty(".issued")]
        public DateTimeOffset Issued { get; set; }
        [JsonProperty(".expires")]
        public DateTimeOffset Expires { get; set; }
        public LoginInfoWeb LoginInfo { get; set; }
        public List<UserPageViewModel> UserPages { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required(ErrorMessage = "Password is required.")]
        public string NewPassword { get; set; }
        [Compare("NewPassword", ErrorMessage = "Password doesnot match.")]
        public string ConfirmPassword { get; set; }
        public string OldPassword { get; set; }
        public Guid UserId { get; set; }

    }

    public class SignInResponseViewModel
    {
        public string DeviceKey { get; set; }
        public DateTime DeviceKeyExpiryDate { get; set; }
        public string RefreshToken { get; set; }
        public string OtpKey { get; set; }
        public string OldDeviceIdentifier { get; set; }
        public bool IsNotificationActive { get; set; }
        public string Name { get; set; }
    }

    public class AccountMapper : Profile
    {
        public AccountMapper()
        {
            CreateMap<SignInViewModel, UserDeviceKey>();
        }
    }
}
