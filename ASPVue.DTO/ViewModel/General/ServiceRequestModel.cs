﻿namespace ASPVueProject.DTO.ViewModel
{
	public class ServiceRequestModel<T>
	{
		public T Model { get; set; }
		public string IpAddress { get; set; }
		public LoginInfo LoginInfo { get; set; }
	}
	public class CurrentUserInfo
	{
		public string IpAddress { get; set; }
		public LoginInfo LoginInfo { get; set; }
	}
}
