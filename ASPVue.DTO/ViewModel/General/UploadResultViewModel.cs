﻿namespace ASPVueProject.DTO.ViewModel
{
	public class UploadResultViewModel
	{
		public string Name { get; set; }
		public string Extension { get; set; }
		public string NameWithExtension { get; set; }
		public string Path { get; set; }
		public bool Success { get; set; }
		public string Message { get; set; }
		public string PhysicalPath { get; set; }
	}
}
