﻿using AutoMapper;
using ASPVueProject.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPVueProject.DTO.ViewModel
{
   public class GeneralSettingViewModel
    {
        public Guid Id { get; set; }
        public bool EnableSMS { get; set; }
        public bool EnablePremiumDelivery { get; set; }
        public string Remarks { get; set; }
        public decimal B2B { get; set; }
        public decimal B2C { get; set; }
        public decimal OB2C { get; set; }
        public decimal BonuslinkRm { get; set; }
        public bool EnableVirtualInventory { get; set; }
        public string VirtualInventoryStartTime { get; set; }
        public string VirtualInventoryEndTime { get; set; }
        public bool EnabledWelcomePromo { get; set; }
        public bool EnableCurrentTxnPromo { get; set; }
        public bool EnableNextTxnPromo { get; set; }
        public bool EnableBonusLink { get; set; }
        public bool EnableOzoPayPayment { get; set; }
        public bool EnableMerchantradeMoneyPayment { get; set; }
            public bool IsActive { get; set; }
    }
    public class GeneralSettingMapper : Profile
    {
        public GeneralSettingMapper()
        {
            CreateMap<GeneralSetting, GeneralSettingViewModel>()
                 .ForMember(dest => dest.VirtualInventoryStartTime, opts => opts.ResolveUsing(src =>
                 {
                     return DateTime.Today.Add(src.VirtualInventoryStartTime).ToString("hh:mm tt");
                 }))
                .ForMember(dest => dest.VirtualInventoryEndTime, opts => opts.ResolveUsing(src =>
                {
                    return DateTime.Today.Add(src.VirtualInventoryEndTime).ToString("hh:mm tt");
                }));


            CreateMap<GeneralSettingViewModel, GeneralSetting>()
                .ForMember(dest => dest.VirtualInventoryStartTime, opts => opts.ResolveUsing(src =>
                {
                    DateTime dateTime = DateTime.ParseExact(src.VirtualInventoryStartTime,
                                          "h:m tt", CultureInfo.InvariantCulture);
                    TimeSpan span = dateTime.TimeOfDay;
                    return span;
                }))
            .ForMember(dest => dest.VirtualInventoryEndTime, opts => opts.ResolveUsing(src =>
            {
                DateTime dateTime = DateTime.ParseExact(src.VirtualInventoryEndTime,
                                      "h:m tt", CultureInfo.InvariantCulture);
                TimeSpan span = dateTime.TimeOfDay;
                return span;
            }));
            ;
        }
    }
}
