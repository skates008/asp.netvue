﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using ASPVueProject.Domain.Enum;
using ASPVueProject.DTO.Configuration;
using ASPVueProject.Domain.Entity;

namespace ASPVueProject.DTO.ViewModel
{
    public class PurposeOfOrderViewModel
    {
        [Required]
        public Guid Id { get; set; }
        public string Name { get; set; }

        public PurposeOfOrderType Type { get; set; }

        public string Description { get; set; }
        public string SetupTable { get; set; }
        public bool IsEnable { get; set; }
    }
    public class PurposeOfOrderListViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Type { get; set; }
        public string Description { get; set; }
        public string SetupTable { get; set; }
        public string IsEnable { get; set; }
    }
    public class PurposeOfOrderMapper : Profile
    {
        public PurposeOfOrderMapper()
        {
            CreateMap<PurposeOfOrder, PurposeOfOrderViewModel>();
            CreateMap<PurposeOfOrderViewModel, PurposeOfOrder>();
            CreateMap<PurposeOfOrder, PurposeOfOrderListViewModel>()
                .ForMember(a => a.IsEnable, opts => opts.ResolveUsing(d => d.IsEnable ? "Yes" : "No"));
        }
    }
}

