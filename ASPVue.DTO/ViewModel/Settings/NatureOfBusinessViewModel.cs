﻿using System;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using ASPVueProject.Domain.Enum;
using ASPVueProject.Domain.Entity;

namespace ASPVueProject.DTO.ViewModel
{
    public class NatureOfBusinessViewModel
    {
        [Required]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SetupTable { get; set; }
        public bool IsEnable { get; set; }
    }
    public class NatureOfBusinessListViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SetupTable { get; set; }
        public string IsEnable { get; set; }
    }
    public class NatureOfBusinessViewModelMapper : Profile
    {
        public NatureOfBusinessViewModelMapper()
        {
            CreateMap<NatureOfBusiness, NatureOfBusinessViewModel>();
            CreateMap<NatureOfBusinessViewModel, NatureOfBusiness>();
            CreateMap<NatureOfBusiness, NatureOfBusinessListViewModel>()
                .ForMember(a => a.IsEnable, opts => opts.ResolveUsing(d => d.IsEnable ? "Yes" : "No"));
        }
    }
}

