﻿using System;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using ASPVueProject.Domain.Enum;
using ASPVueProject.Domain.Entity;

namespace ASPVueProject.DTO.ViewModel
{
    public class SecretQuestionViewModel
    {
        [Required]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SetupTable { get; set; }
        public bool IsEnable { get; set; }
    }
    public class SecretQuestionListViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SetupTable { get; set; }
        public string IsEnable { get; set; }
    }
    public class SecretQuestionViewModelMapper : Profile
    {
        public SecretQuestionViewModelMapper()
        {
            CreateMap<SecretQuestion, SecretQuestionViewModel>();
            CreateMap<SecretQuestionViewModel, SecretQuestion>();
            CreateMap<SecretQuestion, SecretQuestionListViewModel>()
                .ForMember(a => a.IsEnable, opts => opts.ResolveUsing(d => d.IsEnable ? "Yes" : "No"));
        }
    }
}

