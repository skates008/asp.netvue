﻿using System.Net.Mail;
using System.Threading.Tasks;
using ASPVueProject.Mailing.Models;

namespace ASPVueProject.Mailing.Interfaces
{
    public interface IEmailer
    {
        string RenderTemplate(EmailTemplate template, object model = null);
        Task<bool> SendEmailAsync(MailMessage message);
    }
}
