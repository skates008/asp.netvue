﻿using ASPVueProject.Mailing.Models;

namespace ASPVueProject.Mailing.Interfaces
{
	public interface IEmailComposer
    {
        void SendEmail(EmailVariables emailRequest);
        void SendEmail(EmailVariables emailRequest, EmailTemplate emailTemplate);
        void SendEmail<T>(EmailRequest<T> emailRequest, EmailTemplate emailTemplate) where T : class;
        string GetMailContent<T>(T model, EmailTemplate emailTemplate);
    }
}
