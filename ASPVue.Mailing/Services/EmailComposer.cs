﻿using System.Linq;
using System.Net.Mail;
using ASPVueProject.Mailing.Interfaces;
using ASPVueProject.Mailing.Models;

namespace ASPVueProject.Mailing.Services
{
	public class EmailComposer : IEmailComposer
    {
        private readonly IEmailer _emailer;

        public EmailComposer(IEmailer emailer)
        {
            _emailer = emailer;
        }
        public async void SendEmail(EmailVariables emailRequest)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = emailRequest.Body;
                mailMessage.Subject = emailRequest.Subject;
                if (emailRequest.From != null)
                {
                    mailMessage.From = emailRequest.From;
                }
                emailRequest.To.ToList().ForEach(mailMessage.To.Add);
                emailRequest.CC.ToList().ForEach(mailMessage.CC.Add);
                await _emailer.SendEmailAsync(mailMessage);
            }
        }
        public async void SendEmail(EmailVariables emailRequest, EmailTemplate emailTemplate)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = _emailer.RenderTemplate(emailTemplate);
                mailMessage.Subject = emailRequest.Subject;
                if (emailRequest.From != null)
                {
                    mailMessage.From = emailRequest.From;
                }
                emailRequest.To.ToList().ForEach(mailMessage.To.Add);
                emailRequest.CC.ToList().ForEach(mailMessage.CC.Add);
                await _emailer.SendEmailAsync(mailMessage);
            }
        }
        public async void SendEmail<T>(EmailRequest<T> emailRequest, EmailTemplate emailTemplate) where T : class
        {
            using (var mailMessage = new MailMessage())
            {
                mailMessage.Body = _emailer.RenderTemplate(emailTemplate, emailRequest);
                mailMessage.IsBodyHtml = true;
                mailMessage.Subject = emailRequest.Subject;
                if (emailRequest.From != null)
                {
                    mailMessage.From = emailRequest.From;
                }
                emailRequest.To.ToList().ForEach(mailMessage.To.Add);
                emailRequest.CC.ToList().ForEach(mailMessage.CC.Add);
                await _emailer.SendEmailAsync(mailMessage);
            }
        }
        public string GetMailContent<T>(T model, EmailTemplate emailTemplate)
        {
            return _emailer.RenderTemplate(emailTemplate, model);
        }
    }
}
