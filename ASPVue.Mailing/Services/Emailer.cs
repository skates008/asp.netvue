﻿using System;
using System.IO;
using System.Net.Mail;
using System.Threading.Tasks;
using ASPVueProject.Mailing.Interfaces;
using ASPVueProject.Mailing.Models;
using RazorEngine;

namespace ASPVueProject.Mailing.Services
{
    public class Emailer : IEmailer
    {
        public string RenderTemplate(EmailTemplate emailTemplate, object model)
        {
            var emailContent = Render(emailTemplate.ToString(), model);
            var mailWrapperVariables = new MailMasterVariables()
            {
                Html = emailContent
            };
            return Render("MailMaster", mailWrapperVariables);
        }
        private string Render(string emailTemplate, object model = null)
        {
            var templatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                $"bin\\Templates\\{emailTemplate}.cshtml");

            var templateContent = File.ReadAllText(templatePath);

            var template = Razor.Parse(templateContent, model, emailTemplate);
            return template;
        }
        public Task<bool> SendEmailAsync(MailMessage message)
        {
            try
            {
                SmtpClient client = new SmtpClient();
                client.Send(message);
                return Task.FromResult(true);
            }
            catch
            {
                //throw;
                return Task.FromResult(false);
            }

        }
    }
}
