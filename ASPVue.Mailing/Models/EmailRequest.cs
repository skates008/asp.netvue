﻿using System.Collections.Generic;
using System.Net.Mail;

namespace ASPVueProject.Mailing.Models
{
    public class EmailRequest<T> : EmailVariables where T : class
    {
        public EmailRequest()
        {
            
        }
        public EmailRequest(T model)
        {
            Model = model;
        }
      
        public T Model { get; set; }

    }

   
    public class EmailVariables
    {
        private IList<MailAddress> _to;
        public IList<MailAddress> To => _to ?? (_to = new List<MailAddress>());

        private IList<MailAddress> _cc;
        public IList<MailAddress> CC => _cc ?? (_cc = new List<MailAddress>());

        public MailAddress From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        //public EmailTemplate Template { get; set; }
    }
    public class EmailRequestWithUrl<T> : EmailRequest<T> where T : class
    {
        public string Url { get; set; }
    }
    public class MailMasterVariables
    {
        public string ApplicationName => "ASPVueProject";
        public string MainWrapperStyle => "margin-top:20px;padding:0px;background:#f5f5f5;border:1px solid #d2d2d2;border-radius:5px;font-family:Georgia,'Times New Roman',Times,serif;";
        public string LogoWrapperStyle => "padding: 10px;background: #367fa9;border-radius: 5px 5px 0px 0px;text-align: center;color: white;font-size: 24px;font-weight: bold;";
        public string BodyWrapperStyle => "padding: 10px 20px;font-size: 14px;margin-top: 10px;word-break: break-all;";
        public string FooterTextStyle => "color: #666;padding: 10px 20px;font-size: 10px;";
        public string Html { get; set; }
    }
}
