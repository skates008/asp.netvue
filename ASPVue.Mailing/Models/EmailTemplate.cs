﻿namespace ASPVueProject.Mailing.Models
{
    public enum EmailTemplate
    {
        ResetPassword,
        SetUpPassword,
        PasswordChangeConfirmation,
        EmailConfirmation
    }
}
