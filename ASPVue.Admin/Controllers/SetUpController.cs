﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using ASPVueProject.Common;
using ASPVueProject.DTO.ViewModel;

namespace ASPVueProject.Admin.Controllers
{
  [RoutePrefix("api/setup")]
  public class SetUpController : ApiController
  {

    #region Role
    [HttpPost]
    [Route("role/list")]
    public async Task<IHttpActionResult> ListRole(KendoDataRequest kendoDataRequest)
    {
      return Ok(await WebApiService.Instance.PostAsync<DataSourceResult<RoleListViewModel>>("setup/role/list", kendoDataRequest));
    }
    [HttpPost]
    [Route("role/add")]
    public async Task<IHttpActionResult> AddRole(RoleViewModel model)
    {
      await WebApiService.Instance.PostAsync("setup/role/add", model);
      return Ok();
    }
    [HttpPost]
    [Route("role/update")]
    public async Task<IHttpActionResult> UpdateRole(RoleViewModel model)
    {
      await WebApiService.Instance.PostAsync("setup/role/update", model);
      return Ok();
    }
    [HttpGet]
    [Route("role/get/{id}")]
    public async Task<IHttpActionResult> GetRole(Guid id)
    {
      return Ok(await WebApiService.Instance.GetAsync<RoleViewModel>($"setup/role/get/{id}"));
    }

    [HttpGet]
    [Route("role/delete/{id}")]
    public async Task<IHttpActionResult> DeleteRole(Guid id)
    {
      await WebApiService.Instance.GetAsync($"setup/role/delete/{id}");
      return Ok();
    }
    #endregion

    #region User
    [HttpPost]
    [Route("user/list")]
    public async Task<IHttpActionResult> ListUser(KendoDataRequest kendoDataRequest)
    {
      return Ok(await WebApiService.Instance.PostAsync<DataSourceResult<UserListViewModel>>("setup/user/list", kendoDataRequest));
    }

    [HttpPost]
    [Route("user/add")]
    public async Task<IHttpActionResult> AddUser(UserViewModel model)
    {
      await WebApiService.Instance.PostAsync("setup/user/add", model);
      return Ok();
    }
    [HttpPost]
    [Route("user/update")]
    public async Task<IHttpActionResult> UpdateUser(UserViewModel model)
    {
      await WebApiService.Instance.PostAsync("setup/user/update", model);
      return Ok();
    }
    [HttpGet]
    [Route("user/get/{id}")]
    public async Task<IHttpActionResult> GetUser(Guid id)
    {
      return Ok(await WebApiService.Instance.GetAsync<UserViewModel>($"setup/user/get/{id}"));
    }


    [HttpGet]
    [Route("user/reset/password/{id}")]
    public async Task<IHttpActionResult> ResetPassword(Guid id)
    {
      await WebApiService.Instance.GetAsync($"setup/user/reset/password/{id}");
      return Ok();
    }

    [HttpGet]
    [Route("user/delete/{id}")]
    public async Task<IHttpActionResult> DeleteUser(Guid id)
    {
      await WebApiService.Instance.GetAsync($"setup/user/delete/{id}");
      return Ok();
    }

    #endregion


  }
}
