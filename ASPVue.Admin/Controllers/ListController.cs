using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using ASPVueProject.Common;
using ASPVueProject.DTO.ViewModel;
using System;

namespace ASPVueProject.Admin.Controllers
{
  [RoutePrefix("api/list")]
  public class ListController : ApiController
  {
    [HttpGet]
    [Route("role")]
    public async Task<IHttpActionResult> ListRole()
    {
      return Ok(await WebApiService.Instance.GetAsync<IList<ListViewModel>>("list/role"));
    }

    [HttpGet]
    [Route("userstatus")]
    public async Task<IHttpActionResult> ListUserStatus()
    {
      return Ok(await WebApiService.Instance.GetAsync<IList<ListViewModel>>("list/userstatus"));
    }
    [HttpGet]
    [Route("country")]
    public async Task<IHttpActionResult> ListCountry()
    {
      return Ok(await WebApiService.Instance.GetAsync<IList<ListViewModel>>("list/countrylist"));
    }
    [HttpGet]
    [Route("state")]
    public async Task<IHttpActionResult> ListState()
    {
      return Ok(await WebApiService.Instance.GetAsync<IList<ListViewModel>>("list/statelist"));
    }

    [HttpGet]
    [Route("state/get/{id}")]
    public async Task<IHttpActionResult> ListStateByCountryId(Guid id)
    {
      var rec = await WebApiService.Instance.GetAsync<IList<ListViewModel>>($"list/state/get/{id}");
      return Ok(rec);
    }


    [HttpGet]
    [Route("city/get/{id}")]
    public async Task<IHttpActionResult> ListCity(Guid id)
    {
      var rec = await WebApiService.Instance.GetAsync<IList<ListViewModel>>($"list/citylist/get/{id}");
      return Ok(rec);
    }
    [HttpGet]
    [Route("postcode/get/{id}")]
    public async Task<IHttpActionResult> ListPostCode(Guid id)
    {
      var rec = await WebApiService.Instance.GetAsync<IList<ListViewModel>>($"list/postcode/get/{id}");
      return Ok(rec);
    }

    [HttpGet]
    [Route("purposeType")]
    public async Task<IHttpActionResult> ListPurposeOfOrderType()
    {
      return Ok(await WebApiService.Instance.GetAsync<IList<ListViewModel>>("list/purposeType"));
    }

    [HttpGet]
    [Route("documentType")]
    public async Task<IHttpActionResult> ListDocumentType()
    {
      return Ok(await WebApiService.Instance.GetAsync<IList<ListViewModel>>("list/documentType"));
    }
 
    [HttpGet]
    [Route("collectionmode")]
    public async Task<IHttpActionResult> ListCollectionMode()
    {
      return Ok(await WebApiService.Instance.GetAsync<IList<ListViewModel>>("list/collectionmode"));
    }

    [HttpGet]
    [Route("postcodegroup")]
    public async Task<IHttpActionResult> ListPostCodeGroup()
    {
      return Ok(await WebApiService.Instance.GetAsync<IList<ListViewModel>>("list/postcodegroup"));
    }

    
  }

}
