using ASPVueProject.Common;
using ASPVueProject.Domain.Enum;
using ASPVueProject.DTO.ViewModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace ASPVueProject.Admin.Controllers
{
    [RoutePrefix("api/settings")]
    public class SettingsController : ApiController
    {
        #region Purpose Of Order
        [HttpPost]
        [Route("purposeoforder/list")]
        public async Task<IHttpActionResult> ListPurposeOfOrder(KendoDataRequest kendoDataRequest)
        {
            return Ok(await WebApiService.Instance.PostAsync<DataSourceResult<PurposeOfOrderListViewModel>>("settings/purposeoforder/list", kendoDataRequest));
        }
        [HttpPost]
        [Route("purposeoforder/add")]
        public async Task<IHttpActionResult> AddPurposeOfOrder(PurposeOfOrderViewModel model)
        {
            await WebApiService.Instance.PostAsync("settings/purposeoforder/add", model);
            return Ok();
        }
        [HttpPost]
        [Route("purposeoforder/update")]
        public async Task<IHttpActionResult> UpdatePurposeOfOrder(PurposeOfOrderViewModel model)
        {
            await WebApiService.Instance.PostAsync("settings/purposeoforder/update", model);
            return Ok();
        }
        [HttpGet]
        [Route("purposeoforder/get/{id}")]
        public async Task<IHttpActionResult> GetPurposeOfOrder(Guid id)
        {
            return Ok(await WebApiService.Instance.GetAsync<PurposeOfOrderViewModel>($"settings/purposeoforder/get/{id}"));
        }

        [HttpGet]
        [Route("purposeoforder/delete/{id}")]
        public async Task<IHttpActionResult> DeletePurposeOfOrder(Guid id)
        {
            await WebApiService.Instance.GetAsync($"settings/purposeoforder/delete/{id}");
            return Ok();
        }
        #endregion

        #region Nature Of Business
        [HttpPost]
        [Route("natureofbusiness/list")]
        public async Task<IHttpActionResult> ListNatureOfBusiness(KendoDataRequest kendoDataRequest)
        {
            return Ok(await WebApiService.Instance.PostAsync<DataSourceResult<NatureOfBusinessListViewModel>>("settings/natureofbusiness/list", kendoDataRequest));
        }
        [HttpPost]
        [Route("natureofbusiness/add")]
        public async Task<IHttpActionResult> AddNatureOfBusiness(NatureOfBusinessViewModel model)
        {
            await WebApiService.Instance.PostAsync("settings/natureofbusiness/add", model);
            return Ok();
        }
        [HttpPost]
        [Route("natureofbusiness/update")]
        public async Task<IHttpActionResult> UpdateNatureOfBusiness(NatureOfBusinessViewModel model)
        {
            await WebApiService.Instance.PostAsync("settings/natureofbusiness/update", model);
            return Ok();
        }
        [HttpGet]
        [Route("natureofbusiness/get/{id}")]
        public async Task<IHttpActionResult> GetNatureOfBusiness(Guid id)
        {
            return Ok(await WebApiService.Instance.GetAsync<NatureOfBusinessViewModel>($"settings/natureofbusiness/get/{id}"));
        }

        [HttpGet]
        [Route("natureofbusiness/delete/{id}")]
        public async Task<IHttpActionResult> DeleteNatureOfBusiness(Guid id)
        {
            await WebApiService.Instance.GetAsync($"settings/natureofbusiness/delete/{id}");
            return Ok();
        }
        #endregion

        #region Secret Question
        [HttpPost]
        [Route("secretquestion/list")]
        public async Task<IHttpActionResult> ListSecretQuestion(KendoDataRequest kendoDataRequest)
        {
            return Ok(await WebApiService.Instance.PostAsync<DataSourceResult<SecretQuestionListViewModel>>("settings/secretquestion/list", kendoDataRequest));
        }
        [HttpPost]
        [Route("secretquestion/add")]
        public async Task<IHttpActionResult> AddSecretQuestion(SecretQuestionViewModel model)
        {
            await WebApiService.Instance.PostAsync("settings/secretquestion/add", model);
            return Ok();
        }
        [HttpPost]
        [Route("secretquestion/update")]
        public async Task<IHttpActionResult> UpdateSecretQuestion(SecretQuestionViewModel model)
        {
            await WebApiService.Instance.PostAsync("settings/secretquestion/update", model);
            return Ok();
        }
        [HttpGet]
        [Route("secretquestion/get/{id}")]
        public async Task<IHttpActionResult> GetSecretQuestion(Guid id)
        {
            return Ok(await WebApiService.Instance.GetAsync<SecretQuestionViewModel>($"settings/secretquestion/get/{id}"));
        }

        [HttpGet]
        [Route("secretquestion/delete/{id}")]
        public async Task<IHttpActionResult> DeleteSecretQuestion(Guid id)
        {
            await WebApiService.Instance.GetAsync($"settings/secretquestion/delete/{id}");
            return Ok();
        }
        #endregion

       

    }

}
