using ASPVueProject.Common;
using ASPVueProject.DTO.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ASPVueProject.Admin.Controllers
{
  [RoutePrefix("api/address")]
  public class AddressSetupController : ApiController
  {

    #region Country Setup

    [HttpPost]
    [Route("country/list")]
    public async Task<IHttpActionResult> ListCountry(KendoDataRequest kendoDataRequest)
    {
      var rec = await WebApiService.Instance.PostAsync<DataSourceResult<CountryListViewModel>>("address/country/list", kendoDataRequest);
      return Ok(rec);
    }
    [Route("country/add")]
    public async Task<IHttpActionResult> CreateCountry(CountryViewModel countryViewModel)
    {
      await WebApiService.Instance.PostAsync("address/country/add", countryViewModel);
      return Ok();
    }

    [HttpPost]
    [Route("country/update")]
    public async Task<IHttpActionResult> UpdateCountry(CountryViewModel model)
    {
      await WebApiService.Instance.PostAsync("address/country/update", model);
      return Ok();
    }
    [HttpGet]
    [Route("country/get/{id}")]
    public async Task<IHttpActionResult> GetCountry(Guid id)
    {
      return Ok(await WebApiService.Instance.GetAsync<CountryViewModel>($"address/country/get/{id}"));
    }

    [HttpGet]
    [Route("country/delete/{id}")]
    public async Task<IHttpActionResult> DeleteCountry(Guid id)
    {
      await WebApiService.Instance.GetAsync($"address/country/delete/{id}");
      return Ok();
    }

    #endregion

    #region State Province Setup

    [HttpPost]
    [Route("state/list")]
    public async Task<IHttpActionResult> Liststate(KendoDataRequest kendoDataRequest)
    {
      var rec = await WebApiService.Instance.PostAsync<DataSourceResult<StateProvinceListViewModel>>("address/state/list", kendoDataRequest);
      return Ok(rec);
    }
    [Route("state/add")]
    public async Task<IHttpActionResult> CreateState(StateProvinceViewModel stateViewModel)
    {
      await WebApiService.Instance.PostAsync("address/state/add", stateViewModel);
      return Ok();
    }

    [HttpPost]
    [Route("state/update")]
    public async Task<IHttpActionResult> UpdateState(StateProvinceViewModel model)
    {
      await WebApiService.Instance.PostAsync("address/state/update", model);
      return Ok();
    }
    [HttpGet]
    [Route("state/get/{id}")]
    public async Task<IHttpActionResult> GetState(Guid id)
    {
      return Ok(await WebApiService.Instance.GetAsync<StateProvinceViewModel>($"address/state/get/{id}"));
    }

    [HttpGet]
    [Route("state/delete/{id}")]
    public async Task<IHttpActionResult> DeleteState(Guid id)
    {
      await WebApiService.Instance.GetAsync($"address/state/delete/{id}");
      return Ok();
    }

    #endregion

    #region City Setup

    [HttpPost]
    [Route("city/list")]
    public async Task<IHttpActionResult> ListCity(KendoDataRequest kendoDataRequest)
    {
      var rec = await WebApiService.Instance.PostAsync<DataSourceResult<CityListViewModel>>("address/city/list", kendoDataRequest);
      return Ok(rec);
    }
    [Route("city/add")]
    public async Task<IHttpActionResult> AddCity(CityViewModel stateViewModel)
    {
      await WebApiService.Instance.PostAsync("address/city/add", stateViewModel);
      return Ok();
    }

    [HttpPost]
    [Route("city/update")]
    public async Task<IHttpActionResult> UpdateCity(CityViewModel model)
    {
      await WebApiService.Instance.PostAsync("address/city/update", model);
      return Ok();
    }
    [HttpGet]
    [Route("city/get/{id}")]
    public async Task<IHttpActionResult> GetCity(Guid id)
    {
      return Ok(await WebApiService.Instance.GetAsync<CityViewModel>($"address/city/get/{id}"));
    }

    [HttpGet]
    [Route("city/delete/{id}")]
    public async Task<IHttpActionResult> DeleteCity(Guid id)
    {
      await WebApiService.Instance.GetAsync($"address/city/delete/{id}");
      return Ok();
    }

    #endregion

    #region PostCode Setup

    [HttpPost]
    [Route("postcode/list")]
    public async Task<IHttpActionResult> ListPostCode(KendoDataRequest kendoDataRequest)
    {
      var rec = await WebApiService.Instance.PostAsync<DataSourceResult<PostCodeListViewModel>>("address/postcode/list", kendoDataRequest);
      return Ok(rec);
    }
    [Route("postcode/add")]
    public async Task<IHttpActionResult> AddPostCode(PostCodeViewModel stateViewModel)
    {
      await WebApiService.Instance.PostAsync("address/postcode/add", stateViewModel);
      return Ok();
    }

    [HttpPost]
    [Route("postcode/update")]
    public async Task<IHttpActionResult> UpdatePostCode(PostCodeViewModel model)
    {
      await WebApiService.Instance.PostAsync("address/postcode/update", model);
      return Ok();
    }
    [HttpGet]
    [Route("postcode/get/{id}")]
    public async Task<IHttpActionResult> GetPostCode(Guid id)
    {
      return Ok(await WebApiService.Instance.GetAsync<PostCodeViewModel>($"address/postcode/get/{id}"));
    }

    [HttpGet]
    [Route("postcode/delete/{id}")]
    public async Task<IHttpActionResult> DeletePostCode(Guid id)
    {
      await WebApiService.Instance.GetAsync($"address/postcode/delete/{id}");
      return Ok();
    }

    #endregion

    #region Suburb Setup

    [HttpPost]
    [Route("suburb/list")]
    public async Task<IHttpActionResult> ListSuburb(KendoDataRequest kendoDataRequest)
    {
      var rec = await WebApiService.Instance.PostAsync<DataSourceResult<SuburbListViewModel>>("address/suburb/list", kendoDataRequest);
      return Ok(rec);
    }
    [Route("suburb/add")]
    public async Task<IHttpActionResult> AddSuburb(SuburbViewModel stateViewModel)
    {
      await WebApiService.Instance.PostAsync("address/suburb/add", stateViewModel);
      return Ok();
    }

    [HttpPost]
    [Route("suburb/update")]
    public async Task<IHttpActionResult> UpdateSuburb(SuburbViewModel model)
    {
      await WebApiService.Instance.PostAsync("address/suburb/update", model);
      return Ok();
    }
    [HttpGet]
    [Route("suburb/get/{id}")]
    public async Task<IHttpActionResult> GetSuburb(Guid id)
    {
      return Ok(await WebApiService.Instance.GetAsync<SuburbViewModel>($"address/suburb/get/{id}"));
    }

    [HttpGet]
    [Route("suburb/delete/{id}")]
    public async Task<IHttpActionResult> DeleteSuburb(Guid id)
    {
      await WebApiService.Instance.GetAsync($"address/suburb/delete/{id}");
      return Ok();
    }

    #endregion

    #region Street Setup

    [HttpPost]
    [Route("street/list")]
    public async Task<IHttpActionResult> ListStreet(KendoDataRequest kendoDataRequest)
    {
      var rec = await WebApiService.Instance.PostAsync<DataSourceResult<SuburbListViewModel>>("address/suburb/list", kendoDataRequest);
      return Ok(rec);
    }
    [Route("street/add")]
    public async Task<IHttpActionResult> AddStreet(SuburbViewModel stateViewModel)
    {
      await WebApiService.Instance.PostAsync("address/suburb/add", stateViewModel);
      return Ok();
    }

    [HttpPost]
    [Route("street/update")]
    public async Task<IHttpActionResult> UpdateStreet(SuburbViewModel model)
    {
      await WebApiService.Instance.PostAsync("address/suburb/update", model);
      return Ok();
    }
    [HttpGet]
    [Route("street/get/{id}")]
    public async Task<IHttpActionResult> GetStreet(Guid id)
    {
      return Ok(await WebApiService.Instance.GetAsync<SuburbViewModel>($"address/suburb/get/{id}"));
    }

    [HttpGet]
    [Route("street/delete/{id}")]
    public async Task<IHttpActionResult> DeleteStreet(Guid id)
    {
      await WebApiService.Instance.GetAsync($"address/suburb/delete/{id}");
      return Ok();
    }

    #endregion
  }
}
