using System.Threading.Tasks;
using System.Web.Http;
using ASPVueProject.Common;
using ASPVueProject.DTO.ViewModel;

namespace ASPVueProject.Admin.Controllers
{
  [RoutePrefix("api/account")]
  public class AccountController : ApiController
  {
    [HttpPost]
    [Route("signin")]
    [AllowAnonymous]
    public async Task<IHttpActionResult> SignIn(SignInViewModel model)
    {
      return Ok(await WebApiService.Instance.AuthenticateAsync<TokenViewModel>(model.Email, model.Password, true));
    }
    [HttpPost]
    [Route("signout")]
    [AllowAnonymous]
    public async Task<IHttpActionResult> Signout()
    {
      await WebApiService.Instance.PostAsync("account/signout");
      return Ok();
    }
    [HttpGet]
    [Route("forgotpassword")]
    [AllowAnonymous]
    public async Task<IHttpActionResult> ForgotPassword(string email)
    {
      await WebApiService.Instance.GetAsync($"account/forgotpassword?email={email}");
      return Ok();
    }
    [HttpPost]
    [Route("resetpassword")]
    [AllowAnonymous]
    public async Task<IHttpActionResult> ResetPassword(ResetPasswordModel model)
    {
      await WebApiService.Instance.PostAsync("account/resetPassword", model);
      return Ok();
    }
    [HttpPost]
    [Route("changepassword")]
    [AllowAnonymous]
    public async Task<IHttpActionResult> ChangePassword(ChangePasswordViewModel model)
    {
      await WebApiService.Instance.PostAsync("account/changepassword", model);
      return Ok();
    }
    [HttpGet]
    [Route("confirmemail")]
    [AllowAnonymous]
    public async Task<IHttpActionResult> ConfirmEmail(string userId, string code)
    {
      await WebApiService.Instance.GetAsync($"account/confirmemail?userId={userId}&code={code}");
      return Ok();
    }
  }
}
