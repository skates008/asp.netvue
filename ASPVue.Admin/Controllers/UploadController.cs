using ASPVueProject.Common;
using System.Web;
using System.Web.Http;

namespace ASPVueProject.Admin.Controllers
{
    [RoutePrefix("api/upload")]
    [Authenticate]
    public class UploadController : ApiController
    {
        [HttpPost]
        [Route("document")]
        public IHttpActionResult DocumentUpload()
        {
            var uploadDatModel = HttpContext.Current.Request.Files[0].SaveFile();
            return Ok(uploadDatModel);
        }
    }
}
