﻿using Quartz;
using Quartz.Impl;

namespace ASPVueProject.Admin
{
    public class JobScheduler
    {
        public static void Start()
        {
            // construct a scheduler factory
            ISchedulerFactory schedFact = new StdSchedulerFactory();

            // get a scheduler
            var sched = schedFact.GetScheduler();
        }
    }
}