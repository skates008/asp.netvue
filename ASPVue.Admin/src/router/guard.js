
import CommonService from '../services/common'

export default {
  needGuard (to, from, next) {
    next(CommonService.isAuthenticated() ? true : {
      path: '/login',
      query: {
        redirect: to.fullPath
      }
    })
  }
}
