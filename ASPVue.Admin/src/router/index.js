import Vue from 'vue'
import Router from 'vue-router'
// Containers
import Full from '@/containers/Full'
// Views
import Dashboard from '@/views/dashboard/Dashboard'
import User from '@/views/setup/User'

const NatureOfBusiness = () => import ('../views/Settings/NatureOfBusiness.vue')
const SecretQuestion = () => import ('../views/Settings/SecretQuestion.vue')
const PurposeOfOrder = () => import ('../views/Settings/PurposeOfOrder.vue')
const CustomerList=()=>import('../views/UserManagement/CustomerList.vue')


const Role = () =>
  import ('../views/setup/Role.vue')
const Country = () => import ('../views/Configuration/Country.vue')
const StateProvince = () => import ('../views/Configuration/StateProvince.vue')
const City = () => import ('../views/Configuration/City.vue')
const PostCode = () => import ('../views/Configuration/PostCode.vue')
const Suburb = () => import ('../views/Configuration/Suburb.vue')
const Street = () => import('../views/Configuration/Street.vue')


// Views - Pages
import Page404 from '@/views/pages/Page404'
import Page500 from '@/views/pages/Page500'
import Login from '@/views/login/Login'

import RouterGuard from '../router/guard'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  linkActiveClass: "open active",
  scrollBehavior: () => ({ y: 0 }),
  routes: [{
    path: '/login',
    component: {
      render(c) {
        return c('router-view')
      }
    },
    children: [{
      path: '',
      name: 'Login',
      component: Login
    }]
  },
  {
    path: '/',
    redirect: '/dashboard',
    name: 'Dashboard',
    component: Full,
    beforeEnter: RouterGuard.needGuard,
    children: [
      {
        path: 'dashboard',
        component: Dashboard
      },
      {
        path: 'setup',
        redirect: '/setup/user',
        component: {
          render(c) {
            return c('router-view')
          }
        },
        name: 'Setup',
        children: [
          {
            path: 'user',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            children: [
              {
                path: '',
                name: 'User',
                component: User
              },
              {
                path: ':mode',
                name: 'User Add',
                component: User,
                props: true
              },
              {
                path: ':mode/:id',
                name: 'User Edit',
                component: User,
                props: true
              },
            ]
          },
          {
            path: 'role',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            children: [
              {
                path: '',
                name: 'Role',
                component: Role
              },
              {
                path: ':mode',
                name: 'Role Add',
                component: Role,
                props: true
              },
              {
                path: ':mode/:id',
                name: 'Role Edit',
                component: Role,
                props: true
              }
            ]
          }
        ]
      },
      
       
       
      {
        path: 'address',
        redirect: 'address/country',
        component: {
          render(c) {
            return c('router-view')
          }
        },
        name: 'Address',
        children: [
          {
            path: 'country',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            children: [
              {
                path: '',
                name: 'Country',
                component: Country
              },
              {
                path: ':mode',
                name: 'Country Add',
                component: Country,
                props: true
              },
              {
                path: ':mode/:id',
                name: 'Country Edit',
                component: Country,
                props: true
              },
            ]
          },
          {
            path: 'state',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            children: [
              {
                path: '',
                name: 'State',
                component: StateProvince
              },
              {
                path: ':mode',
                name: 'State Add',
                component: StateProvince,
                props: true
              },
              {
                path: ':mode/:id',
                name: 'State Edit',
                component: StateProvince,
                props: true
              }
            ]
          },
          {

            path: 'city',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            children: [
              {
                path: '',
                name: 'City',
                component: City
              },
              {
                path: ':mode',
                name: 'City Add',
                component: City,
                props: true
              },
              {
                path: ':mode/:id',
                name: 'City Edit',
                component: City,
                props: true
              }
            ]
          },
          {

            path: 'postcode',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            children: [
              {
                path: '',
                name: 'PostCode',
                component: PostCode
              },
              {
                path: ':mode',
                name: 'Post Code Add',
                component: PostCode,
                props: true
              },
              {
                path: ':mode/:id',
                name: 'Post Code Edit',
                component: PostCode,
                props: true
              }
            ]
          },
          {

            path: 'suburb',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            children: [
              {
                path: '',
                name: 'Suburb',
                component: Suburb
              },
              {
                path: ':mode',
                name: 'Suburb Add',
                component: Suburb,
                props: true
              },
              {
                path: ':mode/:id',
                name: 'Suburb Edit',
                component: Suburb,
                props: true
              }
            ]
          },
          
          {
            path: 'street',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            children: [
              {
                path: '',
                name: 'Street',
                component: Street
              },
              {
                path: ':mode',
                name: 'Street Add',
                component: Street,
                props: true
              },
              {
                path: ':mode/:id',
                name: 'Street Edit',
                component: Street,
                props: true
              }
            ]
          }
        ]
      },
       
      {
        path:'usermanagement',
        redirect:'/usermanagement/customerlist',
        component:{
          render(c){
            return c('router-view')
          }
        },
        name:'usermanagement',
        children:[
          {
            path:'customerlist',
            component:{
              render(c){
                return c('router-view')
              }
            },
            children:[
                {
                  path: '',
                  name: 'Customer List',
                  component: CustomerList
                },
                {
                  path: ':mode',
                  name: 'Customer List Add',
                  component: CustomerList,
                  props: true
                },
                {
                  path: ':mode/:id',
                  name: 'Customer Edit',
                  component: CustomerList,
                  props: true
                },  
            ]
          }
        ]
      },
      {
        path: 'Settings',
        redirect: '/settings/purposeoforder',
        component: {
          render(c) {
            return c('router-view')
          }
        },
        name: 'Settings',
        children: [
          {
            path: 'purposeoforder',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            children: [
              {
                path: '',
                name: 'Purpose Of Order',
                component: PurposeOfOrder
              },
              {
                path: ':mode',
                name: 'Purpose Of Order Add',
                component: PurposeOfOrder,
                props: true
              },
              {
                path: ':mode/:id',
                name: 'Purpose Of Order Edit',
                component: PurposeOfOrder,
                props: true
              },
            ]
          },
          {
            path: 'natureofbusiness',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            children: [
              {
                path: '',
                name: 'Nature Of Business',
                component: NatureOfBusiness
              },
              {
                path: ':mode',
                name: 'Nature Of Business Add',
                component: NatureOfBusiness,
                props: true
              },
              {
                path: ':mode/:id',
                name: 'Nature Of Business Edit',
                component: NatureOfBusiness,
                props: true
              }
            ]
          },
          {
            path: 'secretquestion',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            children: [
              {
                path: '',
                name: 'Secret Question',
                component: SecretQuestion
              },
              {
                path: ':mode',
                name: 'Secret Question Add',
                component: SecretQuestion,
                props: true
              },
              {
                path: ':mode/:id',
                name: 'Secret Question Edit',
                component: SecretQuestion,
                props: true
              }
            ]
          }
            
        ]
      }
    ]
  },
  {
    path: '/pages',
    redirect: '/pages/404',
    name: 'Pages',
    component: {
      render(c) {
        return c('router-view')
      }
    },
    children: [{
      path: '404',
      name: 'Page404',
      component: Page404
    },
    {
      path: '500',
      name: 'Page500',
      component: Page500
    }
    ]
  }
  ]
})
