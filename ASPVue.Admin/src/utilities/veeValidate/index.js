import Vue from 'vue'
import VeeValidate from 'vee-validate'
const dictionary = {
  en: {
    messages: {
      min: (field, params) => ` ${field} must be at least ${params} characters.`,
      max: (field, params) => ` ${field} may not be greater than ${params} characters. `,
      between: (field, params) => `${field} must be between ${params[0]} and ${params[1]}`,
      required: (field, params) => `${field} is required`,
      numeric: (field, params) => `${field} must be a positive number.`,
      decimals: (field, params) => `${field} must be a decimals number.`,
      digits: (field, params) => `length must be ${params[0]}`
    },
    attributes: {
      email: 'Email Address',
      username: 'Username',
      password: 'Password',
      confirmPassword: 'Confirm Password'
    }
  }
};
const config = {
  errorBagName: 'errors', // change if property conflicts
  fieldsBagName: 'fields',
  delay: 0,
  locale: 'en',
  dictionary: dictionary,
  strict: true,
  classes: true,
  classNames: {
    touched: 'touched', // the control has been blurred
    untouched: 'untouched', // the control hasn't been blurred
    valid: 'valid', // model is valid
    invalid: 'invalid', // model is invalid
    pristine: 'pristine', // control has not been interacted with
    dirty: 'dirty' // control has been interacted with
  },
  events: 'input|blur',
  inject: true,
  validity: false,
  aria: true
}
VeeValidate.Validator.updateDictionary(dictionary)

// VeeValidate.Validator.extend('customeValidation', (value) => {
//   return value === 'valid'
//});
Vue.use(VeeValidate, config)
