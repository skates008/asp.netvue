import { vueinstance } from '../main.js'
export default {
  getUseStatus() {
    return vueinstance.$http.get('list/userstatus')
  },
  getRoleList() {
    return vueinstance.$http.get('list/role')
  },
  getById(url) {
    return vueinstance.$http.get(url)
  },
  getSingleItem(url) {
    return vueinstance.$http.get(url)
  },
  getCountryList() {
    return vueinstance.$http.get('list/country')
  },
  getStateList() {
    return vueinstance.$http.get('list/state')
  },
  getPurposeType() {
    return vueinstance.$http.get('list/purposetype')
  },
  getDocumentType() {
    return vueinstance.$http.get('list/documentType')
  },
  getAgentList() {
    return vueinstance.$http.get('list/agent')
  },
  getBranchList() {
    return vueinstance.$http.get('list/branch')
  },

 getCollectionModeList() {
    return vueinstance.$http.get('list/collectionmode')
  }
}
