import Vue from 'vue'
import { vueinstance } from '../main.js'

export default {
    isAuthenticated: function() {
        return Vue.localStorage.get('token') !== null
    },
    logOut: function() {
        Vue.localStorage.remove('userinfo')
        Vue.localStorage.remove('userpages')
        Vue.localStorage.remove('token')
        vueinstance.$router.push('/login')
    },
    getAuthToken: function() {
        return Vue.localStorage.get('token')
    },
    getUserPages: function() {
        return this.jsonParse(Vue.localStorage.get('userpages'))
    },
    getUserInfo: function() {
        return this.jsonParse(Vue.localStorage.get('userinfo'))
    },
    getBaseUrl: function(url) {
      return 'http://localhost:53746/api/' + url
    },
    jsonStringify: function(obj) {
        return JSON.stringify(obj)
    },
    jsonParse: function(str) {
        return JSON.parse(str)
    }
}
