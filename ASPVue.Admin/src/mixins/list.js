export default {
  methods: {
    getPost: function () {
      return this.$http.get('list/branch')
    },
    getById: function (url) {
      return this.$http.get(url)
    },
    getUseStatus: function () {
      return this.$http.get('list/userstatus')
    },
    getRoleList: function () {
      return this.$http.get('list/role')
    }
  }
}
