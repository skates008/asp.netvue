using System.Web.Http;
using ASPVueProject.Common;
using System.Web.Http.ExceptionHandling;

namespace ASPVueProject.Admin
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      // Web API configuration and services
      config.EnableCors();
      // Web API routes
      config.MapHttpAttributeRoutes();

      config.Routes.MapHttpRoute(
          name: "DefaultApi",
          routeTemplate: "api/{controller}/{id}",
          defaults: new { id = RouteParameter.Optional }
      );

      config.Formatters.Remove(config.Formatters.XmlFormatter);
      //config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new  CamelCasePropertyNamesContractResolver();

      config.Services.Replace(typeof(IExceptionHandler), new CustomExceptionHandler());
    }
  }
}
