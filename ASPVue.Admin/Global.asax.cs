using System;
using System.Web;
using System.Web.Routing;
using System.Web.Http;
using ASPVueProject.Common;

namespace ASPVueProject.Admin
{
  public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.MessageHandlers.Add(new HttpsObligationHandler());
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            JobScheduler.Start();
        }
    }
}
