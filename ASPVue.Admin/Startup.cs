using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(ASPVueProject.Admin.Startup))]
namespace ASPVueProject.Admin
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
            HttpConfiguration httpConfig = new HttpConfiguration();
            app.UseWebApi(httpConfig);
        }
    }
}
