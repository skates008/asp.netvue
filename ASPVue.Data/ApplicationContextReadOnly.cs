﻿using System;
using System.Threading.Tasks;

namespace ASPVueProject.Data
{
	public class ApplicationContextReadOnly : ApplicationContext
	{
		public ApplicationContextReadOnly()
			: base("ApplicationContextReadOnly")
		{

		}

		public override Task<int> SaveChangesAsync()
		{
			throw new InvalidOperationException("This context is read-only.");
		}

		public override int SaveChanges()
		{
			throw new InvalidOperationException("This context is read-only.");
		}
	}
}
