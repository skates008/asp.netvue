﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ASPVueProject.Data.Infrastructure
{
	public interface IGenericRepository<TEntity, in TKey> where TEntity : class
	{
		IQueryable<TEntity> GetAll();
		void Add(TEntity entity);
		void Update(TEntity entity);
		void Delete(TEntity entity);
		void AddOrUpdate(Expression<Func<TEntity, object>> identifierExpression, TEntity entity);
		TEntity GetById(TKey id);
		TEntity FirstOrDefault();
		TEntity FirstOrDefault(Expression<Func<TEntity, bool>> expression);
		IQueryable<string> GetTagList(object value, List<string> exclude = null);
	}
}
