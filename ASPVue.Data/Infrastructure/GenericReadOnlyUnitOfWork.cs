﻿using System.Linq;

namespace ASPVueProject.Data.Infrastructure
{
	public class GenericReadOnlyUnitOfWork : IGenericReadOnlyUnitOfWork
	{
		private readonly ApplicationContextReadOnly _context = new ApplicationContextReadOnly();

		public GenericReadOnlyRepository<TEntity, TKey> GenericUnitOfWork<TEntity, TKey>() where TEntity : class
		{
			return new GenericReadOnlyRepository<TEntity, TKey>(_context);
		}
		public IQueryable<T> SqlQuery<T>(string sqlQuery, params object[] parameters)
		{
			return _context.Database.SqlQuery<T>(sqlQuery, parameters).AsQueryable();
		}
		public void NonQuery(string sqlQuery, params object[] parameters)
		{
			_context.Database.ExecuteSqlCommand(sqlQuery, parameters);
		}
	}
}
