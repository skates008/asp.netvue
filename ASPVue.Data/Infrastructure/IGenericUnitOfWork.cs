﻿using System.Collections.Generic;
using System.Linq;

namespace ASPVueProject.Data.Infrastructure
{
	public interface IGenericUnitOfWork
	{
		GenericRepository<TEntity, TKey> GetRepository<TEntity, TKey>() where TEntity : class;
		void SaveChanges();
		IQueryable<T> SqlQuery<T>(string sqlQuery, params object[] parameters);
		void NonQuery(string sqlQuery, params object[] parameters);
	}
}
