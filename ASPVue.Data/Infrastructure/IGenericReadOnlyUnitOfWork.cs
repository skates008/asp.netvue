﻿using System.Linq;

namespace ASPVueProject.Data.Infrastructure
{
	public interface IGenericReadOnlyUnitOfWork
	{
		GenericReadOnlyRepository<TEntity, TKey> GenericUnitOfWork<TEntity, TKey>() where TEntity : class;
		IQueryable<T> SqlQuery<T>(string sqlQuery, params object[] parameters);
		void NonQuery(string sqlQuery, params object[] parameters);
	}
}
