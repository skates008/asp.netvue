﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ASPVueProject.Data.Infrastructure
{
	public interface IGenericReadOnlyRepository<TEntity, in TKey> where TEntity : class
	{
		IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> expression = null);
		TEntity GetById(TKey id);
		TEntity FirstOrDefault();
		TEntity FirstOrDefault(Expression<Func<TEntity, bool>> expression);
		IQueryable<string> GetTagList(object value, List<string> exclude = null);
	}
}
