﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;

namespace ASPVueProject.Data.Infrastructure
{
	public class GenericRepository<TEntity, TKey> : IGenericRepository<TEntity, TKey> where TEntity : class
	{
		private readonly IDbSet<TEntity> _dbSet;
		private readonly ApplicationContext _dataContext;
		public ApplicationContext DataContext => this._dataContext;
		public GenericRepository(ApplicationContext dataContext)
		{
			_dataContext = dataContext;
			_dbSet = _dataContext.Set<TEntity>();
		}
		public IQueryable<TEntity> GetAll()
		{
			return _dbSet;
		}
		public void Add(TEntity entity)
		{
			if (entity == null)
				throw new ArgumentException("entity");
			_dbSet.Add(entity);
		}
		public void AddOrUpdate(TEntity entity)
		{
			if (entity == null)
				throw new ArgumentException("entity");
			_dbSet.AddOrUpdate(entity);
		}
		public void AddOrUpdate(Expression<Func<TEntity, object>> identifierExpression, TEntity entity)
		{
			if (entity == null)
				throw new ArgumentException("entity");
			_dbSet.AddOrUpdate(identifierExpression, entity);
		}
		public void Update(TEntity entity)
		{
			if (entity == null)
				throw new ArgumentException("entity");
			_dbSet.Attach(entity);
			_dataContext.Entry(entity).State = EntityState.Modified;
		}
		public void Delete(TEntity entity)
		{
			if (_dataContext.Entry(entity).State == EntityState.Detached)
				_dbSet.Attach(entity);
			_dbSet.Remove(entity);
		}
		public TEntity GetById(TKey id)
		{
			return _dbSet.Find(id);
		}
		public TEntity FirstOrDefault()
		{
			return _dbSet.FirstOrDefault();
		}
		public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> expression)
		{
			return _dbSet.FirstOrDefault(expression);
		}
		public ReturnType<TEntity> GetAllWithFilter(Expression<Func<TEntity, bool>> expression, Expression<Func<TEntity, object>> orderby, int skip = 0, int take = 0)
		{
			var returnValue = new ReturnType<TEntity>();
			if (skip >= 0 && take > 0)
			{
				var record = _dbSet.AsQueryable();
				returnValue.TotalRecord = record.Where(expression).Count();
				var Data = record.Where(expression).OrderByDescending(orderby).Skip(skip).Take(take).ToList();
				returnValue.Tentity = Data;
			}
			else
			{
				returnValue.Tentity = _dbSet.Where(expression).AsEnumerable<TEntity>();
				returnValue.TotalRecord = returnValue.Tentity.Count();
			}
			return returnValue;
		}
		public IQueryable<string> GetTagList(object value, List<string> exclude = null)
		{
			var excludequery = string.Empty;
			if (exclude != null)
			{
				var tables = new List<string>();
				exclude.ForEach(s =>
				{
					tables.Add("'" + s + "'");
				});
				excludequery = $"AND o1.name not in ({string.Join(",", tables)})";
			}

			var tableName = typeof(TEntity).Name;
			var tableattr = typeof(TEntity).GetCustomAttributes(true).SingleOrDefault(attr => attr.GetType().Name == "TableAttribute") as dynamic;
			if (tableattr != null)
				tableName = tableattr.Name;
			string sql = $@"select 
                STUFF((SELECT ' union all select '''+ ForeignTable + ''' as tablename from [' + ForeignTable + '] where ' + ForeignKey + '=''{value}'' having count(*) > 0'
                FROM (
			        SELECT
				    o1.name AS [ForeignTable],c1.name AS [ForeignKey]
			    FROM sys.objects o1
				INNER JOIN sys.foreign_keys fk ON o1.object_id = fk.parent_object_id
				INNER JOIN sys.foreign_key_columns fkc ON fk.object_id = fkc.constraint_object_id
				INNER JOIN sys.columns c1 ON fkc.parent_object_id = c1.object_id AND fkc.parent_column_id = c1.column_id
				INNER JOIN sys.objects o2 ON fk.referenced_object_id = o2.object_id
			    where o2.name = '{tableName}' {excludequery} ) a
                FOR XML PATH('')), 1, 10, '')";
			var generatedquery = _dataContext.Database.SqlQuery<string>(sql).SingleOrDefault();
			return generatedquery == null ? new List<string>().AsQueryable() : _dataContext.Database.SqlQuery<string>(generatedquery.Replace("&gt;", ">")).AsQueryable();
		}
	}
	public class ReturnType<TEntity> where TEntity : class
	{
		public IEnumerable<TEntity> Tentity { get; set; }
		public int TotalRecord { get; set; }
	}
}
