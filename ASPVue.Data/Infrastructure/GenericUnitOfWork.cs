﻿using System.Linq;
using System.Transactions;

namespace ASPVueProject.Data.Infrastructure
{
	public class GenericUnitOfWork : IGenericUnitOfWork
	{
		private readonly ApplicationContext _context = new ApplicationContext();

		public GenericRepository<TEntity, TKey> GetRepository<TEntity, TKey>() where TEntity : class
		{
			return new GenericRepository<TEntity, TKey>(_context);
		}
		public void SaveChanges()
		{
			_context.SaveChanges();
		}
		public IQueryable<T> SqlQuery<T>(string sqlQuery, params object[] parameters)
		{
			return _context.Database.SqlQuery<T>(sqlQuery, parameters).AsQueryable();
		}
		public void NonQuery(string sqlQuery, params object[] parameters)
		{
			_context.Database.ExecuteSqlCommand(sqlQuery, parameters);
		}
	}
}
