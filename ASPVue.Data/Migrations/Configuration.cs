﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using Microsoft.AspNet.Identity;
using ASPVueProject.Domain.Constant;
using ASPVueProject.Domain.Entity;
using ASPVueProject.Domain.Enum;

namespace ASPVueProject.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationContext>
    {
        public Configuration()
        {
            AutomaticMigrationDataLossAllowed = true;
            AutomaticMigrationsEnabled = true;
        }
        protected override void Seed(ApplicationContext context)
        {
            #region Page
            var pages = new List<Page>()
            {
                new Page() { Id=1, Path = "/dashboard",                     Title="Dashboard",          DisplayOrder=1, Icon="fa-tachometer", ShowInView = true },

                new Page() { Id=100, Path = "/setup",                       Title="Set Up",             DisplayOrder=2 ,Icon="fa-cog", ShowInView = true},
                new Page() { Id=101, Path = "/setup/role",                  Title="Role",               ParentPageId=100, DisplayOrder=1, Icon="fa-circle-o", ShowInView = true },
                new Page() { Id=102, Path = "/setup/user",                  Title="User",               ParentPageId=100, DisplayOrder=2, Icon="fa-circle-o", ShowInView = true },

                new Page() { Id=200, Path = "/usermanagement",              Title="User Management",    DisplayOrder=3, Icon="fa-tachometer", ShowInView = true },
                new Page() { Id=201, Path = "/usermanagement/customerlist", Title="Customer List",      ParentPageId=200, DisplayOrder=3, Icon="fa-tachometer", ShowInView = true },

                new Page() { Id=300, Path = "/address",                     Title="Configuration",      DisplayOrder=7 ,Icon="fa fa-cogs fa-fw", ShowInView = true},
                new Page() { Id=301, Path = "/address/country",             Title="Country",            ParentPageId=300, DisplayOrder=1, Icon="fa fa-flag", ShowInView = true },
                new Page() { Id=302, Path = "/address/state",               Title="State",              ParentPageId=300, DisplayOrder=2, Icon="fa fa-flag-o", ShowInView = true },
                new Page() { Id=303, Path = "/address/city",                Title="City",               ParentPageId=300, DisplayOrder=3, Icon="fa fa-building", ShowInView = true },
                new Page() { Id=304, Path = "/address/postcode",            Title="Post Code",          ParentPageId=300, DisplayOrder=4, Icon="fa fa-building", ShowInView = true },
                new Page() { Id=305, Path = "/address/suburb",              Title="Suburb",             ParentPageId=300, DisplayOrder=5, Icon="fa fa-building", ShowInView = true },
                new Page() { Id=306, Path = "/address/street",              Title="Street",             ParentPageId=300, DisplayOrder=6, Icon="fa fa-street-view", ShowInView = true },

                new Page() { Id=400, Path = "/settings",                    Title="Settings",           DisplayOrder=8 ,Icon="fa-cog", ShowInView = true},
                new Page() { Id=401, Path = "/settings/purposeoforder",     Title="Purpose Of Order",   ParentPageId=400, DisplayOrder = 1, Icon="fa-circle-o", ShowInView = true },
                new Page() { Id=402, Path = "/settings/natureofbusiness",   Title="Nature Of Business", ParentPageId=400, DisplayOrder = 2 , Icon="fa-briefcase", ShowInView = true },
                new Page() { Id=403, Path = "/settings/secretquestion",     Title="Secret Question",    ParentPageId=400, DisplayOrder = 3  , Icon="fa-question-circle", ShowInView = true },

            };
            pages.ForEach(c =>
            {
                context.Pages.AddOrUpdate(c);
            });
            context.SaveChanges();
            #endregion

            #region Role

            var adminMenu = new List<int>() { 1, 100, 101, 102,
                                              200, 201,
                                              300, 301, 302, 303, 304, 305, 306,
                                              400,401,402,403};

            context.CreateRole(new Role()
            {
                Id = new Guid(ApplicationConstants.SuperAdminRoleGuid),
                Name = "Super Admin",
                Description = "Super Admin",
                IsActive = true,
                SystemDefined = true,
                Hierarchy = 1
            }, adminMenu);
            context.CreateRole(new Role()
            {
                Id = new Guid(ApplicationConstants.AdminRoleGuid),
                Name = "Admin",
                Description = "Admin",
                IsActive = true,
                SystemDefined = true,
                Hierarchy = 2
            }, adminMenu);

            pages.ForEach(c =>
            {
                context.Pages.AddOrUpdate(c);
            });

            context.SaveChanges();

            #endregion

            #region Super Admin
            const string superadminemail = "admin@tgsolutions.co";
            const string admin = "tgamitk@gmail.co";
            context.CreateUser(new User()
            {
                Id = new Guid(ApplicationConstants.SuperAdminUserGuid),
                FullName = "Super Admin",
                EmailConfirmed = true,
                Email = superadminemail,
                UserName = superadminemail,
                PasswordHash = new PasswordHasher().HashPassword("Admin@123"),
                SecurityStamp = Guid.NewGuid().ToString(),
                SystemDefined = true,
                Status = UserStatus.Active,
                ApprovalStatus = ApprovalStatus.Approved,
                Type = UserType.SystemUser,
            }, new Guid(ApplicationConstants.SuperAdminRoleGuid));
            context.CreateUser(new User()
            {
                Id = new Guid(ApplicationConstants.AdminUserGuid),
                FullName = "Admin",
                EmailConfirmed = true,
                Email = admin,
                UserName = admin,
                PasswordHash = new PasswordHasher().HashPassword("Admin@123"),
                SecurityStamp = Guid.NewGuid().ToString(),
                SystemDefined = true,
                Status = UserStatus.Active,
                ApprovalStatus = ApprovalStatus.Approved,
                Type = UserType.SystemUser,
            }, new Guid(ApplicationConstants.SuperAdminRoleGuid));
            #endregion



            #region PostCodeGroup
            var postcodeGroup = new List<PostCodeGroup>()
            {
                new PostCodeGroup()
                {
                    Id=1,
                    PostCodeGroupName="Default",
                    IsEnabled = true
                },
                 new PostCodeGroup()
                {
                    Id=2,
                    PostCodeGroupName="Default2",
                    IsEnabled = true
                },
                  new PostCodeGroup()
                {
                    Id=3,
                    PostCodeGroupName="Default3",
                    IsEnabled = true
                }
            };
            postcodeGroup.ForEach(c =>
            {
                context.PostCodeGroups.AddOrUpdate(c);
            });
            context.SaveChanges();
            #endregion

        }
    }
}

