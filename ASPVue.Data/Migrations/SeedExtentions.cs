﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using ASPVueProject.Domain.Constant;
using ASPVueProject.Domain.Entity;

namespace ASPVueProject.Data.Migrations
{
	public static class SeedExtentions
	{
		public static void CreateRole(this ApplicationContext context, Role role, List<int> menuIds)
		{
			context.Roles.AddOrUpdate(role);
			if (menuIds.Any())
				menuIds.Distinct().ToList().ForEach(c =>
				{
					context.UserPages.AddOrUpdate(p => new { p.PageId, p.RoleId }, new UserPage() { PageId = c, RoleId = role.Id });
				});
			context.SaveChanges();
		}
		public static void CreateUser(this ApplicationContext context, User user, Guid roleId)
		{
			user.Roles.Add(new UserRole { RoleId = roleId, UserId = user.Id });
			context.Users.AddOrUpdate(user);
			context.SaveChanges();
		}
		public static void SaveEnums(this ApplicationContext context)
		{
			var enumTypes = AppDomain.CurrentDomain.GetAssemblies()
				.SelectMany(t => t.GetTypes())
				.Where(t => t.IsEnum && t.Namespace == "ASPVueProject.Domain.Enum");
			foreach (var enumType in enumTypes)
			{
				var values = Enum.GetValues(enumType).Cast<int>();
				foreach (var i in values)
				{
					var name = Enum.GetName(enumType, i);
					var description = enumType.GetField(name).GetCustomAttributes(typeof(DescriptionAttribute), true);
					context.EnumCollections.AddOrUpdate(new EnumCollection()
					{
						Type = enumType.Name,
						Id = i,
						Name = description.Any() ? ((DescriptionAttribute)description[0]).Description : name
					});
				}
			}
			var constants = typeof(ApplicationConstants).GetFields();
			foreach (var fieldInfo in constants)
			{
				context.ConstantCollections.AddOrUpdate(new ConstantCollection()
				{
					Value = fieldInfo.GetValue(null).ToString(),
					Key = fieldInfo.Name
				});
			}
		}
		public static void RunScripts(this ApplicationContext context)
		{
			var sqlFiles = Directory.GetFiles(GetBaseDirectory(), "*.sql").Select(f => new FileInfo(f)).OrderByDescending(f => f.Name).ToList();
			var exludedFiles = new List<string>() { "" };
			foreach (var file in sqlFiles)
			{
				if (exludedFiles.All(c => c != file.Name))
				{
					context.DropIfExists($"{file.Name.Replace(".sql", "")}");
					var text = File.ReadAllText(file.FullName);
					text = text.Replace("ALTER", "CREATE");
					text = text.Replace("alter", "CREATE");
					context.Database.ExecuteSqlCommand(text);
				}
			}
		}
		private static string GetBaseDirectory()
		{
			var baseDir = AppDomain.CurrentDomain.BaseDirectory + "\\Migrations\\Sql";
			if (!Directory.Exists(baseDir))
				baseDir = AppDomain.CurrentDomain.BaseDirectory + "\\bin\\Migrations\\Sql";
			return baseDir;
		}
		private static void DropIfExists(this ApplicationContext context, string name)
		{
			var type = context.Database.SqlQuery<string>(
					$"SELECT  ROUTINE_TYPE FROM Information_schema.Routines WHERE Specific_schema = 'dbo' AND specific_name = '{name}'").SingleOrDefault();
			if (type != null)
				context.Database.ExecuteSqlCommand($"Drop {type} dbo.{name}");
		}
	}
}
