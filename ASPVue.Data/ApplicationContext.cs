﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using ASPVueProject.Domain.Entity;
using Microsoft.AspNet.Identity.EntityFramework;


namespace ASPVueProject.Data
{
	public class ApplicationContext : IdentityDbContext<User, Role, Guid, UserLogin, UserRole, UserClaim>
	{
		public ApplicationContext()
			: base("ApplicationContext")
		{
			//Configuration.LazyLoadingEnabled = false;
			Configuration.ProxyCreationEnabled = true;

			if (!Database.Exists())
				Database.SetInitializer(new CreateDatabaseIfNotExists<ApplicationContext>());
			Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationContext, Migrations.Configuration>());
		}
		public ApplicationContext(string connectionString)
			: base(connectionString)
		{

		}
		public static ApplicationContext Create()
		{
			return new ApplicationContext();
		}
		public virtual DbSet<ErrorLog> ErrorLogs { get; set; }
		public virtual DbSet<ApplicationLog> ApplicationLogs { get; set; }
		public virtual DbSet<Page> Pages { get; set; }
		public virtual DbSet<UserPage> UserPages { get; set; }
		public virtual DbSet<EnumCollection> EnumCollections { get; set; }
		public virtual DbSet<ConstantCollection> ConstantCollections { get; set; }
		public virtual DbSet<Setting> Settings { get; set; }
		public virtual DbSet<DeviceLog> DeviceLogs { get; set; }
		public virtual DbSet<UserDeviceKey> UserDeviceKeys { get; set; }
		public virtual DbSet<UserPasscode> UserPasscodes { get; set; }
		public virtual DbSet<Country> Countries { get; set; }

        public virtual DbSet<City> Citys { get; set; }
        public virtual DbSet<PostCode> PostCodes { get; set; }
  
		public virtual DbSet<FireBaseLog> FireBaseLogs { get; set; }
		
		public virtual DbSet<SmsLog> SmsLogs { get; set; }
		public virtual DbSet<Notification> Notifications { get; set; }
		public virtual DbSet<NotificationUserLog> NotificationUserLogs { get; set; }
	
		public virtual DbSet<PreviousPassword> PreviousPasswords { get; set; }
		public virtual DbSet<Advertisement> Advertisements { get; set; }
        public virtual DbSet<PurposeOfOrder> PurposeOfOrders { get; set; }
        public virtual DbSet<NatureOfBusiness> NatureOfBusinesss { get; set; }
        public virtual DbSet<StateProvince> StateProvinces { get; set; }
        public virtual DbSet<SecretQuestion> SecretQuestions { get; set; }
        
        public virtual DbSet<Suburb> Suburbs { get; set; }
        public virtual DbSet<Street> Streets { get; set; }
       
        public virtual DbSet<DocumentSettings> DocumentSettings { get; set; }
         
        public virtual DbSet<CorporateMemberType> CorporateMemberTypes { get; set; }
        public virtual DbSet<PostCodeGroup> PostCodeGroups { get; set; }
        
        public virtual DbSet<GeneralSetting> GeneralSettings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
			modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

			modelBuilder.Entity<User>().ToTable("User");
			modelBuilder.Entity<Role>().ToTable("Role");
			modelBuilder.Entity<UserClaim>().ToTable("UserClaim");
			modelBuilder.Entity<UserRole>().ToTable("UserRole");
			modelBuilder.Entity<UserLogin>().ToTable("UserLogin");
		}
	}
}
