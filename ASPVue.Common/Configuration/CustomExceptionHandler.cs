﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http.ExceptionHandling;
using Newtonsoft.Json;

namespace ASPVueProject.Common
{
	public class CustomExceptionHandler : ExceptionHandler
	{
		private string FormatMessage(List<ResponseMessage> model)
		{
			return JsonConvert.SerializeObject(model);
		}
		public override void Handle(ExceptionHandlerContext context)
		{
			var message = new List<ResponseMessage>();
			var ex = context.Exception as ApiException ?? new ApiException(HttpStatusCode.InternalServerError, null);
			if (ex.StatusCode == HttpStatusCode.BadRequest)
			{
				var badRequestData = JsonConvert.DeserializeObject<BadRequest>(ex.JsonData);
				if (badRequestData.ModelState != null && badRequestData.ModelState.Any())
				{
					foreach (var item in badRequestData.ModelState)
					{
						message.Add(new ResponseMessage() { Title = item.Key.Replace("model.", ""), Message = item.Value.First().ToString() });
					}
				}
				if (string.Equals(badRequestData.Error, "invalid_grant"))
					message.Add(new ResponseMessage() { Title = "invalid_grant", Message = string.IsNullOrEmpty(badRequestData.ErrorDescription) ? badRequestData.Error_Description : badRequestData.ErrorDescription });
			}
			else if (ex.StatusCode == HttpStatusCode.Unauthorized)
			{
				message.Add(new ResponseMessage() { Title = "Authorized", Message = "Unauthorized" });
			}
			else
			{
				if (ex.JsonData != null)
				{
					var errorData = JsonConvert.DeserializeObject<BadRequest>(ex.JsonData);
					message.Add(new ResponseMessage() { Title = "message", Message = string.IsNullOrEmpty(errorData.ExceptionMessage) ? errorData.Message : errorData.ExceptionMessage });
				}
				else
					message.Add(new ResponseMessage() { Title = "message", Message = context.Exception.Message });
			}

			if (message.Count == 0)
				message.Add(new ResponseMessage() { Title = "message", Message = "Unknown Error" });

			context.Result = new ExceptionResponse
			{
				statusCode = ex.StatusCode.Value,
				message = FormatMessage(message),
				request = context.Request
			};
		}
		public override bool ShouldHandle(ExceptionHandlerContext context)
		{
			return true;
		}
	}
}
