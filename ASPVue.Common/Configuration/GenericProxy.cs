﻿using System;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace ASPVueProject.Common
{
	public sealed class GenericProxy<TContract> : IDisposable where TContract : class
	{
		private readonly ChannelFactory<TContract> _channelFactory;
		private TContract _channel;

		private bool OverrideCertificateValidation(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors error)
		{
			return true;
		}
		public GenericProxy(string endpointName, SecurityProtocolType tls = SecurityProtocolType.Tls12)
		{
			if (tls == SecurityProtocolType.Tls12)
			{
				ServicePointManager.SecurityProtocol = tls;
			}
			_channelFactory = new ChannelFactory<TContract>(endpointName);
			if (tls == SecurityProtocolType.Tls12)
			{
				ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(OverrideCertificateValidation);
			}
		}
		public GenericProxy(Binding binding, EndpointAddress remoteAddress)
		{
			_channelFactory = new ChannelFactory<TContract>(binding, remoteAddress);
		}
		public void Execute(Action<TContract> action)
		{
			action.Invoke(Channel);
		}
		public TResult Execute<TResult>(Func<TContract, TResult> function)
		{
			return function.Invoke(Channel);
		}
		private TContract Channel => _channel ?? (_channel = _channelFactory.CreateChannel());
		public void Dispose()
		{
			try
			{
				if (_channel != null)
				{
					var currentChannel = _channel as IClientChannel;
					if (currentChannel.State == CommunicationState.Faulted)
					{
						currentChannel.Abort();
					}
					else
					{
						currentChannel.Close();
					}
				}
			}
			finally
			{
				_channel = null;
				GC.SuppressFinalize(this);
			}
		}
	}
}