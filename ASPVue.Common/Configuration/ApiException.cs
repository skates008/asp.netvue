﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json;

namespace ASPVueProject.Common
{
	public class ApiException : Exception
	{
		public ApiException(HttpStatusCode statusCode, string jsonData)
		{
			StatusCode = statusCode;
			JsonData = jsonData;
		}
		public HttpStatusCode? StatusCode { get; set; }
		public string JsonData { get; private set; }
	}

	public class ResponseMessage
	{
		public string Title { get; set; }
		public string Message { get; set; }
	}

	public class BadRequest
	{
		public string Error { get; set; }
		public string ErrorDescription { get; set; }
		public string Error_Description { get; set; }
		public string Message { get; set; }
		public Dictionary<string, List<string>> ModelState { get; set; }
		[JsonProperty("exceptionMessage")]
		public string ExceptionMessage { get; set; }
	}

	public class ExceptionResponse : IHttpActionResult
	{
		public HttpStatusCode statusCode { get; set; }
		public string message { get; set; }
		public HttpRequestMessage request { get; set; }
		public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
		{
			var response = new HttpResponseMessage(statusCode);
			response.RequestMessage = request;
			response.Content = new StringContent(message);
			return Task.FromResult(response);
		}
	}
}