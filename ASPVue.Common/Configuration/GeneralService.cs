﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using ASPVueProject.DTO.ViewModel;

#pragma warning disable 618

namespace ASPVueProject.Common.Configuration
{
	public static class GeneralService
	{
		public static string TimeZoneInfoName = "Nepal Standard Time";
		public static string ClientIp
		{
			get
			{
				var context = HttpContext.Current;
				var ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

				if (string.IsNullOrEmpty(ipAddress)) return context.Request.ServerVariables["REMOTE_ADDR"];
				var addresses = ipAddress.Split(',');
				return addresses.Length != 0 ? addresses[0] : context.Request.ServerVariables["REMOTE_ADDR"];
			}
		}
		public static string ApplicationUrl => HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath?.TrimEnd('/') + "/";
		public static string ClientIpFromHeader => HttpContext.Current.Request.Headers["ClientIp"];
		public static DateTime CurrentDate()
		{
			DateTime utcDate = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
			return TimeZoneInfo.ConvertTimeFromUtc(utcDate, TimeZoneInfo.FindSystemTimeZoneById(TimeZoneInfoName));
		}
		public static void SetCulture()
		{
			var newCulture = new CultureInfo("en-GB");
			Thread.CurrentThread.CurrentCulture = newCulture;
		}
		public static T ConvertToEnum<T>(object value)
		{
			return (T)Enum.Parse(typeof(T), value.ToString());
		}
		public static TEnum ParseToEnum<TEnum>(object valueToParse)
		{
			if (valueToParse == null)
			{
				throw new ArgumentNullException("Value to parse required");
			}
			var returnValue = (TEnum)Enum.Parse(typeof(TEnum), valueToParse.ToString(), true);
			return returnValue;
		}
		public static T ToClass<T>(this Dictionary<string, object> dict)
		{
			var type = typeof(T);
			var obj = Activator.CreateInstance(type);

			foreach (var kv in dict)
			{
				type.GetProperty(kv.Key).SetValue(obj, kv.Value);
			}
			return (T)obj;
		}
		public static IDictionary<string, object> ToDictionary<T>(this T source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
		{
			return source.GetType().GetProperties(bindingAttr).ToDictionary
			(
				propInfo => propInfo.Name,
				propInfo => propInfo.GetValue(source, null)
			);
		}
		public static class EnumList<T>
		{
			public static IEnumerable<ListViewModel> Get()
			{
				var names = GetDescriptions(typeof(T));
				var values = Enum.GetValues(typeof(T)).Cast<int>();

				return names.Zip(values, (name, value) => new ListViewModel() { Text = name, Id = value }).ToList();
			}
			private static IEnumerable<string> GetDescriptions(Type type)
			{
				var names = Enum.GetNames(type);
				return (from name in names let field = type.GetField(name) let fds = field.GetCustomAttributes(typeof(DescriptionAttribute), true) select fds.Any() ? ((DescriptionAttribute)fds[0]).Description : name).ToList();
			}
		}
	}
}