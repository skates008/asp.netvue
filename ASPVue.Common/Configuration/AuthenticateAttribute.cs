﻿using Newtonsoft.Json;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace ASPVueProject.Common
{
	public class AuthenticateAttribute : ActionFilterAttribute
	{
		public const string XTokenName = "X-Token";
		public const string LoadingBarHeaderName = "HideLoadingBar";
		public const string IgnoreSessionCheck = "IgnoreSessionCheck";
		public override void OnActionExecuting(HttpActionContext actionContext)
		{
			var tokenHeader = HttpContext.Current.Request[XTokenName] ?? "";
			if (!actionContext.ModelState.IsValid)
			{
				throw new ApiException(HttpStatusCode.BadRequest, JsonConvert.SerializeObject(new BadRequest()
				{
					ModelState = actionContext.ModelState.Where(x => x.Value.Errors.Any()).ToDictionary(kvp => kvp.Key, kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToList())
				}));
			}
		}
		public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
		{
			if (actionExecutedContext.Response != null)
			{
				var loadingBarHeader = actionExecutedContext.Request.Headers.Contains(LoadingBarHeaderName) ? actionExecutedContext.Request.Headers.GetValues(LoadingBarHeaderName).FirstOrDefault() : "";
				actionExecutedContext.Response.Headers.Add(LoadingBarHeaderName, loadingBarHeader);
				var ignoreSessionCheck = actionExecutedContext.Request.Headers.Contains(IgnoreSessionCheck) ? actionExecutedContext.Request.Headers.GetValues(IgnoreSessionCheck).FirstOrDefault() : "";
				actionExecutedContext.Response.Headers.Add(IgnoreSessionCheck, ignoreSessionCheck);
			}
		}
	}
	public class IgnoreValidationAttribute : ActionFilterAttribute
	{

	}
	public class ModelStateValidatorAttribute : ActionFilterAttribute
	{
		public override void OnActionExecuting(HttpActionContext actionContext)
		{
			if (actionContext.ActionArguments.Count > 0)
			{
				foreach (var item in actionContext.ActionArguments)
				{
					if (item.Value == null)
						actionContext.ModelState.AddModelError("Input Parameters", "Parameters Required");
				}
			}
			if (!actionContext.ModelState.IsValid)
			{
				throw new ApiException(HttpStatusCode.BadRequest, JsonConvert.SerializeObject(new BadRequest()
				{
					ModelState = actionContext.ModelState.Where(x => x.Value.Errors.Any()).ToDictionary(kvp => kvp.Key, kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToList())
				}));
			}
		}
	}
}