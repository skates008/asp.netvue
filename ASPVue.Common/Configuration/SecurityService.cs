﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;
using Newtonsoft.Json;

namespace ASPVueProject.Common
{
	public static class SecurityService
	{
		private const string InitializationVector = "1345158303522110";
		private const string Key = "1b231v091d720v16w6cac765bdog0a15";
		private const string Purpose = "SecureToken";
		public static string Encrypt(string text, string initializationVector = InitializationVector, string key = Key)
		{
			var myAes = new AesManaged();
			byte[] biteword;
			// Override the cipher mode, key and IV
			myAes.Mode = CipherMode.CBC;
			myAes.IV = Encoding.UTF8.GetBytes(initializationVector);  //new byte[16] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; // CRB mode uses an empty IV
			myAes.Key = Encoding.UTF8.GetBytes(Key);
			myAes.Padding = PaddingMode.ANSIX923;

			// Create a encryption object to perform the stream transform.
			ICryptoTransform encryptor = myAes.CreateEncryptor(myAes.Key, myAes.IV);

			// TODO: perform the encryption / decryption as required...
			// Create the streams used for encryption.
			using (var msEncrypt = new MemoryStream())
			{
				using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
				{
					using (var srEncrypt = new StreamWriter(csEncrypt))
					{

						//Write all data to the stream.
						srEncrypt.Write(text);
					}
					biteword = msEncrypt.ToArray();
				}
			}
			// Return the encrypted bytes from the memory stream.
			return Protect(Convert.ToBase64String(biteword));
		}
		public static string Decrypt(string text, string initializationVector = InitializationVector, string key = Key)
		{
			using (var aesAlg = new AesManaged())
			{
				aesAlg.KeySize = 256;
				aesAlg.BlockSize = 128;
				aesAlg.Mode = CipherMode.CBC;
				aesAlg.IV = Encoding.UTF8.GetBytes(initializationVector);  //new byte[16] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; // CRB mode uses an empty IV
				aesAlg.Key = Encoding.UTF8.GetBytes(key);
				aesAlg.Padding = PaddingMode.ANSIX923;

				// Create a decrytor to perform the stream transform.
				ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

				// Create the streams used for decryption.
				using (var msDecrypt = new MemoryStream(Convert.FromBase64String(Unprotect(text))))
				{
					using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
					{
						using (var srDecrypt = new StreamReader(csDecrypt))
						{

							// Read the decrypted bytes from the decrypting stream
							// and place them in a string.
							text = srDecrypt.ReadToEnd();
						}
					}

				}

			}
			return text;
		}
		public static string Encrypt<T>(this T model, string initializationVector = InitializationVector, string key = Key)
		{
			var text = JsonConvert.SerializeObject(model);

			var myAes = new AesManaged();
			byte[] biteword;
			// Override the cipher mode, key and IV
			myAes.Mode = CipherMode.CBC;
			myAes.IV = Encoding.UTF8.GetBytes(initializationVector);  //new byte[16] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; // CRB mode uses an empty IV
			myAes.Key = Encoding.UTF8.GetBytes(key);
			myAes.Padding = PaddingMode.ANSIX923;

			// Create a encryption object to perform the stream transform.
			var encryptor = myAes.CreateEncryptor(myAes.Key, myAes.IV);

			// TODO: perform the encryption / decryption as required...
			// Create the streams used for encryption.
			using (var msEncrypt = new MemoryStream())
			{
				using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
				{
					using (var srEncrypt = new StreamWriter(csEncrypt))
					{

						//Write all data to the stream.
						srEncrypt.Write(text);
					}
					biteword = msEncrypt.ToArray();
				}
			}
			// Return the encrypted bytes from the memory stream.
			return Protect(Convert.ToBase64String(biteword));
		}
		public static T Decrypt<T>(this string text, string initializationVector = InitializationVector, string key = Key)
		{
			using (var aesAlg = new AesManaged())
			{
				aesAlg.KeySize = 256;
				aesAlg.BlockSize = 128;
				aesAlg.Mode = CipherMode.CBC;
				aesAlg.IV = Encoding.UTF8.GetBytes(initializationVector);  //new byte[16] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; // CRB mode uses an empty IV
				aesAlg.Key = Encoding.UTF8.GetBytes(key);
				aesAlg.Padding = PaddingMode.ANSIX923;

				// Create a decrytor to perform the stream transform.
				var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

				// Create the streams used for decryption.
				using (var msDecrypt = new MemoryStream(Convert.FromBase64String(Unprotect(text))))
				{
					using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
					{
						using (var srDecrypt = new StreamReader(csDecrypt))
						{

							// Read the decrypted bytes from the decrypting stream
							// and place them in a string.
							text = srDecrypt.ReadToEnd();
						}
					}

				}

			}
			return JsonConvert.DeserializeObject<T>(text);
		}
		public static string Protect(string text, string purpose = Purpose)
		{
			if (string.IsNullOrEmpty(text))
				return null;

			var stream = Encoding.UTF8.GetBytes(text);
			var encodedValue = MachineKey.Protect(stream, purpose);
			return HttpServerUtility.UrlTokenEncode(encodedValue);
		}
		public static string Unprotect(string text, string purpose = Purpose)
		{
			if (string.IsNullOrEmpty(text))
				return null;

			var stream = HttpServerUtility.UrlTokenDecode(text);
			var decodedValue = MachineKey.Unprotect(stream, purpose);
			return Encoding.UTF8.GetString(decodedValue);
		}
	}
}