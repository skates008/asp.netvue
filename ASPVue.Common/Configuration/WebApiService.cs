﻿using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ASPVueProject.DTO.ViewModel;

namespace ASPVueProject.Common
{
	public class WebApiService
	{
		private WebApiService() { }
		private WebApiService(string baseUri, TokenViewModel token)
		{
			BaseUri = baseUri;
			Token = token;
		}
		private static WebApiService _instance;
		private static WebApiService _instanceExternal;
		public static WebApiService Instance => _instance ?? (_instance = new WebApiService(ConfigurationManager.AppSettings["WebApiUri"], new TokenViewModel()));
		public static WebApiService InstanceForExternal => _instanceExternal ?? (_instanceExternal = new WebApiService());
		public string BaseUri { get; }
		public TokenViewModel Token { get; }
		public async Task<T> AuthenticateAsync<T>(string userName, string password, bool isBOUser = false)
		{
			using (var client = new HttpClient())
			{
				client.AddHeaders();
				var arguments =
					new List<KeyValuePair<string, string>>
					{
						new KeyValuePair<string, string>("grant_type", "password"),
						new KeyValuePair<string, string>("userName", userName),
						new KeyValuePair<string, string>("password", password),
						new KeyValuePair<string, string>("isBOUser", isBOUser.ToString()),
					};


				var result = await client.PostAsync(BuildActionUri("token"), new FormUrlEncodedContent(arguments));

				client.DefaultRequestHeaders.Add("IPAddress", GetIpAddress());

				var json = await result.Content.ReadAsStringAsync();

				if (!result.IsSuccessStatusCode) throw new ApiException(result.StatusCode, json);

				var res =
					json.Replace("\\\"", "\"")
						.Replace("\"[", "[")
						.Replace("]\"", "]")
						.Replace("\"{", "{")
						.Replace("}\"", "}");

				return JsonConvert.DeserializeObject<T>(res);
			}
		}
		public async Task<T> AuthenticateWithoutLoginAsync<T>(string userName, string password)
		{
			using (var client = new HttpClient())
			{
				client.AddHeaders();
				var arguments =
					new List<KeyValuePair<string, string>>
				{
					new KeyValuePair<string, string>("grant_type", "password"),
					new KeyValuePair<string, string>("userName", userName),
					new KeyValuePair<string, string>("password", password)
				};



				var result = await client.PostAsync(BuildActionUri("token"), new FormUrlEncodedContent(arguments));

				client.DefaultRequestHeaders.Add("IPAddress", GetIpAddress());

				var json = await result.Content.ReadAsStringAsync();

				if (!result.IsSuccessStatusCode) throw new ApiException(result.StatusCode, json);

				var res =
					json.Replace("\\\"", "\"")
						.Replace("\"[", "[")
						.Replace("]\"", "]")
						.Replace("\"{", "{")
						.Replace("}\"", "}");

				return JsonConvert.DeserializeObject<T>(res);
			}
		}
		public async Task<T> RenewToken<T>(string refreshToken)
		{
			using (var client = new HttpClient())
			{
				client.AddHeaders();
				var result =
					await
						client.PostAsync(BuildActionUri("token"),
							new FormUrlEncodedContent(new List<KeyValuePair<string, string>>
							{
								new KeyValuePair<string, string>("grant_type", "refresh_token"),
								new KeyValuePair<string, string>("refresh_token", refreshToken)
							}));
				client.DefaultRequestHeaders.Add("IPAddress", GetIpAddress());

				var json = await result.Content.ReadAsStringAsync();
				if (!result.IsSuccessStatusCode) throw new ApiException(result.StatusCode, json);
				var res =
					json.Replace("\\\"", "\"")
						.Replace("\"[", "[")
						.Replace("]\"", "]")
						.Replace("\"{", "{")
						.Replace("}\"", "}");
				return JsonConvert.DeserializeObject<T>(res);
			}
		}
		protected static string GetIpAddress()
		{
			var context = System.Web.HttpContext.Current;
			var ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

			if (string.IsNullOrEmpty(ipAddress)) return context.Request.ServerVariables["REMOTE_ADDR"];
			var addresses = ipAddress.Split(',');
			return addresses.Length != 0 ? addresses[0] : context.Request.ServerVariables["REMOTE_ADDR"];
		}
		public async Task<T> GetAsync<T>(string action)
		{
			using (var client = new HttpClient())
			{
				client.AddHeaders();

				var result = await client.GetAsync(BuildActionUri(action));

				var json = await result.Content.ReadAsStringAsync();
				if (result.IsSuccessStatusCode)
				{
					return JsonConvert.DeserializeObject<T>(json);
				}
				throw new ApiException(result.StatusCode, json);
			}
		}
		public T Get<T>(string action)
		{
			using (var client = new HttpClient())
			{
				client.AddHeaders();

				var result = client.GetAsync(BuildActionUri(action)).Result;

				var json = result.Content.ReadAsStringAsync().Result;
				if (result.IsSuccessStatusCode)
				{
					return JsonConvert.DeserializeObject<T>(json);
				}
				throw new ApiException(result.StatusCode, json);
			}
		}
		public void Get(string action)
		{
			using (var client = new HttpClient())
			{
				client.AddHeaders();

				var result = client.GetAsync(BuildActionUri(action)).Result;
				if (result.IsSuccessStatusCode)
				{
					return;
				}
				var json = result.Content.ReadAsStringAsync().Result;
				throw new ApiException(result.StatusCode, json);
			}
		}
		public async Task<T> GetExternalAsync<T>(string action)
		{
			using (var client = new HttpClient())
			{
				client.AddHeaders();

				var result = await client.GetAsync(action);

				var json = await result.Content.ReadAsStringAsync();
				if (result.IsSuccessStatusCode)
				{
					return JsonConvert.DeserializeObject<T>(json);
				}
				throw new ApiException(result.StatusCode, json);
			}
		}
		public async Task GetAsync(string action)
		{
			using (var client = new HttpClient())
			{
				client.AddHeaders();

				var result = await client.GetAsync(BuildActionUri(action));
				if (result.IsSuccessStatusCode)
				{
					return;
				}
				var json = await result.Content.ReadAsStringAsync();
				throw new ApiException(result.StatusCode, json);
			}
		}
		public async Task GetExternalAsync(string action)
		{
			using (var client = new HttpClient())
			{
				client.AddHeaders();

				var result = await client.GetAsync(action);
				if (result.IsSuccessStatusCode)
				{
					return;
				}
				var json = await result.Content.ReadAsStringAsync();
				throw new ApiException(result.StatusCode, json);
			}
		}
		public async Task<T> PostAsync<T>(string action, object data)
		{
			using (var client = new HttpClient())
			{
				client.AddHeaders();

				var result = await client.PostAsJsonAsync(BuildActionUri(action), data);
				var json = await result.Content.ReadAsStringAsync();
				if (result.IsSuccessStatusCode)
				{
					return JsonConvert.DeserializeObject<T>(json);
				}
				throw new ApiException(result.StatusCode, json);
			}
		}
		public async Task PostAsync(string action, object data = null)
		{
			using (var client = new HttpClient())
			{
				client.AddHeaders();

				var result = await client.PostAsJsonAsync(BuildActionUri(action), data);
				if (result.IsSuccessStatusCode)
				{
					return;
				}
				var json = await result.Content.ReadAsStringAsync();
				throw new ApiException(result.StatusCode, json);
			}
		}
		public void Post(string action, object data = null)
		{
			using (var client = new HttpClient())
			{
				client.AddHeaders();

				var result = client.PostAsJsonAsync(BuildActionUri(action), data).Result;
				if (result.IsSuccessStatusCode)
				{
					return;
				}
				var json = result.Content.ReadAsStringAsync().Result;
				throw new ApiException(result.StatusCode, json);
			}
		}
		public T Post<T>(string action, object data)
		{
			using (var client = new HttpClient())
			{
				client.AddHeaders();

				var result = client.PostAsJsonAsync(BuildActionUri(action), data).Result;
				var json = result.Content.ReadAsStringAsync().Result;
				if (result.IsSuccessStatusCode)
				{
					return JsonConvert.DeserializeObject<T>(json);
				}
				throw new ApiException(result.StatusCode, json);
			}
		}
		private string BuildActionUri(string action)
		{
			return BaseUri.TrimEnd('/') + '/' + action.TrimStart('/');
		}
		private string BuildPaymentActionUri(string action)
		{
			return BaseUri.TrimEnd('/') + "/" + action.TrimStart('/');
		}
	}
}