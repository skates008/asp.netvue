﻿using System.Net.Http;
using System.Web;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System;
using RazorEngine;
using System.Web.Hosting;
using System.Text;
using System.Reflection;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.ComponentModel.DataAnnotations;
using ASPVueProject.Common.Configuration;
using System.Security.Principal;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using System.ComponentModel;
using ASPVueProject.DTO.ViewModel;

#pragma warning disable 618

namespace ASPVueProject.Common
{
	public static class Extensions
	{
		public static class EnumModel<T>
		{
			public static IEnumerable<ListViewModel> List()
			{
				var names = GetDescriptions(typeof(T));
				var values = Enum.GetValues(typeof(T)).Cast<int>();

				return names.Zip(values, (name, value) => new ListViewModel() { Text = name, Id = value }).ToList();
			}
			private static IEnumerable<string> GetDescriptions(Type type)
			{
				var names = Enum.GetNames(type);
				return (from name in names let field = type.GetField(name) let fds = field.GetCustomAttributes(typeof(DescriptionAttribute), true) select fds.Any() ? ((DescriptionAttribute)fds[0]).Description : name).ToList();
			}
		}

		#region Common
		public static LoginInfo GetUserLoginInfo(this IIdentity identity)
		{
			var loginInfo = GetClaimValue("LoginInfo");
			if (loginInfo == string.Empty)
				return null;
			var loginInfoVal = Newtonsoft.Json.JsonConvert.DeserializeObject<LoginInfo>(loginInfo);
			return loginInfoVal;
		}
		private static string GetClaimValue(string claimType)
		{
			var user = HttpContext.Current.User;
			if (user.Identity.IsAuthenticated)
			{
				return (user.Identity as ClaimsIdentity).FindFirstValue(claimType);
			}
			return string.Empty;
		}
		public static string ToShortDateTimeString(this DateTime date)
		{
			return date.ToShortDateString() + " " + date.ToShortTimeString();
		}
		public static DateTime ToDateTime(this String date)
		{
			return DateTime.Parse(date);
		}
		public static string ToDescription(this Enum value)
		{
			try
			{
				FieldInfo fi = value.GetType().GetField(value.ToString());

				DescriptionAttribute[] attributes =
					(DescriptionAttribute[])fi.GetCustomAttributes(
						typeof(DescriptionAttribute),
						false);

				if (attributes.Length > 0)
					return attributes[0].Description;
				return value.ToString();
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
		}
		#endregion

		#region WebApiService
		public static void AddHeaders(this HttpClient client)
		{
			var authToken = Token;
			if (!string.IsNullOrWhiteSpace(authToken) && authToken != "undefined")
				client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse("Bearer " + authToken);
			client.DefaultRequestHeaders.Add("ClientIp", ClientIp);
			client.DefaultRequestHeaders.Add("DeviceType", DeviceType);
			client.DefaultRequestHeaders.Add("DeviceKey", DeviceKey);
			client.DefaultRequestHeaders.Add("ApplicationUrl", ApplicationUrl);
		}
		#endregion

		#region General
		public static string Token
		{
			get
			{
				var context = HttpContext.Current;
				if (context == null)
					return "";
				var token = context.Request.Headers["X-Token"];
				if (!string.IsNullOrEmpty(token))
				{
					return token;
				}
				return "";
			}
		}
		public static string DeviceKey
		{
			get
			{
				var context = HttpContext.Current;
				if (context == null)
					return string.Empty;
				var authToken = context.Request.Headers["DeviceKey"];
				if (!string.IsNullOrEmpty(authToken))
				{
					return authToken;
				}
				return string.Empty;
			}
		}
		public static string DeviceType
		{
			get
			{
				var context = HttpContext.Current;
				if (context == null)
					return "Web";
				var deviceType = context.Request.Headers["DeviceType"];
				if (!string.IsNullOrEmpty(deviceType))
				{
					return deviceType;
				}
				return "Web";
			}
		}
		public static string ClientIp
		{
			get
			{
				var context = HttpContext.Current;
				if (context == null)
					return string.Empty;
				var ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

				if (!string.IsNullOrEmpty(ipAddress))
				{
					var addresses = ipAddress.Split(',');
					if (addresses.Length != 0)
					{
						return addresses[0];
					}
				}
				return context.Request.ServerVariables["REMOTE_ADDR"];
			}
		}
		public static string ApplicationUrl
		{
			get
			{
				if (HttpContext.Current == null)
					return string.Empty;
				return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/";
			}
		}
		public static string TerminalToken
		{
			get
			{
				var context = HttpContext.Current;
				if (context == null)
					return string.Empty;
				var authToken = context.Request.Headers["Token"];
				if (!string.IsNullOrEmpty(authToken))
				{
					return authToken;
				}
				return string.Empty;
			}
		}
		public static string IsTerminalUser
		{
			get
			{
				var context = HttpContext.Current;
				return context.Request.Headers["isTerminalUser"];

			}
		}
		private static void ByteArrayToHexString(byte[] bData, ref string s)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();

			for (int i = 0; i <= bData.GetUpperBound(0); i++)
			{
				sb.AppendFormat("{0:X2}", bData[i]);
			}
			s = sb.ToString();
			sb = null;

		}
		public static object GenerateHex(this HttpPostedFile file, string type = "", int maxFileSize = 10)
		{
			var extension = Path.GetExtension(file.FileName);
			var isValid = IsValidFile(file, type, maxFileSize);
			if (!string.IsNullOrEmpty(isValid.ErrorMessage))
			{
				throw new ArgumentNullException(isValid.ErrorMessage, innerException: null);
			}
			var path = System.Configuration.ConfigurationManager.AppSettings["TempFolder"] + "/" + DateTime.Now.ToString("yyyyMMdd");
			var onlyName = $"{Path.GetFileNameWithoutExtension(file.FileName)}-{DateTime.Now.ToString("ddMMyyyyHHmmss")}";
			var fileName = $"{onlyName}{Path.GetExtension(file.FileName)}";
			var folderPath = HostingEnvironment.MapPath(path);

			if (!Directory.Exists(folderPath))
				Directory.CreateDirectory(folderPath);

			var filePath = Path.Combine(folderPath, fileName);
			file.SaveAs(filePath);

			BinaryReader reader = new BinaryReader(new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.None));
			reader.BaseStream.Position = 0x0;     // The offset you are reading the data from
			byte[] data = reader.ReadBytes(0x490); // Read 1168 bytes into an array
			reader.Close();

			string HexString = string.Empty;
			ByteArrayToHexString(data, ref HexString);


			if (File.Exists(filePath))
				File.Delete(filePath);


			return new
			{
				HexString
			};
		}
		public static UploadResultViewModel SaveFile(this HttpPostedFile file, string type = "", int maxFileSize = 10)
		{
			var extension = Path.GetExtension(file.FileName);
			var isValid = IsValidFile(file, type, maxFileSize);
			if (!string.IsNullOrEmpty(isValid.ErrorMessage))
			{
				throw new ArgumentNullException(isValid.ErrorMessage, innerException: null);
			}
			var path = System.Configuration.ConfigurationManager.AppSettings["TempFolder"] + "/" + DateTime.Now.ToString("yyyyMMdd");
			var onlyName = $"{Path.GetFileNameWithoutExtension(file.FileName)}-{DateTime.Now.ToString("ddMMyyyyHHmmss")}";
			var fileName = $"{onlyName}{Path.GetExtension(file.FileName)}";
			var folderPath = HostingEnvironment.MapPath(path);

			if (!Directory.Exists(folderPath))
				Directory.CreateDirectory(folderPath);

			var filePath = Path.Combine(folderPath, fileName);
			file.SaveAs(filePath);

			return new UploadResultViewModel()
			{
				Name = onlyName,
				NameWithExtension = fileName,
				Extension = extension,
				Path = (path + "/" + fileName).Replace("~", ""),
				Success = true,
				PhysicalPath = filePath
			};
		}
		public static ValidationResult IsValidFile(object value, string validFileTypes, int fileSize)
		{
			if (value == null)
				return ValidationResult.Success;
			try
			{
				if (value.GetType() != typeof(HttpPostedFileWrapper))
					throw new InvalidOperationException("");
				var errorMessage = new StringBuilder();
				var file = value as HttpPostedFileBase;
				if ((value as HttpPostedFileBase).ContentLength > fileSize)
				{
					errorMessage.Append("File size limit exceeded");
				}
				var fileNameParts = Path.GetExtension(file?.FileName);
				var typeToValidate = validFileTypes.ToLower().Split(',').ToArray();
				if (!typeToValidate.Contains(fileNameParts))
				{
					errorMessage.Append("Invalid file type");
				}
				return string.IsNullOrEmpty(errorMessage.ToString()) ? ValidationResult.Success : new ValidationResult(errorMessage.ToString());

			}
			catch (Exception ex)
			{
				return new ValidationResult(ex.Message);
			}
		}
		public static void DeleteFile(this string path)
		{
			if (File.Exists(path))
			{
				File.Delete(path);
			}
		}
		public static string MoveToDestination(this string temPath, string folder)
		{
			if (string.IsNullOrEmpty(temPath) || string.IsNullOrWhiteSpace(temPath))
				return "";

			var path = string.Concat("~/Resources/Uploads/", folder);
			var mappedPath = HostingEnvironment.MapPath(path);

			if (!Directory.Exists(mappedPath))
				Directory.CreateDirectory(mappedPath);

			var fileName = Path.GetFileName(temPath);
			var permanentFilePath = mappedPath + "\\" + fileName;

			var tempFilePath = HostingEnvironment.MapPath(string.Concat("~", temPath));

			if (!File.Exists(permanentFilePath))
				File.Move(tempFilePath, permanentFilePath);


			return path.Replace("~", "") + "/" + fileName;
		}
		public static TValue GetAttributValue<TAttribute, TValue>(this PropertyInfo prop, Func<TAttribute, TValue> value) where TAttribute : Attribute
		{
			var att = prop.GetCustomAttributes(
				typeof(TAttribute), true
				).FirstOrDefault() as TAttribute;
			if (att != null)
			{
				return value(att);
			}
			return default(TValue);
		}
		public static Dictionary<string, object> ToDictionary<TEntity, TAttribute, TValue>(this TEntity entity, Func<TAttribute, TValue> value) where TAttribute : Attribute
		{
			var dictionary = typeof(TEntity).GetProperties()
				.Where(prop => Attribute.IsDefined(prop, typeof(TAttribute)))
				.Select(prop => new
				{
					Name = prop.GetAttributValue(value).ToString(),
					Value = prop.GetValue(entity, null)
				}).Where(attr => attr.Name != null).ToDictionary(p => p.Name, p => p.Value);
			return dictionary;
		}
		#endregion

		#region Report
		public static async Task<ReportResponseViewModel<T>> GetReport<T>(this ReportRequestViewModel request, string viewName, string apiUrl, string parentDirectory = null) where T : class
		{
			if (parentDirectory == null)
				parentDirectory = "Report";
			var reportTemplate = new ReportTemplateViewModel<T>();
			if (request.IsExport)
			{
				request.Take = 0;
				request.Skip = 0;
				//reportTemplate.IsExport = true;
				//reportTemplate.ExportType = request.ExportType;
				//reportTemplate.FilterText = request.FilterText;
			}
			var data = await WebApiService.Instance.PostAsync<ReportResponseViewModel<T>>(apiUrl, request);
			reportTemplate.Model = data.Data;
			reportTemplate.Skip = request.Skip;
			var html = data.Total > 0
				? reportTemplate.RenderView(HttpContext.Current.Server.MapPath($"~/Views/{parentDirectory}/{viewName}"))
				: "";
			data.Content = html;
			data.Data = null;
			return data;
		}
		public static async Task<HttpResponseMessage> GetExcel<T>(this HttpRequest httpRequest, string viewName, string apiUrl, string fileName) where T : class
		{
			ReportRequestViewModel reportRequestModel = new ReportRequestViewModel
			{
				IsExport = true,
				Filter = JsonConvert.DeserializeObject<Dictionary<string, object>>(httpRequest["Filter"]),
				FilterText = JsonConvert.DeserializeObject<Dictionary<string, object>>(httpRequest["FilterText"])
			};

			var report = await reportRequestModel.GetReport<T>(viewName, apiUrl);
			var result = new HttpResponseMessage(HttpStatusCode.OK)
			{
				Content = new StringContent(report.Content)
			};

			result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
			result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
			{
				FileName = $"{fileName}.xls"
			};
			return result;
		}
		public static async Task<HttpResponseMessage> Export<T>(this HttpRequest httpRequest, string viewName, string apiUrl, string fileName, string reportTitle, string customHeader = null, string parentDirectory = null) where T : class
		{
			ReportRequestViewModel reportRequestModel = new ReportRequestViewModel
			{
				Take = 0,
				Skip = 0,
				IsExport = true,
				ExportType = httpRequest["Type"],
				Filter = JsonConvert.DeserializeObject<Dictionary<string, object>>(httpRequest["Filter"]),
				FilterText = JsonConvert.DeserializeObject<Dictionary<string, object>>(httpRequest["FilterText"] ?? "")
			};
			string header = customHeader;
			if (header == null)
			{
				header = new ReportHeaderViewModel()
				{
					FilterText = reportRequestModel.FilterText,
					ReportTitle = reportTitle
				}.RenderView(HttpContext.Current.Server.MapPath("~/Views/Razor/Header.cshtml"));
			}
			var report = await reportRequestModel.GetReport<T>(viewName, apiUrl, parentDirectory);
			var result = new HttpResponseMessage(HttpStatusCode.OK);
			switch (reportRequestModel.ExportType)
			{
				case "pdf":
					Byte[] bytes;
					using (var ms = new MemoryStream())
					{
						using (var doc = new Document(PageSize.A4.Rotate(), 5, 5, 5, 5))
						{
							using (var writer = PdfWriter.GetInstance(doc, ms))
							{
								doc.Open();
								try
								{
									//var _cssSource = HttpContext.Current.Server.MapPath("~/node_modules/bootstrap/dist/css/bootstrap.min.css");
									//var _cssCustomeSource = HttpContext.Current.Server.MapPath("~/assets/invoice.css");
									//var css = System.IO.File.ReadAllText(_cssSource) + System.IO.File.ReadAllText(_cssCustomeSource);
									//using (var cssMemoryStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(css)))
									using (var msHtml = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(header + report.Content)))
									{
										XMLWorkerHelper.GetInstance()
											.ParseXHtml(writer, doc, msHtml, System.Text.Encoding.UTF8);
									}
								}
								finally
								{
									doc.Close();
								}
							}
						}
						bytes = ms.ToArray();
					}
					result.Content = new ByteArrayContent(bytes);
					result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
					result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
					{
						FileName = $"{fileName}.pdf"
					};
					break;
				default:
					result.Content = new StringContent(header + report.Content);
					result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
					result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
					{
						FileName = $"{fileName}.xls"
					};
					break;
			}
			return result;
		}
		public static string RenderReport<T>(this ReportTemplateViewModel<T> model, string templatePath)
		{
			var templateContent = File.ReadAllText(templatePath);
			return Razor.Parse(templateContent, model);
		}
		public static string ToNL(this string item)
		{
			return item += "\n";
		}
		#endregion

		#region Razor
		public static string RenderView<T>(this T model, string templatePath)
		{
			var templateContent = File.ReadAllText(templatePath);
			return Razor.Parse(templateContent, model);
		}
		public static string RenderRazorTemplate<T>(this T model, string templatePath, string layoutPath = null, string layoutName = null)
		{
			if (!(string.IsNullOrEmpty(layoutPath) || string.IsNullOrEmpty(layoutName)))
			{
				string templateContents = File.ReadAllText(layoutPath);
				Razor.Compile(templateContents, typeof(T), layoutName);
			}
			var templateContent = File.ReadAllText(templatePath);
			return Razor.Parse(templateContent, model);
		}
		#endregion

		#region Masking
		public static string Mask(this string content, char maskCharacter = 'X', int numberOfDigitsToVisibleFromLast = 4, int paddingLength = 0)
		{
			if (String.IsNullOrEmpty(content) || content.Length <= numberOfDigitsToVisibleFromLast)
			{
				if (paddingLength != default(int))
					return content.PadLeft(paddingLength, maskCharacter);
				return content;
			}

			var maskedText = "";
			for (var i = 0; i < content.Length - numberOfDigitsToVisibleFromLast; i++)
				maskedText += maskCharacter;
			maskedText += content.Substring(content.Length - numberOfDigitsToVisibleFromLast);
			if (paddingLength != default(int))
				return maskedText.PadLeft(paddingLength, maskCharacter);
			return maskedText;
		}
		#endregion
	}
}