﻿using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ASPVueProject.Common
{
	public class XmlLogHelper
	{
		public static string SaveIOXML(object obj, string fileName, string subfolder = "")
		{
			var xML = string.Empty;
			var servicemethodName = string.Empty;
			xML = SerializeObject(obj, obj.GetType());
			var strIOXMLPath = Path.Combine(@"" + ConfigurationManager.AppSettings["IOXMLPath"], @"IOXML\");
			strIOXMLPath = string.IsNullOrEmpty(subfolder) ? strIOXMLPath : (strIOXMLPath + subfolder + "\\");
			var strFullPath = string.Empty;
			XmlDocument xmlDoc;
			try
			{
				xmlDoc = new XmlDocument();
				xML = xML.Substring(39);
				xmlDoc.LoadXml(xML);
				if (Directory.Exists(strIOXMLPath) == false)
				{
					Directory.CreateDirectory(strIOXMLPath);
				}
				strFullPath = strIOXMLPath + fileName + ".xml";
				xmlDoc.Save(strFullPath);
			}
			catch (Exception ex)
			{
				//don't do anything
			}

			return xML;
		}
		public static T GetSavedXMLObject<T>(string fileName)
		{
			var returnObject = default(T);
			if (string.IsNullOrEmpty(fileName)) return default(T);

			try
			{
				var strIOXMLPath = Path.Combine(@"" + ConfigurationManager.AppSettings["ioxmlpath"], @"IOXML\");
				var strFullPath = strIOXMLPath + fileName + ".xml";
				var xmlStream = new StreamReader(strFullPath);
				var serializer = new XmlSerializer(typeof(T));
				returnObject = (T)serializer.Deserialize(xmlStream);
			}
			catch (Exception)
			{
			}
			return returnObject;

		}
		private static string SerializeObject(Object pObject, Type typClassName)
		{
			String XmlizedString = null;
			try
			{
				var memoryStream = new MemoryStream();
				var xs = new XmlSerializer(typClassName);
				var xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
				xs.Serialize(xmlTextWriter, pObject);
				memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
				XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
			}
			catch (Exception)
			{
				//do nothing
			}
			return XmlizedString;
		}
		private static String UTF8ByteArrayToString(Byte[] characters)
		{
			var constructedString = "";
			try
			{
				var encoding = new UTF8Encoding();
				constructedString = encoding.GetString(characters);
			}
			catch
			{
				//do nothing
			}
			return (constructedString);
		}
	}
	public enum XMLLogDirection
	{
		IN,
		OUT
	}
}