﻿using System.Data;
using CSVFile;

namespace ASPVueProject.Common
{
	public static class CSVHelper
	{
		public static DataTable ReadFromCSV(string path)
		{
			DataTable dt = new DataTable("CSV");
			dt = CSV.LoadDataTable(path);
			return dt;
		}
	}
}