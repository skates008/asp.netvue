﻿using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using OfficeOpenXml;

namespace ASPVueProject.Common
{
	public static class ExcelHelper
	{
		public static DataTable ReadFromExcelWithSheet(string filePath, string sheetName)
		{
			if (sheetName.Contains(" "))
			{
				sheetName = "'" + sheetName + "$'";
			}
			else
			{
				sheetName = sheetName + "$";
			}
			System.Data.OleDb.OleDbConnection conn1 = null;
			try
			{
				string connection = "";
				if (filePath.Substring(filePath.LastIndexOf('.')).Trim().ToLower() == ".xls")
				{
					connection = "Provider=Microsoft.Jet.OLEDB.4.0;" +
				   "Data Source=" + filePath + ";" +
				   "Extended Properties=\"Excel 8.0;HDR=YES\"";
				}
				else
				{
					connection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=YES\"";
				}
				conn1 = new OleDbConnection(connection);
				conn1.Open();
				DataTable dtSheetName = conn1.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
				conn1.Close();
				conn1.Open();
				OleDbCommand cmd = new OleDbCommand("Select * From [" + sheetName + "]", conn1);
				OleDbDataAdapter da = new OleDbDataAdapter(cmd);
				DataTable dtExcelData = new DataTable();
				da.Fill(dtExcelData);
				return dtExcelData;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				conn1.Close();
			}
		}

		public static DataTable ReadFromExcelWithNoSheet(string filePath)
		{
			OleDbConnection conn1 = null;
			try
			{
				string connection = "";
				if (filePath.Substring(filePath.LastIndexOf('.')).Trim().ToLower() == ".xls")
				{
					connection = "Provider=Microsoft.Jet.OLEDB.4.0;" +
				   "Data Source=" + filePath + ";" +
				   "Extended Properties=\"Excel 8.0;HDR=YES\"";
				}
				else
				{
					connection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=YES\"";
				}
				conn1 = new OleDbConnection(connection);
				conn1.Open();
				DataTable dtSheetName = conn1.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
				conn1.Close();
				conn1.Open();
				OleDbCommand cmd = new OleDbCommand("Select * From [{0}]", conn1);
				OleDbDataAdapter da = new OleDbDataAdapter(cmd);
				DataTable dtExcelData = new DataTable();
				da.Fill(dtExcelData);
				return dtExcelData;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				conn1.Close();
			}
		}

		public static DataTable WorksheetToDataTable(string filePath)
		{
			using (var pck = new ExcelPackage())
			{
				using (var stream = File.OpenRead(filePath))
				{
					pck.Load(stream);
				}
				var ws = pck.Workbook.Worksheets.First();
				DataTable tbl = new DataTable(ws.Name);
				bool hasHeader = true;
				foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
				{
					tbl.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
				}
				var startRow = hasHeader ? 2 : 1;
				for (var rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
				{
					var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
					var row = tbl.NewRow();
					foreach (var cell in wsRow)
					{
						row[cell.Start.Column - 1] = cell.Text;
					}
					tbl.Rows.Add(row);
				}
				return tbl;
			}
		}
	}
}