﻿using System;
using System.Data;
using System.Text;

namespace ASPVueProject.Common
{
	public static class CSVUtility
	{
		public static string ExportDataTableToCSV(this DataTable dt)
		{
			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < dt.Columns.Count; i++)
			{
				sb.Append(dt.Columns[i].ColumnName + ',');
			}
			sb.Append(Environment.NewLine);

			for (int j = 0; j < dt.Rows.Count; j++)
			{
				for (int k = 0; k < dt.Columns.Count; k++)
				{
					sb.Append(dt.Rows[j][k].ToString() + ',');
				}
				sb.Append(Environment.NewLine);
			}
			return sb.ToString();
		}
	}
}