﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace ASPVueProject.Common
{
	public class HttpsObligationHandler : DelegatingHandler
	{
		protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			if (!HttpContext.Current.Request.IsLocal && !HttpContext.Current.Request.IsSecureConnection)
			{
				return await Task.Factory.StartNew(() =>
				{
					return new HttpResponseMessage(HttpStatusCode.BadRequest)
					{
						Content = new StringContent("Unsecure Connection")
					};
				});
			}
			else
			{
				return await base.SendAsync(request, cancellationToken);
			}
		}
	}
}
