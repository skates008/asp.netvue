﻿using System;
using System.Configuration;
using System.IO;
using Renci.SshNet;
using System.Collections.Generic;

namespace ASPVueProject.Common.Helper
{
	public static class SftpConfigure
	{
		public static void FileUploadSftp(string text, string fileName, string sftpHost = null, int? sftpPort = null, string sftpUserName = null, string sftpPassword = null, string sftpDirectory = null)
		{
			try
			{

				var host = sftpHost ?? ConfigurationManager.AppSettings["sFtpGateway"];
				var port = sftpPort ?? Convert.ToInt32(ConfigurationManager.AppSettings["sFtpPort"]);
				var username = sftpUserName ?? ConfigurationManager.AppSettings["sFtpUsername"];
				var password = sftpPassword ?? ConfigurationManager.AppSettings["sFtpPassword"];
				var mmpUserDirectory = sftpDirectory ?? ConfigurationManager.AppSettings["sFtpFolder"];

				var file = Convert.FromBase64String(text);


				using (var client = new SftpClient(host, port, username, password))
				{
					client.Connect();
					if (!client.IsConnected) return;

					client.ChangeDirectory(mmpUserDirectory);
					using (var ms = new MemoryStream(file))
					{
						client.BufferSize = (uint)ms.Length;
						client.UploadFile(ms, fileName);
					}
				}

			}
			catch (Exception ex)
			{
				XmlLogHelper.SaveIOXML(ex, $"fileUpload{DateTime.Now.ToString("ddMMyyyyhhmmss")}");

			}
		}
		public static void FileUploadSftpKeyAuth(string text, string fileName, string sftpHost = null, int? sftpPort = null, string sftpUserName = null, string sftpKeyPath = null, string sftpDirector = null)
		{
			var host = sftpHost ?? ConfigurationManager.AppSettings["sFtpGatewayProd"];
			var port = sftpPort ?? Convert.ToInt32(ConfigurationManager.AppSettings["sFtpPortProd"]);
			var username = sftpUserName ?? ConfigurationManager.AppSettings["sFtpUsernameProd"];
			var keyPath = sftpKeyPath ?? System.Web.HttpContext.Current.Server.MapPath("~/Certificates/mmp_production_sftp.key"); ;
			var mmpUserDirectory = sftpKeyPath ?? ConfigurationManager.AppSettings["sFtpFolderProd"];

			var file = Convert.FromBase64String(text);
			var keyFile = new PrivateKeyFile(Path.Combine(@"", keyPath));
			var keyFiles = new[] { keyFile };

			var methods = new List<AuthenticationMethod>();
			methods.Add(new PrivateKeyAuthenticationMethod(sftpUserName, keyFiles));

			var con = new ConnectionInfo(host, port, username, methods.ToArray());
			using (var client = new SftpClient(con))
			{
				client.Connect();
				if (!client.IsConnected) return;

				client.ChangeDirectory(mmpUserDirectory);
				using (var ms = new MemoryStream(file))
				{
					client.BufferSize = (uint)ms.Length;
					client.UploadFile(ms, fileName);
				}
				client.Disconnect();
			}

		}
	}
}